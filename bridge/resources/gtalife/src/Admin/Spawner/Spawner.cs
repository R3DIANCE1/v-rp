﻿using System;
using System.Collections.Generic;
using GTANetworkAPI;

namespace gtalife.src.Admin.Spawner
{
    public class Spawner : Script
    {
        private Dictionary<Client, NetHandle> _vehicleHistory = new Dictionary<Client, NetHandle>();

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (_vehicleHistory.ContainsKey(player))
            {
                NAPI.Entity.DeleteEntity(_vehicleHistory[player]);
                _vehicleHistory.Remove(player);
            }
        }

        [RemoteEvent("CREATE_VEHICLE")]
        public void CreateVehicle(Client player, object[] arguments)
        {
            var model = NAPI.Util.GetHashKey((string)arguments[0]);

            if (!Enum.IsDefined(typeof(VehicleHash), model)) return;

            var rot = NAPI.Entity.GetEntityRotation(player.Handle);
            var veh = NAPI.Vehicle.CreateVehicle((VehicleHash)model, player.Position, new Vector3(0, 0, rot.Z), 0, 0);

            if (_vehicleHistory.ContainsKey(player) && _vehicleHistory[player] != null && NAPI.Entity.DoesEntityExist(_vehicleHistory[player]))
            {
                NAPI.Entity.DeleteEntity(_vehicleHistory[player]);
            }

            _vehicleHistory[player] = veh;

            NAPI.Player.SetPlayerIntoVehicle(player, veh, -1);
        }

        [RemoteEvent("REQUEST_SPAWNER_MENU")]
        public void RequestSpawnerMenu(Client player, object[] arguments)
        {
            if (!player.IsAdmin()) return;

            player.TriggerEvent("SHOW_SPAWNER_MENU");
        }
    }
}
