﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var menus = [];

// Main menu
var mainMenu = API.createMenu("SPAWNER", "Seleciona una categoría", 0, 0, 6);
menus.push(mainMenu);

API.onResourceStart.connect(() => {
    var groupedVehicles = [];

    for (let i = 0; i < resource.SpawnerVehicles.vehicleHashes.length; i++) {
        var vehicleHash = resource.SpawnerVehicles.vehicleHashes[i];
        var vehicleClass = resource.SpawnerVehicles.vehicleClassNames[API.getVehicleClass(vehicleHash)];

        if (groupedVehicles[vehicleClass] === undefined) {
            groupedVehicles[vehicleClass] = [];
        }

        groupedVehicles[vehicleClass].push({ hash: vehicleHash, name: API.getVehicleDisplayName(vehicleHash) });
    }

    for (var group in groupedVehicles) {
        if (!groupedVehicles.hasOwnProperty(group)) continue;

        var groupName = group;
        var vehicles = groupedVehicles[group];
        var categoryMenu = createVehicleCategory(groupName);

        for (let i = 0; i < vehicles.length; i++) {
            var vehicle = vehicles[i];
            createSpawnVehicleItem(vehicle.name, vehicle.hash, categoryMenu);
        }
    }
});

API.onServerEventTrigger.connect(function (event_name, args) {
    switch (event_name) {
        case "SHOW_SPAWNER_MENU":
            var anyOpen = false;
            for (var i = 0; i < menus.length; i++) {
                if (menus[i].Visible) {
                    anyOpen = true;
                    menus[i].Visible = false;
                }
            }

            if (anyOpen) return;

            mainMenu.Visible = true;
            break;
    }
});

// Menu creation
function createVehicleCategory(name) {
    var vehicleCategoryMenu = API.createMenu(name, "Seleciona un vehículo", 0, 0, 6);
    menus.push(vehicleCategoryMenu);

    var vehicleCategoryItem = API.createMenuItem(name, "");

    mainMenu.AddItem(vehicleCategoryItem);
    mainMenu.BindMenuToItem(vehicleCategoryMenu, vehicleCategoryItem);

    return vehicleCategoryMenu;
}

function createSpawnVehicleItem(name, hash, parentMenu) {
    var menuItem = API.createMenuItem(name, "");
    menuItem.Activated.connect(function (menu, item) {
        API.triggerServerEvent("CREATE_VEHICLE", hash);
    });
    parentMenu.AddItem(menuItem);
}

// Menu handling
API.onKeyDown.connect(function (sender, keyEventArgs) {
    if (keyEventArgs.KeyCode === Keys.F2) {
        API.triggerServerEvent("REQUEST_SPAWNER_MENU");
    }
});
