﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var menus = [];

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    switch (eventName) {
        case "ShowInteriorBrowser":
            if (menus.length == 0) {
                var categories = JSON.parse(args[0]);
                var interiors = JSON.parse(args[1]);
                var menu = API.createMenu("Interiors", "", 0, 0, 6);
                menus.push(menu);

                for (let i = 0; i < categories.length; i++) {
                    var item = API.createMenuItem(categories[i], "");
                    menu.AddItem(item);

                    var submenu = API.createMenu(categories[i], "", 0, 0, 6);
                    menu.BindMenuToItem(submenu, item);
                    menus.push(submenu);

                    for (let j = 0; j < interiors.length; j++) {
                        if (interiors[j].Category == i) {
                            item = API.createMenuItem(interiors[j].Name, "");
                            submenu.AddItem(item);

                            item.Activated.connect(function (menu, item) {
                                API.triggerServerEvent("TeleportToInterior", j);
                            });
                        }
                    }
                }
            }
            for (let i = 0; i < menus.length; i++) menus[i].Visible = false;
            menus[0].Visible = true;
            break;
    }
});