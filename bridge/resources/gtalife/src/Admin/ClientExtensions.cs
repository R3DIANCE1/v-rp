﻿using GTANetworkAPI;

namespace gtalife.src.Admin
{
    public static class ClientExtensions
    {
        public static bool IsAdmin(this Client player)
        {
            if (player.HasData("User"))
            {
                if (player.GetData("User").Admin > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public static int GetAdminLevel(this Client player)
        {
            if (player.HasData("User"))
            {
                return player.GetData("User").Admin;
            }
            return 0;
        }

        public static void SetAdmin(this Client player, int level)
        {
            if (player.HasData("User"))
            {
                player.GetData("User").Admin = level;
            }
        }
    }
}
