﻿using DSharpPlus.Entities;
using GTANetworkAPI;
using gtalife.src.Flags;
using gtalife.src.Managers;
using gtalife.src.Player.Outfit;
using gtalife.src.Player.Utils;
using System;

namespace gtalife.src.Admin
{
    class Commands : Script
    {
        [Command("acmds")]
        public void CMD_AdminCmds(Client player)
        {
            if (player.IsAdmin())
            {
                NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Admin ~~~~~~~~~~~");
                NAPI.Chat.SendChatMessageToPlayer(player, "* /a - /ir - /tirar - /congelar - /descongelar - /irpos - /say - /darvida - /darchaleco - /dararma - /kill - /alternarcanaldudas");
                NAPI.Chat.SendChatMessageToPlayer(player, "* /silenciar - /pm - /setmoney - /daritem - /removeritems - /setadmin - /sendtojail - /liberar - /kick - /pb - /resucitar");
                NAPI.Chat.SendChatMessageToPlayer(player, "* /ban - /banip - /unban - /respawnveh - /respawnallveh - /spec - /specoff - /interiors - /setvw - /outfit");
                NAPI.Chat.SendChatMessageToPlayer(player, "* /clearchat - /setjob - /rveh - /ar - /rr - /playanim - /stopanim");
                NAPI.Chat.SendChatMessageToPlayer(player, "* /casacmds - /negociocmds - /escuelacmds - /couriercmds - /zonacmds - /ncmds - /bcmds - /pcmds - /blipcmds");
                NAPI.Chat.SendChatMessageToPlayer(player, "* /bancocmds - /factioncmds - /weathercmds");
                NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Admin ~~~~~~~~~~~");
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("a", "~y~USO: ~w~/a [message]", GreedyArg = true)]
        public void CMD_AdminChat(Client sender, string message)
        {
            if (sender.IsAdmin())
            {
                string rank = sender.getRank();
                foreach (var player in NAPI.Pools.GetAllPlayers())
                {
                    if (!player.IsAdmin()) continue;
                    player.SendChatMessage($"!{{#FF4900}}* [ADMIN-CHAT] {rank} {sender.Name}: ~w~{message}");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("playanim")]
        public void CMD_PlayAnim(Client sender, string animDict, string animName)
        {
            if (sender.IsAdmin())
            {
                sender.PlayAnimation(animDict, animName, (int)AnimationFlags.Loop);
                sender.SendChatMessage($"~y~INFO: ~w~AnimDict: ~y~{animDict} ~w~- AnimName: ~y~{animName}");
            }
            else NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("stopanim")]
        public void CMD_StopAnim(Client sender)
        {
            if (sender.IsAdmin())
            {
                sender.StopAnimation();
            }
            else NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("clearchat")]
        public void CMD_ClearChat(Client sender)
        {
            if (sender.IsAdmin())
            {
                for (int i = 0; i < 99; i++) NAPI.Chat.SendChatMessageToAll(" ");
                NAPI.Chat.SendChatMessageToAll($"~y~INFO: ~w~{sender.Name} ha limpiado el chat.");
            }
            else NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("rveh")]
        public void CMD_RepairVehicle(Client sender)
        {
            if (sender.IsAdmin())
            {
                if (!sender.IsInVehicle) NAPI.Chat.SendChatMessageToPlayer(sender, "~r~ERROR: ~s~No estás en un vehículo.");
                else sender.Vehicle.Repair();
            }
            else NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("ar", GreedyArg = true)]
        public void CMD_AcceptReport(Client sender, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
            else if (!target.HasData("HasReported")) sender.SendChatMessage("~r~ERROR: ~w~Este jugador no ha reportado a nadie.");
            else
            {
                target.ResetData("HasReported");
                target.SendChatMessage($"~y~INFO: ~w~El administrador {sender.Name} ha aceptado tu reporte, por favor ten paciencia hasta que lo investigue.");
                sender.SendChatMessage("~y~INFO: ~w~Usted aceptó el informe.");
            }
        }

        [Command("rr", GreedyArg = true)]
        public void CMD_RefuseReport(Client sender, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
            else if (!target.HasData("HasReported")) sender.SendChatMessage("~r~ERROR: ~w~Este jugador no ha reportado a nadie.");
            else
            {
                target.ResetData("HasReported");
                target.SendChatMessage($"~y~INFO: ~w~El administrador {sender.Name} ha rechazado su reporte.");
                sender.SendChatMessage("~y~INFO: ~w~Usted negaste el informe.");
            }
        }

        [Command("pb")]
        public void CMD_TeleportToPb(Client sender, string idOrName)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.Position = new Vector3(-218.9297, 6176.09, 31.29833);
                    target.Dimension = 0;
                }
            }
            else NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("spec")]
        public void CMD_SpecPlayer(Client sender, string idOrName)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else if (sender == target) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No puedes spectar usted mismo.");
                else
                {
                    NAPI.Player.SetPlayerToSpectatePlayer(sender, target);
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("specoff")]
        public void CMD_SpecOff(Client sender)
        {
            if (sender.IsAdmin())
            {
                if (!NAPI.Player.IsPlayerSpectating(sender)) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No estás spec a nadie.");
                else
                {
                    NAPI.Player.UnspectatePlayer(sender);
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("respawnveh")]
        public void CMD_RespawnThisVehicle(Client sender)
        {
            if (sender.IsAdmin())
            {
                if (!sender.IsInVehicle) sender.SendChatMessage("~r~ERROR: ~w~Usted no está en un vehículo.");
                else
                {
                    if (sender.Vehicle.HasData("RESPAWNABLE"))
                    {
                        sender.Vehicle.Position = sender.Vehicle.GetData("SPAWN_POS") - new Vector3(0.0, 0.0, 0.3);
                        sender.Vehicle.Rotation = sender.Vehicle.GetData("SPAWN_ROT");
                    }
                    else
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~Este vehículo no es respawnable.");
                    }
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("respawnallveh")]
        public void CMD_RespawnAllVehicles(Client sender)
        {
            if (sender.IsAdmin())
            {
                foreach (var vehicle in NAPI.Pools.GetAllVehicles())
                {
                    var playersInCar = NAPI.Vehicle.GetVehicleOccupants(sender.Vehicle);
                    if (playersInCar.Count > 0) continue;

                    if (NAPI.Data.HasEntityData(vehicle, "RESPAWNABLE"))
                    {
                        var position = NAPI.Data.GetEntityData(vehicle, "SPAWN_POS") - new Vector3(0.0, 0.0, 0.3);
                        var rotation = NAPI.Data.GetEntityData(vehicle, "SPAWN_ROT");

                        NAPI.Entity.SetEntityPosition(vehicle, position);
                        NAPI.Entity.SetEntityRotation(vehicle, rotation);
                    }
                }
                sender.SendChatMessage("~y~INFO: ~w~Has reaparecido todos los vehículos respawnable.");
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("setjob", "~y~USO: ~w~/setjob [jugador] [jobid]")]
        public void CMD_SetJob(Client sender, string idOrName, int job)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else if (job < -1 || job > Enum.GetValues(typeof(Job.JobType)).Length - 1) NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ERROR: ~w~ID inválido. (( -1, 0-{Enum.GetValues(typeof(Job.JobType)).Length - 1} )).");
                else
                {
                    if (job == -1)
                    {
                        target.SetJob(null);
                        sender.SendChatMessage("~y~INFO: ~w~Cambiaste lo trabajo de {target.Name} a desempleado.");
                        if (target != sender) target.SendChatMessage($"~y~INFO: ~w~{sender.Name} has cambiado su trabajo a desempleado.");
                    }
                    else
                    {
                        int jobid = (int)Job.JobDefinitions.Jobs[job].Type;
                        string jobname = Job.JobDefinitions.Jobs[job].Name;

                        target.SetJob(job);
                        sender.SendChatMessage($"~y~INFO: ~w~Cambiaste lo trabajo de {target.Name} a {jobname}.");
                        if (target != sender) target.SendChatMessage($"~y~INFO: ~w~{sender.Name} has cambiado su trabajo a {jobname}.");
                    }
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("setvw", "~y~USO: ~w~/setvw [jugador] [vw]")]
        public void CMD_SetVirtualWorld(Client sender, string idOrName, uint dimension)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.Dimension = dimension;
                    sender.SendChatMessage($"~y~INFO: ~w~Cambiaste la dimensión de {target.Name} a {dimension}.");
                    if (target != sender) target.SendChatMessage($"~y~INFO: ~w~{sender.Name} has cambiado su dimensión a {dimension}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("kick", "~y~USO: ~w~/kick [jugador] [razón]", GreedyArg = true)]
        public void CMD_Kick(Client sender, string idOrName, string reason)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else if (sender == target) sender.SendChatMessage("~r~ERROR: ~w~No puedes kickar usted mismo.");
                else
                {
                    NAPI.Chat.SendChatMessageToAll($"~y~[ADMIN] {sender.Name} ha chutado {target.Name}. Razón: {reason}");
                    target.Kick(reason);
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("ban", "~y~USO: ~w~/ban [jugador] [dias(0 = permanente)] [razón]", GreedyArg = true)]
        public void CMD_Ban(Client sender, string idOrName, int days, string reason)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else if (sender == target) sender.SendChatMessage("~r~ERROR: ~w~No puedes banir usted mismo.");
                else
                {
                    BanAPI banInstance = new BanAPI();
                    string ban_id = banInstance.BanPlayer(target, (int)BanType.SocialClubName, reason, days);
                    if (ban_id == null)
                    {
                        sender.SendChatMessage("~r~ERROR: ~w~Ban ha fallado.");
                    }
                    else
                    {
                        NAPI.Chat.SendChatMessageToAll($"~y~[ADMIN] {sender.Name} ha banido {target.Name}. Razón: {reason}");
                        sender.SendChatMessage(string.Format("Banned {0}(SC: {1}). (BanID: {2})", target.Name, target.SocialClubName, ban_id));
                    }
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("unban")]
        public void CMD_Unban(Client player, string ban_ID)
        {
            if (player.IsAdmin())
            {
                BanAPI banInstance = new BanAPI();
                if (banInstance.Unban(ban_ID))
                {
                    player.SendChatMessage("~g~ÉXITO: ~w~Unban exitoso.");
                }
                else
                {
                    player.SendChatMessage("~r~ERROR: ~w~Unban ha fallado.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(player, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("sendtojail", "~y~USO: ~w~/sendtojail [jugador] [tiempo(min)]")]
        public void CMD_SendtoJail(Client sender, string idOrName, int time)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else if (sender == target) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No detenir usted mismo.");
                else
                {
                    target.Position = new Vector3(136.5146, -2203.149, 7.30914);
                    target.Freeze(true);
                    target.TriggerEvent("DisplaySubtitle", "Cargando...", 3000);
                    System.Threading.Tasks.Task.Run(() =>
                    {
                        NAPI.Task.Run(() =>
                        {
                            target.Freeze(false);
                        }, delayTime: 2000);
                    });

                    target.setJailTime(time * 60);
                    target.RemoveAllWeapons();
                    target.SetOutfit(67);

                    NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~El administrador {sender.Name} te deteniu.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Deteniste a {target.Name}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("liberar", "~y~USO: ~w~/liberar", GreedyArg = true)]
        public void CMD_FreeFromJail(Client sender, string idOrName)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.setJailTime(2);

                    NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~El administrador {sender.Name} te liberó.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Liberaste a {target.Name}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("ir", "~y~USO: ~w~/ir [jugador]", GreedyArg = true)]
        public void CMD_GoTo(Client sender, string idOrName)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (sender == target) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No puedes teleportarte a tu posición.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    if (sender.IsInVehicle && sender.VehicleSeat == -1)
                    {
                        NAPI.Entity.SetEntityPosition(sender.Vehicle, target.Position);
                        NAPI.Entity.SetEntityDimension(sender, target.Dimension);
                        NAPI.Entity.SetEntityDimension(sender.Vehicle, target.Dimension);                        
                    }
                    else
                    {
                        NAPI.Entity.SetEntityPosition(sender, target.Position);
                        NAPI.Entity.SetEntityDimension(sender, target.Dimension);
                    }

                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Te teletransportastes a la posición de {target.Name}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("traer", "~y~USO: ~w~/traer [jugador]", GreedyArg = true)]
        public void CMD_GetHere(Client sender, string idOrName)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (sender == target) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No puedes traerte a ti mismo.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    if (target.IsInVehicle && target.VehicleSeat == -1)
                    {
                        NAPI.Entity.SetEntityPosition(target.Vehicle, sender.Position);
                        NAPI.Entity.SetEntityDimension(target, sender.Dimension);
                        NAPI.Entity.SetEntityDimension(target.Vehicle, sender.Dimension);
                    }
                    else
                    {
                        NAPI.Entity.SetEntityPosition(target, sender.Position);
                        NAPI.Entity.SetEntityDimension(target, sender.Dimension);                        
                    }

                    NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{sender.Name} te teletransportó a su posición.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Teletransportastes a {target.Name}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("congelar", "~y~USO: ~w~/congelar [jugador]", GreedyArg = true)]
        public void CMD_Freeze(Client sender, string idOrName)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.Freeze(true);
                    if (sender != target) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~El administrador {sender.Name} te congeló.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Congelastes a {target.Name}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("descongelar", "~y~USO: ~w~/descongelar [jugador]", GreedyArg = true)]
        public void CMD_Unfreeze(Client sender, string idOrName)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.Freeze(false);
                    if (sender != target) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{sender.Name} te ha descongelado.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Descongelastes a {target.Name}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("irpos", "~y~USO: ~w~/irpos [X] [Y] [Z]")]
        public void CMD_GoToPos(Client sender, float x, float y, float z)
        {
            if (sender.IsAdmin())
            {
                if (sender.IsInVehicle) sender.Vehicle.Position = new Vector3(x, y, z);
                else sender.Position = new Vector3(x, y, z);

                NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Usted teletransportó hasta la coordenada {x}, {y}, {z}.");
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("say", "~y~USO: ~w~/say [mensaje]", GreedyArg = true)]
        public void CMD_Say(Client sender, string message)
        {
            if (sender.IsAdmin())
            {
                NAPI.Chat.SendChatMessageToAll($"~g~* [GENERAL] {sender.getRank()} {sender.Name}: ~s~{message}");

                // Discord bot message
                var embed = new DiscordEmbedBuilder
                {
                    Title = "Mensaje administrativo",
                    Description = message,
                    ThumbnailUrl = "http://www.atdac.com.mx/img/illustrations/admin.png",
                    Author = new DiscordEmbedBuilder.EmbedAuthor { Name = sender.Name, IconUrl = "https://cdn.discordapp.com/attachments/390881942649962505/448191766030712844/vv.png" },
                    Color = new DiscordColor(0xFF0000)
                };
                Discord.Main.SendDiscordBotMessage(Discord.Main.AdvertisementChannelId, "", embed: embed);
            }
            else NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("darvida", "~y~USO: ~w~/darvida [jugador] [vida]", Alias = "sethp")]
        public void CMD_SetHealth(Client sender, string idOrName, int health)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.Health = health;
                    if (sender != target) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{sender.Name} cambió tu hp a {health}%.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Has cambiado el hp de {target.Name} a {health}%.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("darchaleco", "~y~USO: ~w~/darchaleco [jugador] [vida]", Alias = "setarmor")]
        public void CMD_SetArmor(Client sender, string idOrName, int armor)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.Armor = armor;
                    if (sender != target) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{sender.Name} cambió tu hp a {armor}%.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Has cambiado el hp de {target.Name} a {armor}%.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("kill", "~y~USO: ~w~/kill [jogador]", GreedyArg = true)]
        public void CMD_KillPlayer(Client sender, string idOrName)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.Kill();
                    if (sender != target) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{sender.Name} te mató.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Has matado a {target.Name}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("pm", "~y~USO: ~w~/pm [jugador] [mensaje]", GreedyArg = true)]
        public void CMD_PrivateMessage(Client sender, string idOrName, string message)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else if (target == sender) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No puedes enviarte un mensaje a ti mismo.");
                else
                {
                    target.SendChatMessage($"~y~PM de {sender.Name}: {message}");
                    sender.SendChatMessage($"~y~PM para {target.Name}: {message}");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("setmoney", "~y~USO: ~w~/setmoney [jugador] [money]")]
        public void CMD_SetMoney(Client sender, string idOrName, int money)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.setMoney(money);

                    if (sender != target) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{sender.Name} cambió su dinero a ${money}.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Usted ha cambiado el dinero de {target.Name} a ${money}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("dararma", "~r~USO: ~w~/dararma [jugador] [arma] [munición(opcional)]", Alias = "giveweapon")]
        public void CMD_WeaponCommand(Client sender, string idOrName, WeaponHash weapon, int ammo = 999)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    if (sender != target) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~'{sender.Name}' te ha dado un {weapon}.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Le has dado a '{target.Name}' un {weapon}.");

                    NAPI.Player.GivePlayerWeapon(target, weapon, ammo);
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        #region weather
        [Command("weathercmds")]
        public void CMD_WeatherCmds(Client sender)
        {
            if (sender.IsAdmin())
            {
                NAPI.Chat.SendChatMessageToPlayer(sender, "!{#a5f413}~~~~~~~~~~~ Comandos Clima ~~~~~~~~~~~");
                NAPI.Chat.SendChatMessageToPlayer(sender, "* /setweather - /setnextweather");
                NAPI.Chat.SendChatMessageToPlayer(sender, "!{#a5f413}~~~~~~~~~~~ Comandos Clima ~~~~~~~~~~~");
            }
            else NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("setnextweather")]
        public void CMD_SetNextWeather(Client sender, int weather)
        {
            if (sender.IsAdmin())
            {
                if (weather < 0 || weather > 13) sender.SendChatMessage("~r~ERROR: ~w~Clima inválido. (( 0-13 ))");
                else
                {
                    Server.Main.Weather.Next = weather;
                    Server.Main.UpdateWeatherForAll();

                    sender.SendChatMessage($"~g~ÉXITO: ~w~Estableces el siguiente clima para ~g~{weather}~w~.");
                }
            }
            else NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }

        [Command("setweather")]
        public void CMD_SetWeather(Client sender, int weather)
        {
            if (sender.IsAdmin())
            {
                if (weather < 0 || weather > 13) sender.SendChatMessage("~r~ERROR: ~w~Clima inválido. (( 0-13 ))");
                else
                {
                    Server.Main.Weather.Current = weather;
                    Server.Main.UpdateWeatherForAll();

                    sender.SendChatMessage($"~g~ÉXITO: ~w~Estableces el clima para ~g~{weather}~w~.");
                }
            }
            else NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }
        #endregion

        [Command("setadmin", "~y~USO: ~w~/setadmin [jugador] [nivel]")]
        public void SetAdmin(Client sender, string idOrName, int level)
        {
            if (sender.IsAdmin())
            {
                var target = PlayeridManager.FindPlayer(idOrName);
                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else
                {
                    target.SetAdmin(level);

                    if (target != sender) NAPI.Notification.SendNotificationToPlayer(target, $"~r~ADMIN: ~w~{sender.Name} ha cambiado su administrativo nivel para {level}.");
                    NAPI.Notification.SendNotificationToPlayer(sender, $"~r~ADMIN: ~w~Usted ha cambiado el nivel de admin de {target.Name} para {level}.");
                }
                return;
            }
            NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~No tienes permiso.");
        }
    }
}
