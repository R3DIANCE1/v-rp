﻿using GTANetworkAPI;

namespace gtalife.src.Maps
{
    class Barrio : Script
    {
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            NAPI.Object.CreateObject(1042000049, new Vector3(-62.79719, 6695.12744, 12.01), new Vector3(-0.144161433, -2.759255, -46.43463), 0);
            NAPI.Object.CreateObject(1042000049, new Vector3(-55.9441223, 6699.95068, 12.71), new Vector3(0.105835423, 0.9907547, -47.6846237), 0);
            NAPI.Object.CreateObject(-1055611302, new Vector3(-41.25225, 6694.296, 14.5832262), new Vector3(0.499978453, 6.750053, 135.749252), 0);
            NAPI.Object.CreateObject(-1055611302, new Vector3(-25.9700146, 6702.32129, 16.5998955), new Vector3(10.000679, 16.7493515, 172.910812), 0);
            NAPI.Object.CreateObject(1777271576, new Vector3(14.1098156, 6728.37, 21.34), new Vector3(3.8772, 4.36132956, -120.9583), 0);
            NAPI.Object.CreateObject(-941064660, new Vector3(-26.283741, 6744.994, 15.3424559), new Vector3(-16.50006, 0.750019133, 61.2504425), 0);
            NAPI.Object.CreateObject(-941064660, new Vector3(-32.9201965, 6750.237, 13.9127131), new Vector3(11.9999809, 2.98820851E-06, -117.999123), 0);
            NAPI.Object.CreateObject(-941064660, new Vector3(-34.226368, 6747.791, 13.85), new Vector3(11.9999809, 2.98820851E-06, -117.999123), 0);
            NAPI.Object.CreateObject(1134178299, new Vector3(-57.2646, 6740.31055, 10.79), new Vector3(-1.99997938, -3.63998055, -90.9996262), 0);
            NAPI.Object.CreateObject(1134178299, new Vector3(-30.8740749, 6747.97168, 15.01), new Vector3(-1.68130994, 12.8779612, 151.0033), 0);
            NAPI.Object.CreateObject(1134178299, new Vector3(9.038104, 6726.54639, 20.91), new Vector3(-1.08142734, -3.42477417, 33.94803), 0);
            NAPI.Object.CreateObject(1134178299, new Vector3(-59.4567871, 6695.656, 12.92), new Vector3(-5.91504335, 2.10276413, -137.242889), 0);
            NAPI.Object.CreateObject(-1075526692, new Vector3(1534.44373, 6318.77832, 23.9092789), new Vector3(0, 0, 0), 0);
            NAPI.Object.CreateObject(42353697, new Vector3(-61.6468735, 6743.3877, 9.748775), new Vector3(0, 0, 2.00009823), 0);
            NAPI.Object.CreateObject(42353697, new Vector3(-31.4025249, 6751.79736, 14.15367), new Vector3(8.332668E-06, -13.2499361, -21.9998436), 0);
            NAPI.Object.CreateObject(42353697, new Vector3(-56.6617622, 6700.9873, 12.3948259), new Vector3(9.395891E-06, -1.74994373, -40.99979), 0);
            NAPI.Object.CreateObject(42353697, new Vector3(17.4340076, 6731.75439, 20.8948574), new Vector3(9.449276E-06, -1.74994373, -40.9997864), 0);
            NAPI.Object.CreateObject(-382013683, new Vector3(-53.37388, 6757.101, 10.12), new Vector3(3.0213946E-06, -8.499947, -40.49999), 0);
            NAPI.Object.CreateObject(-382013683, new Vector3(-44.1561127, 6739.43848, 12.5531464), new Vector3(2.00000119, 11.2500677, -175.499222), 0);
            NAPI.Object.CreateObject(-382013683, new Vector3(23.05879, 6741.93848, 22.11), new Vector3(5.22422, 2.54180956, -118.302315), 0);
            NAPI.Object.CreateObject(-382013683, new Vector3(-47.5763321, 6701.442, 13.4044933), new Vector3(1.29947042, 2.46692276, -140.794525), 0);
            NAPI.Object.CreateObject(-679720048, new Vector3(-30.0380077, 6742.266, 14.7690859), new Vector3(2.81756957E-06, -10.0000057, -21.9999638), 0);
            NAPI.Object.CreateObject(-741944541, new Vector3(-32.9388657, 6743.006, 14.866394), new Vector3(157.5048, -0.631488562, -47.2324638), 0);
            NAPI.Object.CreateObject(-741944541, new Vector3(-31.659193, 6739.91162, 14.9385548), new Vector3(96.81967, -3.83804965, 58.75585), 0);
            NAPI.Object.CreateObject(-741944541, new Vector3(-25.75843, 6734.62451, 16.003643), new Vector3(159.965347, 14.3038673, -121.192162), 0);
            NAPI.Object.CreateObject(146905321, new Vector3(-56.9903069, 6740.39355, 10.7855854), new Vector3(0, 0, 91.9997), 0);
            NAPI.Object.CreateObject(146905321, new Vector3(9.451782, 6728.548, 20.98111), new Vector3(0, 0, -26.0000725), 0);
            NAPI.Object.CreateObject(386059801, new Vector3(-45.3728, 6755.09375, 11.75), new Vector3(-10.000042, -5.25000668, 36.24993), 0);
            NAPI.Object.CreateObject(-1761659350, new Vector3(-45.8764725, 6755.934, 11.13), new Vector3(-6.500063, -3.25000429, 37.9999657), 0);
            NAPI.Object.CreateObject(-741944541, new Vector3(-43.686554, 6755.46143, 11.4925137), new Vector3(0, 0, -94.99969), 0);
            NAPI.Object.CreateObject(326972916, new Vector3(-57.9656067, 6750.818, 8.915792), new Vector3(0, 0, -93.99969), 0);
            NAPI.Object.CreateObject(326972916, new Vector3(-14.882472, 6738.099, 17.174221), new Vector3(0, 0, -93.99968), 0);
            NAPI.Object.CreateObject(326972916, new Vector3(-52.7500267, 6700.4834, 12.8956947), new Vector3(0, 0, -61.9994354), 0);
            NAPI.Object.CreateObject(-996428325, new Vector3(4.848782, 6654.24268, 30.2815571), new Vector3(0, 0, 0), 0);
            NAPI.Object.CreateObject(-996428325, new Vector3(-0.2247181, 6668.055, 30.0950031), new Vector3(0, 0, 0), 0);
            NAPI.Object.CreateObject(-996428325, new Vector3(-3.43037629, 6679.279, 30.0992088), new Vector3(0, 0, 0), 0);
            NAPI.Object.CreateObject(-996428325, new Vector3(-5.82527161, 6687.90625, 28.8), new Vector3(1.59027725E-15, -5.00895567E-06, 0.250007242), 0);
            NAPI.Object.CreateObject(-996428325, new Vector3(0.09372637, 6699.67139, 26.5645618), new Vector3(1.59027725E-15, -5.00895567E-06, 0.250007242), 0);
            NAPI.Object.CreateObject(-996428325, new Vector3(2.4136157, 6712.19043, 22.62), new Vector3(1.59027725E-15, -5.00895567E-06, 0.250007242), 0);
            NAPI.Object.CreateObject(1890640474, new Vector3(-24.3168736, 6745.385, 15.68), new Vector3(-8.000053, 0.250020325, 60.9997063), 0);
            NAPI.Object.CreateObject(1890640474, new Vector3(-47.5984154, 6708.08643, 12.94), new Vector3(7.996147, 5.05597353, -146.775192), 0);
            NAPI.Object.CreateObject(-230045366, new Vector3(-39.6577072, 6752.3877, 12.98), new Vector3(-2.24999142, -8.000003, -2.00003815), 0);
            NAPI.Object.CreateObject(1918323043, new Vector3(-22.5355778, 6705.223, 16.60405), new Vector3(-3.49999738, -11.7499943, -5.01426939E-06), 0);
            NAPI.Object.CreateObject(1021214550, new Vector3(-65.70223, 6677.95557, 12.2656822), new Vector3(0, 0, -63.00017), 0);
            NAPI.Object.CreateObject(1021214550, new Vector3(-1.63065743, 6712.17969, 22.3066063), new Vector3(0, 0, 0), 0);
            NAPI.Object.CreateObject(1021214550, new Vector3(16.4315166, 6757.909, 20.3359871), new Vector3(0, 0, 111.999794), 0);
            NAPI.Object.CreateObject(-1025550056, new Vector3(-27.2594013, 6746.529, 18.4341946), new Vector3(-8.750062, -3.75000024, 4.97320661E-06), 0);
            NAPI.Object.CreateObject(-1025550056, new Vector3(-58.7301559, 6738.185, 13.5817766), new Vector3(-10.7500515, -0.999934852, -5.00334136E-06), 0);
            NAPI.Object.CreateObject(-1025550056, new Vector3(8.637144, 6723.583, 23.3376236), new Vector3(-10.7500505, -0.999934733, -5.01668364E-06), 0);
            NAPI.Object.CreateObject(-1025550056, new Vector3(-44.88218, 6706.62061, 16.7608356), new Vector3(-10.7500505, -0.9999346, -5.01668364E-06), 0);
            NAPI.Object.CreateObject(1046318489, new Vector3(-39.87601, 6756.031, 15.2772617), new Vector3(-5.500067, -2.75000668, -4.99499174E-06), 0);
            NAPI.Object.CreateObject(-1199485389, new Vector3(-42.00679, 6754.247, 11.8886852), new Vector3(-0.5000688, -8.5, -48.9999), 0);
            NAPI.Object.CreateObject(1120812170, new Vector3(-39.9688873, 6734.771, 12.9434967), new Vector3(9.832733E-06, 9.000011, 172.999756), 0);
            NAPI.Object.CreateObject(1120812170, new Vector3(-3.77600074, 6741.237, 18.7157326), new Vector3(0, 0, 172.999756), 0);
            NAPI.Object.CreateObject(322493792, new Vector3(-54.72027, 6750.70947, 9.547638), new Vector3(-6.25002337, -0.2500075, 37.99994), 0);
            NAPI.Object.CreateObject(-273279397, new Vector3(-31.5522137, 6698.684, 15.4545984), new Vector3(-12.5001907, -22.9999962, -21.9998436), 0);
            NAPI.Object.CreateObject(575699050, new Vector3(-36.4063072, 6696.204, 14.56347), new Vector3(7.74990273, -5.01592E-06, 6.999942), 0);
            NAPI.Object.CreateObject(575699050, new Vector3(-35.4112129, 6696.96826, 14.6247225), new Vector3(10.9999752, 6.403302E-06, 36.9998779), 0);
            NAPI.Object.CreateObject(1948359883, new Vector3(-32.73689, 6699.24268, 14.8086071), new Vector3(0, 0, 0), 0);
            NAPI.Object.CreateObject(1948359883, new Vector3(-12.4966879, 6740.03027, 17.5695686), new Vector3(0, 0, 176.999664), 0);
            NAPI.Object.CreateObject(1948359883, new Vector3(-44.7899055, 6708.89453, 13.4730759), new Vector3(0, 0, 117.9999), 0);
            NAPI.Object.CreateObject(1948359883, new Vector3(-32.2746162, 6747.66553, 13.8922043), new Vector3(0, 0, 176.999664), 0);
            NAPI.Object.CreateObject(-861422469, new Vector3(-16.0034027, 6738.209, 17.00821), new Vector3(1.08481727E-05, -1.25000167, 2.9999094), 0);
            NAPI.Object.CreateObject(682074297, new Vector3(-28.4858932, 6717.839, 15.509511), new Vector3(1.19751667E-05, -3.50000215, 44.7504921), 0);
            NAPI.Object.CreateObject(326972916, new Vector3(16.7522621, 6729.95068, 20.7613964), new Vector3(0, 0, -93.99968), 0);
            NAPI.Object.CreateObject(326972916, new Vector3(-30.3302078, 6716.77246, 15.1927509), new Vector3(0, 0, -93.99968), 0);
            NAPI.Object.CreateObject(682074297, new Vector3(-27.2535858, 6718.91943, 15.715559), new Vector3(1.19751667E-05, -3.50000215, 44.7504921), 0);
            NAPI.Object.CreateObject(682074297, new Vector3(-26.054863, 6719.98047, 15.9002237), new Vector3(1.19751667E-05, -3.50000215, 44.7504921), 0);
            NAPI.Object.CreateObject(326972916, new Vector3(-36.58473, 6752.004, 13.0954275), new Vector3(0, 0, -93.99968), 0);
            NAPI.Object.CreateObject(175309727, new Vector3(-9.135909, 6738.939, 18.04), new Vector3(9.859758E-06, -5.250006, 31.9999027), 0);
            NAPI.Object.CreateObject(1878378076, new Vector3(6.74594, 6724.95557, 20.09135), new Vector3(0, 0, 29.9999676), 0);
            NAPI.Object.CreateObject(1878378076, new Vector3(-33.68485, 6698.248, 14.69), new Vector3(-10.00007, 4.26886845E-06, 29.9999714), 0);
            NAPI.Object.CreateObject(973800157, new Vector3(-51.6076851, 6737.5293, 11.1), new Vector3(3.49992156, 6.50009441, -143.998077), 0);
            NAPI.Object.CreateObject(-1869605644, new Vector3(-54.8172951, 6738.863, 10.9291553), new Vector3(3.7500093, 1.250001, -99.49975), 0);
            NAPI.Object.CreateObject(-1581502570, new Vector3(-37.09486, 6748.77539, 13.06), new Vector3(3.65386427E-06, -6.749943, -29.9999676), 0);
            NAPI.Object.CreateObject(309119026, new Vector3(-37.9521942, 6749.226, 12.9339314), new Vector3(-5.250028, -3.99999928, 47.9996452), 0);
            NAPI.Object.CreateObject(884467146, new Vector3(-31.91956, 6715.75342, 14.99), new Vector3(-2.25007367, -4.749995, 46.99986), 0);
            NAPI.Object.CreateObject(1578055800, new Vector3(-35.1435356, 6713.69043, 14.58), new Vector3(-2.2499907, -1.25000155, 43.9994965), 0);
            NAPI.Object.CreateObject(-57215983, new Vector3(-48.2808647, 6760.856, 10.25), new Vector3(7.29704971E-06, 6.000002, 154.999954), 0);
            NAPI.Object.CreateObject(-415509317, new Vector3(-13.6702747, 6737.19434, 17.3476372), new Vector3(-5.250138, -1.25000215, 76.99948), 0);
        }
    }
}
