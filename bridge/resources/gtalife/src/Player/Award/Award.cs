﻿namespace gtalife.src.Player.Award
{
    public class Award
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public string TXDLib { get; set; }
        public string TXDName { get; set; }
        public int TXDColor { get; set; }

        public int RequiredProgress { get; set; }

        public Award(string name, string description, string txd_lib, string txd_name, int txd_color, int required_progress)
        {
            Name = name;
            Description = description;

            TXDLib = txd_lib;
            TXDName = txd_name;
            TXDColor = (txd_color < 0) ? 0 : ((txd_color > 3) ? 3 : txd_color);

            RequiredProgress = required_progress;
        }
    }
}
