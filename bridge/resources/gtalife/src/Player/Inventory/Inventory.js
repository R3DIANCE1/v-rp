"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var inventoryMenu = null;
var itemActionsMenu = null;
var inventoryData = [];
var selectedIdx = -1;
var dropAmount = 0;
var tradeMenu = null;
var tradeAmount = 0;
API.onResourceStart.connect(function () {
    // main inventory menu
    inventoryMenu = API.createMenu("Inventario", "Selecciona un artículo.", 0, 0, 6);
    inventoryMenu.OnItemSelect.connect(function (menu, item, index) {
        selectedIdx = index;
        // update actions menu
        API.setMenuTitle(itemActionsMenu, inventoryData[index].Name);
        itemActionsMenu.Clear();
        itemActionsMenu.AddItem(API.createMenuItem("Usar", ""));
        if (inventoryData[index].IsDroppable) {
            if (inventoryData[index].Quantity > 1) {
                var amountList = new List(String);
                for (var i = 1; i <= inventoryData[index].Quantity; i++)
                    amountList.Add(i.toString());
                var dropListItem = API.createListItem("Tirar", "", amountList, 0);
                itemActionsMenu.AddItem(dropListItem);
                dropListItem.OnListChanged.connect(function (sender, new_index) {
                    dropAmount = new_index + 1;
                });
                var tradeListItem = API.createListItem("Vender", "", amountList, 0);
                itemActionsMenu.AddItem(tradeListItem);
                tradeListItem.OnListChanged.connect(function (sender, new_index) {
                    tradeAmount = new_index + 1;
                });
            }
            else {
                itemActionsMenu.AddItem(API.createMenuItem("Tirar", ""));
                itemActionsMenu.AddItem(API.createMenuItem("Vender", ""));
            }
        }
        else {
            itemActionsMenu.AddItem(API.createMenuItem("Tirar", "No puedes tirar este item."));
            itemActionsMenu.MenuItems[1].Enabled = false;
            itemActionsMenu.AddItem(API.createMenuItem("Vender", "No puedes vender este item."));
            itemActionsMenu.MenuItems[2].Enabled = false;
        }
        dropAmount = 0;
        tradeAmount = 0;
        inventoryMenu.Visible = false;
        itemActionsMenu.Visible = true;
    });
    // actions menu
    itemActionsMenu = API.createMenu("ItemName", "Selecciona una acción.", 0, 0, 6);
    itemActionsMenu.ParentMenu = inventoryMenu;
    itemActionsMenu.OnItemSelect.connect(function (menu, item, index) {
        switch (index) {
            case 0:
                API.triggerServerEvent("ConsumeItem", selectedIdx);
                break;
            case 1:
                if (!inventoryData[selectedIdx].IsDroppable)
                    return;
                var amount = (inventoryData[selectedIdx].Quantity > 1) ? dropAmount : 1;
                if (amount < 1)
                    amount = 1;
                var pos = API.getEntityFrontPosition(API.getLocalPlayer());
                var ground = API.getGroundHeight(pos);
                API.triggerServerEvent("DropItem", selectedIdx, amount, new Vector3(pos.X, pos.Y, ground));
                break;
            case 2:
                if (!inventoryData[selectedIdx].IsDroppable)
                    return;
                var amount = (inventoryData[selectedIdx].Quantity > 1) ? tradeAmount : 1;
                if (amount < 1)
                    amount = 1;
                API.triggerServerEvent("RequestNearbyPlayersInventory", selectedIdx, amount);
                break;
        }
    });
});
API.onServerEventTrigger.connect(function (eventName, args) {
    switch (eventName) {
        case "ReceiveInventory":
            inventoryMenu.Clear();
            // load data
            inventoryData = JSON.parse(args[0]);
            for (var i = 0; i < inventoryData.length; i++) {
                var temp_item = API.createMenuItem(inventoryData[i].Name, inventoryData[i].Description);
                if (inventoryData[i].StackSize > 1)
                    temp_item.SetRightLabel(inventoryData[i].Quantity + "/" + inventoryData[i].StackSize);
                inventoryMenu.AddItem(temp_item);
            }
            selectedIdx = -1;
            itemActionsMenu.Visible = false;
            inventoryMenu.Visible = (inventoryData.length > 0);
            if (inventoryData.length < 1) {
                API.callNative("_SET_NOTIFICATION_BACKGROUND_COLOR", 11);
                API.sendNotification("Tu inventario está vacío.");
            }
            break;
        case "CloseInventoryMenus":
            inventoryMenu.Visible = false;
            itemActionsMenu.Visible = false;
            break;
        case "ReceiveNearbyPlayersInventory":
            if (tradeMenu == null) {
                tradeMenu = API.createMenu("Inventario", "Selecciona un jugador.", 0, 0, 6);
            }
            tradeMenu.Clear();
            var players = JSON.parse(args[2]);
            for (let i = 0; i < players.length; i++) {
                var playerItem = API.createMenuItem(players[i], "");
                tradeMenu.AddItem(playerItem);
                playerItem.Activated.connect(function (menu, item) {
                    var price = API.getUserInput("", 9);
                    if (parseInt(price) > 1)
                        API.triggerServerEvent("TradeItem", args[0], args[1], players[i], price);
                    else
                        API.sendNotification("~r~Precio inválido");
                    tradeMenu.Visible = false;
                    inventoryMenu.Visible = false;
                    itemActionsMenu.Visible = false;
                });
            }
            inventoryMenu.Visible = false;
            itemActionsMenu.Visible = false;
            tradeMenu.Visible = true;
            break;
    }
});
API.onKeyUp.connect(function (e, key) {
    if (key.KeyCode == Keys.I) {
        if (API.isChatOpen() || API.isAnyMenuOpen() || !API.getHudVisible())
            return;
        API.triggerServerEvent("RequestInventory");
    }
});
