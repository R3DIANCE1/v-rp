﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) =>
{
    if (eventName == "DisplaySubtitle")
    {
        API.displaySubtitle(args[0], (args[1] > 0) ? args[1] : 3000);
    }
});