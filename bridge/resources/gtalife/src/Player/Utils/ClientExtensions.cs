﻿using GTANetworkAPI;
using gtalife.src.Database.Models;
using System;

namespace gtalife.src.Player.Utils
{
    public static class ClientExtensions
    {
        public static bool IsLogged(this Client player)
        {
            if (Data.Character.ContainsKey(player)) return true;
            return false;
        }

        public static void loadClothes(this Client player)
        {
            for (int i = 0; i < 9; i++) player.ClearAccessory(i);
            for (int i = 0; i < 12; i++)
            {
                if (i == 2) continue;
                player.SetClothes(i, -1, -1);
            }

            foreach (var clothes in Data.Character[player].Clothes)
            {
                if (clothes.IsProp) player.SetAccessories(clothes.Slot, clothes.Drawable, clothes.Texture);
                else player.SetClothes(clothes.Slot, clothes.Drawable, clothes.Texture);
            }
        }

        public static System.Collections.Generic.List<Client> getPlayersInRadiusOfPlayer(this Client player, float radius)
        {
            System.Collections.Generic.List<Client> players = new System.Collections.Generic.List<Client>();
            foreach (var target in NAPI.Pools.GetAllPlayers())
            {
                if (!target.IsLogged()) continue;

                if (player.Position.DistanceTo(target.Position) <= radius)
                {
                    players.Add(target);
                }
            }
            return players;
        }

        public static string getPlayerGenderName(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return null;
            return Data.Character[player].Gender ? "female" : "male";
        }

        public static void setPhoneNumber(this Client player, int number)
        {
            if (!Data.Character.ContainsKey(player)) return;
            Data.Character[player].PhoneNumber = number;
        }

        public static int? getPhoneNumber(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return null;
            return Data.Character[player].PhoneNumber;
        }

        public static void setJailTime(this Client player, int time)
        {
            if (!Data.Character.ContainsKey(player)) return;
            Data.Character[player].JailTime = time;
        }

        public static int getJailtime(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return 0;
            return Data.Character[player].JailTime;
        }

        public static bool IsGangMember(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return false;
            if (Data.Character[player].Gang != null) return true;
            return false;
        }

        public static Guid? GetGangId(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return null;
            return Data.Character[player].Gang;
        }

        public static void SetGangId(this Client player, Guid? id)
        {
            if (!Data.Character.ContainsKey(player)) return;
            Data.Character[player].Gang = id;
        }

        public static int GetGangRank(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return 0;
            return Data.Character[player].GangRank;
        }

        public static void SetGangRank(this Client player, int rank)
        {
            if (!Data.Character.ContainsKey(player)) return;
            Data.Character[player].GangRank = rank;
        }

        public static int? GetJob(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return null;
            return Data.Character[player].Job;
        }

        public static void SetJob(this Client player, int? job)
        {
            if (!Data.Character.ContainsKey(player)) return;
            Data.Character[player].Job = job;
        }
        public static Guid? GetFactionID(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return null;
            return Data.Character[player].Faction;
        }

        public static void SetFactionID(this Client player, Guid? faction)
        {
            if (!Data.Character.ContainsKey(player)) return;
            Data.Character[player].Faction = faction;
        }

        public static int? GetFactionRank(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return null;
            return Data.Character[player].FacRank;
        }

        public static void SetFactionRank(this Client player, int? rank)
        {
            if (!Data.Character.ContainsKey(player)) return;
            Data.Character[player].FacRank = rank;
        }

        public static void giveMoney(this Client player, int money)
        {
            if (!Data.Character.ContainsKey(player)) return;

            Data.Character[player].Money += money;
            NAPI.ClientEvent.TriggerClientEvent(player, "UpdateMoneyHUD", Convert.ToString(Data.Character[player].Money), Convert.ToString(money));
        }

        public static void setMoney(this Client player, int money)
        {
            if (!Data.Character.ContainsKey(player)) return;

            Data.Character[player].Money = money;
            NAPI.ClientEvent.TriggerClientEvent(player, "UpdateMoneyHUD", Convert.ToString(Data.Character[player].Money));
        }

        public static int getMoney(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return 0;
            return Data.Character[player].Money;
        }

        public static void setDrivingLicense(this Client player, bool set)
        {
            if (!Data.Character.ContainsKey(player)) return;
            Data.Character[player].DrivingLicense = set;
        }

        public static bool getDrivingLicense(this Client player)
        {
            if (!Data.Character.ContainsKey(player)) return false;
            return Data.Character[player].DrivingLicense;
        }

        public static void sendChatAction(this Client player, string action, int style = 0)
        {
            var msg = (style == 0) ? $"* {player.Name} {action}" : $"* {action} (( {player.Name} ))";
            var players = player.getPlayersInRadiusOfPlayer(15f);

            foreach (var p in players)
            {
                if (p.Dimension != player.Dimension) continue;
                var color = style == 0 ? "!{#DA70D6}" : "!{#5CFF00}";
                p.SendChatMessage($"{color}{msg}");
            }
        }

        public static string getRank(this Client player)
        {
            var rank = "Jugador";
            if (!player.HasData("User")) return rank;

            User user = player.GetData("User");
            switch (user.Admin)
            {
                case 1:
                    rank = "Ayudante";
                    break;
                case 2:
                    rank = "Moderador";
                    break;
                case 3:
                    rank = "Moderador Global";
                    break;
                case 4:
                    rank = "Admin";
                    break;
            }

            return rank;
        }
    }
}
