"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var browser = null;
API.onServerEventTrigger.connect((eventName, args) => {
    switch (eventName) {
        case "PlayAudioStream":
            PlayAudioStream(args[0]);
            break;
        case "StopAudioStream":
            StopAudioStream();
            break;
    }
});
function PlayAudioStream(url) {
    if (browser != null)
        API.destroyCefBrowser(browser);
    browser = API.createCefBrowser(500, 500, false);
    API.setCefBrowserHeadless(browser, true);
    API.setCefBrowserPosition(browser, 500, 250);
    API.waitUntilCefBrowserInit(browser);
    API.loadPageCefBrowser(browser, url);
    API.waitUntilCefBrowserLoaded(browser);
}
function StopAudioStream() {
    if (browser != null)
        API.destroyCefBrowser(browser);
    browser = null;
}
