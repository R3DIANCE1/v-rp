﻿using System;
using System.Linq;
using gtalife.src.Player.Utils;
using gtalife.src.Database.Models;
using gtalife.src.Player.Inventory.Extensions;
using GTANetworkAPI;
using Microsoft.EntityFrameworkCore;

namespace gtalife.src.Player.Selection
{
    class CharacterData
    {
        public string Name { get; set; }

        public bool Gender { get; set; }

        public int FaceFirst { get; set; }

        public int FaceSecond { get; set; }

        public float FaceMix { get; set; }

        public int SkinFirst { get; set; }

        public int SkinSecond { get; set; }

        public float SkinMix { get; set; }

        public int HairType { get; set; }

        public int HairColor { get; set; }

        public int HairHighlight { get; set; }

        public int EyeColor { get; set; }

        public int Eyebrows { get; set; }

        public int EyebrowsColor1 { get; set; }

        public int EyebrowsColor2 { get; set; }

        public int? Beard { get; set; }

        public int? BeardColor { get; set; }

        public int? Makeup { get; set; }

        public int? MakeupColor { get; set; }

        public int? Lipstick { get; set; }

        public int? LipstickColor { get; set; }

        public int Torso { get; set; }

        public int Topshirt { get; set; }

        public int TopshirtTexture { get; set; }

        public int Undershirt { get; set; }

        public int Legs { get; set; }

        public int Feet { get; set; }

        public int Accessory { get; set; }
    }

    class CharacterCreation : Script
    {
        [RemoteEvent("FinishCharacterCreation")]
        public void FinishCharacterCreation(Client player, object[] arguments)
        {
            if (!NAPI.Data.HasEntityData(player, "User"))
                return;

            var characterData = NAPI.Util.FromJson<CharacterData>((string)arguments[0]);

            // Checking if name is available
            string characterName = null;
            characterName = characterData.Name;

            using (var ctx = new DefaultDbContext())
            {
                var user = ctx.Users.FirstOrDefault(up => up.SocialClubName == player.SocialClubName);
                NAPI.Data.SetEntityData(player, "User", user);
                
                var isNameTaken = ctx.Characters.FirstOrDefault(up => up.Name == characterName);
                if (isNameTaken != null)
                {
                    NAPI.ClientEvent.TriggerClientEvent(player, "CharacterNameAlreadyTaken");
                    return;
                }

                // Character
                Character character = new Character
                {
                    User = user,
                    Name = characterData.Name,
                    Gender = characterData.Gender,
                    Money = 100,
                    Bank = 1000,
                    Level = 1,
                    Experience = 0,
                    PlayedTime = 0,
                    PositionX = 169.3792f,
                    PositionY = -967.8402f,
                    PositionZ = 29.98808f,
                    RotationZ = 138.6666f,
                    LastLogin = DateTime.Now,
                    CreatedAt = DateTime.Now
                };

                ctx.Characters.Add(character);

                // Traits
                CharacterTrait traits = new CharacterTrait
                {
                    Character = character,
                    FaceFirst = characterData.FaceFirst,
                    FaceSecond = characterData.FaceSecond,
                    FaceMix = characterData.FaceMix,
                    SkinFirst = characterData.SkinFirst,
                    SkinSecond = characterData.SkinSecond,
                    SkinMix = characterData.SkinMix,
                    HairType = characterData.HairType,
                    HairColor = characterData.HairColor,
                    HairHighlight = characterData.HairHighlight,
                    EyeColor = characterData.EyeColor,
                    Eyebrows = characterData.Eyebrows,
                    EyebrowsColor1 = characterData.EyebrowsColor1,
                    EyebrowsColor2 = characterData.EyebrowsColor2,
                    Beard = characterData.Beard,
                    BeardColor = characterData.BeardColor,
                    Makeup = characterData.Makeup,
                    MakeupColor = characterData.MakeupColor,
                    Lipstick = characterData.Lipstick,
                    LipstickColor = characterData.LipstickColor
                };

                ctx.CharacterTrait.Add(traits);

                // Clothes
                CharacterClothes torso = new CharacterClothes
                {
                    Character = character,
                    Slot = 3,
                    Drawable = characterData.Torso,
                    Texture = 0,
                    IsProp = false
                };

                CharacterClothes top = new CharacterClothes
                {
                    Character = character,
                    Slot = 11,
                    Drawable = characterData.Topshirt,
                    Texture = characterData.TopshirtTexture,
                    IsProp = false
                };

                CharacterClothes undershirt = new CharacterClothes
                {
                    Character = character,
                    Slot = 8,
                    Drawable = characterData.Undershirt,
                    Texture = 0,
                    IsProp = false
                };

                CharacterClothes pants = new CharacterClothes
                {
                    Character = character,
                    Slot = 4,
                    Drawable = characterData.Legs,
                    Texture = 0,
                    IsProp = false
                };

                CharacterClothes shoes = new CharacterClothes
                {
                    Character = character,
                    Slot = 6,
                    Drawable = characterData.Feet,
                    Texture = 0,
                    IsProp = false
                };

                CharacterClothes accessory = new CharacterClothes
                {
                    Character = character,
                    Slot = 7,
                    Drawable = characterData.Accessory,
                    Texture = 0,
                    IsProp = false
                };

                ctx.CharacterClothes.Add(torso);
                ctx.CharacterClothes.Add(top);
                ctx.CharacterClothes.Add(undershirt);
                ctx.CharacterClothes.Add(pants);
                ctx.CharacterClothes.Add(shoes);
                ctx.CharacterClothes.Add(accessory);

                player.Name = characterData.Name;
                player.loadInventory();
                player.setMoney(character.Money);

                // Send to spawn
                NAPI.ClientEvent.TriggerClientEvent(player, "closeCharacterCreationBrowser");

                Managers.DimensionManager.DismissPrivateDimension(player);
                NAPI.Entity.SetEntityDimension(player, 0);

                ctx.SaveChanges();

                // This is fucking ugly
                var chr = ctx.Characters.Include(c => c.Vehicles).Include(c => c.Contacts).Include(c => c.Clothes).Include(c => c.Trait).Include(c => c.Weapons).FirstOrDefault(up => up.Id == character.Id);
                Data.Character.Add(player, chr);
                Data.LoadAccount(player);
            }

            // Sync player face with other players
            Customization.CustomizationModel gtao = new Customization.CustomizationModel();
            gtao.InitializePedFace(player);
            gtao.UpdatePlayerFace(player);

            player.SendChatMessage("~g~¡Bienvenido a Paleto Bay! El autobús te ha dejado en una de las paradas de Paleto.");
            player.SendChatMessage("~g~Eres un extranjero sin dinero que vienes a buscar suerte en uno de los pueblos con mas movimiento de San Andreas.");
            player.SendChatMessage("~g~Te recomendamos que vayas directo a sacarte la licencia de conducción, la piden en la mayoría de los trabajos.");
            player.SendChatMessage("~g~Ah, y recuerda que tienes 1000$ en el banco para sobrevivir.");
            player.SendChatMessage("~g~¡Mucha suerte!");
        }
    }
}
