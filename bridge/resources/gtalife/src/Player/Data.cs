﻿using System;
using System.Linq;
using gtalife.src.Database.Models;
using GTANetworkAPI;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace gtalife.src.Player
{
    public class Data : Script
    {
        public static Dictionary<Client, Character> Character = new Dictionary<Client, Character>();

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            SaveAccount(player);

            if (Character.ContainsKey(player)) Character.Remove(player);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            var players = NAPI.Pools.GetAllPlayers();
            foreach (var player in players)
            {
                SaveAccount(player);

                if (Character.ContainsKey(player)) Character.Remove(player);
                NAPI.Data.ResetEntityData(player, "User");
            }
        }

        public static void LoadAccount(Client player)
        {
            if (!Character.ContainsKey(player)) return;

            foreach (var vehicle in Character[player].Vehicles)
            {
                vehicle.Handle = NAPI.Vehicle.CreateVehicle(vehicle.Model, new Vector3(vehicle.PositionX, vehicle.PositionY, vehicle.PositionZ), 0f, vehicle.Color1, vehicle.Color2);

                NAPI.Vehicle.SetVehicleEngineStatus(vehicle.Handle, false);
                // NAPI.Vehicle.SetVehicleFuelLevel(vehicle.Handle, (vehicle.Fuel >= 1 && vehicle.Fuel <= 3) ? 4 : vehicle.Fuel); // deprecated
            }

            foreach (var weapon in Character[player].Weapons)
            {
                player.GiveWeapon((WeaponHash)weapon.Weapon, weapon.Ammo);
            }

            foreach (var clothes in Character[player].Clothes)
            {
                if (clothes.IsProp) player.SetAccessories(clothes.Slot, clothes.Drawable, clothes.Texture);
                else player.SetClothes(clothes.Slot, clothes.Drawable, clothes.Texture);
            }

            foreach (var contacts in Character[player].Contacts)
            {
                // Just to load from EF lazy loading
            }

            var trait = Character[player].Trait;
        }

        public static void SaveAccount(Client player)
        {
            if (!Character.ContainsKey(player)) return;

            User user = player.GetData("User");
            using (var ctx = new DefaultDbContext())
            {
                User _user = ctx.Users.FirstOrDefault(x => x.Id == user.Id);
                _user.Admin = user.Admin;

                int characterId = Character[player].Id;
                Character character = ctx.Characters.Include(c => c.Vehicles).Include(c => c.Contacts).Include(c => c.Clothes).Include(c => c.Trait).Include(c => c.Weapons).FirstOrDefault(up => up.Id == characterId);

                character.PositionX = player.Position.X;
                character.PositionY = player.Position.Y;
                character.PositionZ = player.Position.Z;
                character.RotationZ = player.Rotation.Z;
                character.Dimension = player.Dimension;

                character.Money = Character[player].Money;
                character.Bank = Character[player].Bank;
                character.Level = Character[player].Level;
                character.Experience = Character[player].Experience;
                character.Job = Character[player].Job;
                character.PhoneNumber = Character[player].PhoneNumber;
                character.DrivingLicense = Character[player].DrivingLicense;
                character.JailTime = Character[player].JailTime;
                character.WantedLevel = Character[player].WantedLevel;

                character.Faction = Character[player].Faction;
                character.FacRank = Character[player].FacRank;

                character.InsideHouseId = Character[player].InsideHouseId;
                character.Gang = Character[player].Gang;
                character.GangRank = Character[player].GangRank;

                character.PlayedTime += (DateTime.Now - character.LastLogin).TotalSeconds;
                character.LastLogin = DateTime.Now;

                // Vehicles
                foreach (var vehicle in Character[player].Vehicles.ToList())
                {
                    // Getting vehicle data
                    float fuel = 100f; // API.shared.getVehicleFuelLevel(vehicle.Handle); // deprecated
                    var position = NAPI.Entity.GetEntityPosition(vehicle.Handle);
                    var rotation = NAPI.Entity.GetEntityRotation(vehicle.Handle);

                    int vehicleId = vehicle.Id;
                    CharacterVehicle characterVehicle = ctx.CharacterVehicles.FirstOrDefault(x => x.Id == vehicleId);

                    if (characterVehicle == null)
                    {
                        characterVehicle = new CharacterVehicle { Model = vehicle.Model, PositionX = vehicle.PositionX, PositionY = vehicle.PositionY, PositionZ = vehicle.PositionZ, RotationX = vehicle.RotationX, RotationY = vehicle.RotationY, RotationZ = vehicle.RotationZ, Color1 = vehicle.Color1, Color2 = vehicle.Color2, Price = vehicle.Price, Fuel = vehicle.Fuel, CreatedAt = DateTime.Now };
                        character.Vehicles.Add(characterVehicle);
                    }

                    characterVehicle.PositionX = position.X;
                    characterVehicle.PositionY = position.Y;
                    characterVehicle.PositionZ = position.Z;
                    characterVehicle.RotationX = rotation.X;
                    characterVehicle.RotationY = rotation.Y;
                    characterVehicle.RotationZ = rotation.Z;

                    characterVehicle.Fuel = (int)fuel;

                    if (NAPI.Entity.DoesEntityExist(vehicle.Handle)) NAPI.Entity.DeleteEntity(vehicle.Handle);
                }

                // Weapons
                if (player.Weapons != null)
                {
                    var playerWeapons = player.Weapons.ToList();
                    foreach (WeaponHash weapon in playerWeapons)
                    {
                        if (weapon == WeaponHash.Unarmed) continue;

                        var weap = ctx.CharacterWeapons.FirstOrDefault(up => up.Character == character && up.Weapon == (int)weapon);
                        if (weap == null)
                        {
                            weap = new CharacterWeapon { Character = character, Weapon = (int)weapon, Ammo = NAPI.Player.GetPlayerWeaponAmmo(player, weapon) };
                            ctx.CharacterWeapons.Add(weap);
                        }
                        else
                        {
                            weap.Ammo = NAPI.Player.GetPlayerWeaponAmmo(player, weapon);
                        }
                    }

                    foreach (var weapon in character.Weapons.ToList())
                    {
                        if (!playerWeapons.Contains((WeaponHash)weapon.Weapon)) ctx.CharacterWeapons.Remove(weapon);
                    }
                }

                // Clothes
                foreach (var clothes in Character[player].Clothes.ToList())
                {
                    CharacterClothes characterClothes = ctx.CharacterClothes.FirstOrDefault(x => x.Id == clothes.Id);
                    if (characterClothes != null)
                    {
                        characterClothes.Drawable = clothes.Drawable;
                        characterClothes.Texture = clothes.Texture;
                    }
                    else
                    {
                        new CharacterClothes { Slot = clothes.Slot, Drawable = clothes.Drawable, Texture = clothes.Texture, IsProp = clothes.IsProp };
                        character.Clothes.Add(clothes);
                    }
                }

                // Contacts
                if (Character[player].Contacts != null)
                {
                    foreach (var contact in Character[player].Contacts.ToList())
                    {
                        CharacterContact characterContact = ctx.CharacterContacts.FirstOrDefault(x => x.Id == contact.Id);
                        if (characterContact == null)
                        {
                            characterContact = new CharacterContact { Name = contact.Name, Phone = contact.Phone };
                            character.Contacts.Add(characterContact);
                        }
                    }

                    foreach (var contact in character.Contacts.ToList())
                    {
                        if (Character[player].Contacts.FirstOrDefault(x => x.Id == contact.Id) == null) ctx.CharacterContacts.Remove(contact);
                    }
                }

                // Traits
                character.Trait.HairType = Character[player].Trait.HairType;
                character.Trait.HairColor = Character[player].Trait.HairColor;

                character.Trait.FaceFirst = Character[player].Trait.FaceFirst;
                character.Trait.FaceSecond = Character[player].Trait.FaceSecond;
                character.Trait.FaceMix = Character[player].Trait.FaceMix;

                ctx.SaveChangesAsync();
            }            
        }
    }
}
