﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var player = null;
var browser = null;
var canMove = false;

API.onResourceStart.connect(() =>
{
    // Login camera position
    NAPI.Entity.SetEntityPosition(API.getLocalPlayer(), new Vector3(-1473.709, 160.6735, 54.92348));

    var startCam = API.createCamera(new Vector3(-1483.709, 160.6735, 54.92348), new Vector3());
    API.pointCameraAtPosition(startCam, new Vector3(-1503.526, 136.576, 55.65308));

    var endCam = API.createCamera(new Vector3(-1539.865, 121.2464, 111.8135), new Vector3());
    API.pointCameraAtPosition(endCam, new Vector3(-1129.835, -125.4717, 231.0006));

    API.interpolateCameras(startCam, endCam, 30000, false, false);

    API.setHudVisible(false);
    API.setChatVisible(false);
    resource.CharacterCreator.ResetCharacterCreation();
    API.startMusic("res/sounds/music01.ogg", true);    
    API.setShowWastedScreenOnDeath(false);
});

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) =>
{
    if (eventName == "ShowLoginForm")
    {
        player = args[0];
        if (browser == null)
        {
            var res = API.getScreenResolution();
            browser = API.createCefBrowser(res.Width, res.Height);
            API.setCefBrowserPosition(browser, 0, 0);
            API.waitUntilCefBrowserInit(browser);
            API.loadPageCefBrowser(browser, "res/views/login.html");
            API.waitUntilCefBrowserLoaded(browser);
        }

        API.showCursor(true);
        API.setCanOpenChat(false);
        API.setCefBrowserHeadless(browser, false);
    }
    else if (eventName == "ShowCharacterSelection")
    {
        if (browser != null)
        {
            API.destroyCefBrowser(browser);
            browser = null;
        }        

        resource.CharacterSelector.ShowCharacterSelector();
    }
    else if (eventName == "LoginError")
    {
        if (browser != null)
        {            
            browser.call("ShowError", args[0]);
        }
    }
});

API.onResourceStop.connect(() =>
{
    if (browser != null)
    {
        API.destroyCefBrowser(browser);
        browser = null;
    }
});

API.onUpdate.connect(() =>
{
    if (!canMove)
    {
        API.disableAllControlsThisFrame();
    }
});

function stopMusic()
{
    API.stopMusic();
}

function Login(username: string, password: string)
{
    resource.Sounds.PlaySelectSound();
    API.triggerServerEvent("LoginAttempt", username, password);
}

function Register(username: string, password: string, email: string)
{
    resource.Sounds.PlaySelectSound();
    API.triggerServerEvent("RegisterAttempt", username, password, email);
}

function LoginBrowserReady()
{
    browser.call("update", player);
}

function showRegisterPage()
{
    API.loadPageCefBrowser(browser, "res/views/register.html");
}

function showLoginPage()
{
    API.loadPageCefBrowser(browser, "res/views/login.html");
}