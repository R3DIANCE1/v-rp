﻿using System;
using System.Linq;
using System.Text;
using gtalife.src.Database.Models;
using System.Security.Cryptography;
using GTANetworkAPI;
using System.Collections.Generic;

namespace gtalife.src.Player.Auth
{
    class Login : Script
    {
        [ServerEvent(Event.PlayerConnected)]
        public void OnPlayerConnected(Client player)
        {
            using (var ctx = new DefaultDbContext())
            {
                var user = ctx.Users.FirstOrDefault(up => up.SocialClubName == player.SocialClubName);
                if (user != null)
                {
                    user.LastIp = player.Address;
                    NAPI.Data.SetEntityData(player, "User", user);
                    NAPI.ClientEvent.TriggerClientEvent(player, "ShowCharacterSelection");

                    ctx.SaveChanges();
                }
                else
                {
                    Dictionary<string, string> playerData = new Dictionary<string, string>()
                    {
                        { "name", player.Name},
                        { "socialClub", player.SocialClubName}
                    };
                    NAPI.ClientEvent.TriggerClientEvent(player, "ShowLoginForm", NAPI.Util.ToJson(playerData));
                }
            }            

            uint dimension = Managers.DimensionManager.RequestPrivateDimension(player);
            NAPI.Entity.SetEntityDimension(player, dimension);
        }

        [RemoteEvent("LoginAttempt")]
        public void LoginAttempt(Client player, object[] arguments)
        {
            string username = (string)arguments[0];
            string password = Encrypt((string)arguments[1]);

            using (var ctx = new DefaultDbContext())
            {
                var user = ctx.Users.FirstOrDefault(up => up.Username == username && up.Password == password);
                if (user != null)
                {
                    user.SocialClubName = player.SocialClubName;
                    NAPI.Data.SetEntityData(player, "User", user);
                    NAPI.ClientEvent.TriggerClientEvent(player, "ShowCharacterSelection");
                }
                else
                {
                    NAPI.ClientEvent.TriggerClientEvent(player, "LoginError", "Usuario o contraseña incorrecta.");
                }
            }
        }

        [RemoteEvent("RegisterAttempt")]
        public void RegisterAttempt(Client player, object[] arguments)
        {
            string username = (string)arguments[0];
            string password = Encrypt((string)arguments[1]);
            string emailadd = (string)arguments[2];

            using (var ctx = new DefaultDbContext())
            {
                var user = ctx.Users.FirstOrDefault(up => up.Username == username);
                if (user == null)
                {
                    user = new User { Username = username, Password = password, Email = emailadd, SocialClubName = player.SocialClubName, Admin = 0, LastIp = player.Address };
                    ctx.Users.Add(user);

                    NAPI.Data.SetEntityData(player, "User", user);
                    NAPI.ClientEvent.TriggerClientEvent(player, "ShowCharacterSelection");

                    ctx.SaveChanges();
                }
                else
                {
                    NAPI.ClientEvent.TriggerClientEvent(player, "LoginError", "Este usuario ya está en uso.");
                }
            }
        }

        public static String Encrypt(String value)
        {
            using (SHA256 hash = SHA256Managed.Create())
            {
                return String.Concat(hash.ComputeHash(Encoding.UTF8.GetBytes(value)).Select(item => item.ToString("x2")));
            }
        }
    }
}
