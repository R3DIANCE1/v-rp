﻿using GTANetworkAPI;
using gtalife.src.Database.Models;
using gtalife.src.Player.Utils;
using System.Collections.Generic;
using System.Linq;

namespace gtalife.src.Player.Phone
{
    class Contact
    {
        public string Name;

        public int Level;
    }

    class Phone : Script
    {
        public static Dictionary<Client, Client> PlayersInCall = new Dictionary<Client, Client>();

        [ServerEvent(Event.ChatMessage)]
        public void OnChatMessage(Client player, string message)
        {
            if (PlayersInCall.ContainsKey(player))
            {
                var target = PlayersInCall[player];

                player.SendChatMessage($"~y~{player.Name}: {message}");
                target.SendChatMessage($"~y~{player.Name}: {message}");
            }
        }

        [ServerEvent(Event.PlayerDeath)]
        public void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            if (PlayersInCall.ContainsKey(player))
                PlayersInCall.Remove(player);

            if (PlayersInCall.ContainsValue(player))
            {
                var call = PlayersInCall.FirstOrDefault(x => x.Value == player);
                call.Key.SendChatMessage("~y~Teléfono: ~w~Se calló la llamada...");
                PlayersInCall.Remove(call.Key);
            }
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (PlayersInCall.ContainsKey(player))
                PlayersInCall.Remove(player);

            if (PlayersInCall.ContainsValue(player))
            {
                var call = PlayersInCall.FirstOrDefault(x => x.Value == player);
                call.Key.SendChatMessage("~y~Teléfono: ~w~Se calló la llamada...");
                PlayersInCall.Remove(call.Key);
            }
        }

        [RemoteEvent("RequestPhone")]
        public void RequestPhone(Client player, object[] arguments)
        {
            if (!player.IsLogged()) return;
            else if (player.getPhoneNumber() == null) return;

            player.TriggerEvent("ShowPhone");
        }

        [RemoteEvent("GetPhoneList")]
        public void GetPhoneList(Client player, object[] arguments)
        {
            if (PlayersInCall.ContainsKey(player) || PlayersInCall.ContainsValue(player)) return;
            PopulatePhoneContacts(player);
        }

        [RemoteEvent("CallPlayer")]
        public void CallPlayer(Client player, object[] arguments)
        {
            int targetPhone = (int)arguments[0];
            Call(player, targetPhone);
        }

        [RemoteEvent("AnswerCall")]
        public void AnswerCall(Client player, object[] arguments)
        {
            if (PlayersInCall.ContainsKey(player)) player.SendChatMessage("~r~ERROR: ~w~Ya estás en una llamada.");
            else if (!PlayersInCall.ContainsValue(player)) player.SendChatMessage("~r~ERROR: ~w~Nadie te está llamando.");
            else
            {
                var call = PlayersInCall.FirstOrDefault(x => x.Value == player);
                player.sendChatAction("contesto el telefono.");
                call.Key.SendChatMessage("~y~Teléfono: ~w~Contestaram.");
                PlayersInCall.Add(player, call.Key);
            }
        }

        [RemoteEvent("HangupPhone")]
        public void HangupPhone(Client player, object[] arguments)
        {
            if (PlayersInCall.ContainsKey(player))
            {
                PlayersInCall[player].SendChatMessage("~y~Teléfono: ~w~Ellos cuelgan.");
                PopulatePhoneContacts(PlayersInCall[player]);
                PlayersInCall.Remove(player);
            }

            if (PlayersInCall.ContainsValue(player))
            {
                foreach (var c in PlayersInCall.Where(x => x.Value == player).ToList())
                {
                    PlayersInCall.Remove(c.Key);
                }
            }
        }

        private void PopulatePhoneContacts(Client sender)
        {
            List<Contact> contacts = new List<Contact>();
            var contactlist = Data.Character[sender].Contacts.Where(x => x.Character == Data.Character[sender]).ToList();
            foreach (var contact in contactlist) contacts.Add(new Contact { Name = contact.Name, Level = contact.Phone });
            sender.TriggerEvent("PopulatePhoneList", NAPI.Util.ToJson(contacts));
        }

        public static void Call(Client sender, int number)
        {
            var players = NAPI.Pools.GetAllPlayers();
            Client target = null;

            Character character = null;
            foreach (var player in players)
            {
                if (!player.IsLogged()) continue;
                if (player.getPhoneNumber() != number) continue;

                target = player;
                break;
            }

            if (target == null)
            {
                sender.SendChatMessage("~y~Teléfono: ~w~El número llamado está ocupado o fuera de línea.");
                return;
            }
            else if (PlayersInCall.ContainsValue(target) || target == sender)
            {
                sender.SendChatMessage("~y~Teléfono: ~w~El número llamado está ocupado.");
                return;
            }

            PlayersInCall.Add(sender, target);
            target.sendChatAction("Teléfono sonando...", 1);
            target.SendChatMessage($"~y~Teléfono: ~w~{character.PhoneNumber} está llamando...");
            target.TriggerEvent("IncomingCall");
        }
    }
}
