﻿using System;
using System.Linq;
using System.Collections.Generic;
using GTANetworkAPI;

namespace gtalife.src.Player.Anims
{
    public class Anim
    {
        public string Name;
        public string AnimDict;
        public string AnimName;

        public Anim(string name, string anim_dict, string anim_name)
        {
            Name = name;
            AnimDict = anim_dict;
            AnimName = anim_name;
        }
    }

    public class Main : Script
    {
        // anim names taken from https://alexguirre.github.io/animations-list/
        List<Anim> PlayerAnims = new List<Anim>
        {
            new Anim("~r~Parar anim",               "",                                                             ""),
            new Anim("¿Donde estoy?",               "amb@world_human_guard_patrol@male@idle_a",                     "idle_c"),
            new Anim("¿Que es eso?",                "amb@world_human_guard_patrol@male@idle_b",                     "idle_e"),
            new Anim("AplaudirF",                   "amb@world_human_cheering@female_d",                            "base"),
            new Anim("AplaudirM",                   "amb@world_human_cheering@male_d",                              "base"),
            new Anim("Aplaudir sin ganas",          "amb@world_human_cheering@male_e",                              "base"),
            new Anim("Apoyarse en la pared",        "amb@world_human_leaning@male@wall@back@foot_up@base",          "base"),
            new Anim("Apoyarse manos juntas",       "amb@world_human_leaning@male@wall@back@hands_together@base",   "base"),
            new Anim("Apuntar en papel",            "amb@medic@standing@timeofdeath@base",                          "base"),
            new Anim("Arreglar bombilla",           "amb@prop_human_movie_bulb@idle_a",                             "idle_b"),
            new Anim("Avisar por radio",            "amb@code_human_police_investigate@idle_a",                     "idle_b"),
            new Anim("Brazos cruzadosF",            "amb@world_human_hang_out_street@female_arms_crossed@base",     "base"),
            new Anim("Brazos cruzadosM",            "amb@world_human_hang_out_street@male_c@base",                  "base"),
            new Anim("Borracho 1",                  "amb@world_human_bum_standing@drunk@idle_a",                    "idle_a"),
            new Anim("Borracho 2",                  "amb@world_human_bum_standing@drunk@idle_a",                    "idle_b"),
            new Anim("Borracho 3",                  "amb@world_human_bum_standing@drunk@idle_a",                    "idle_c"),
            new Anim("Celebrar",                    "amb@world_human_cheering@male_b",                              "base"),
            new Anim("Con ritmoM",                  "amb@world_human_partying@male@partying_beer@idle_a",           "idle_b"),
            new Anim("Con ritmoF",                  "amb@world_human_partying@female@partying_beer@base",           "base"),
            new Anim("Charlar",                     "amb@world_human_hang_out_street@male_a@idle_a",                "idle_c"),
            new Anim("Decepcion",                   "amb@world_human_bum_standing@depressed@idle_a",                "idle_c"),
            new Anim("Depresion",                   "amb@world_human_bum_standing@depressed@idle_a",                "idle_a"),
            new Anim("Estirar musculos",            "amb@world_human_muscle_flex@arms_at_side@idle_a",              "idle_a"),
            new Anim("Flexion",                     "amb@world_human_sit_ups@male@idle_a",                          "idle_a"),
            new Anim("Fumar apoyado 1",             "amb@world_human_leaning@male@wall@back@smoking@idle_a",        "idle_a"),
            new Anim("Fumar apoyado 2",             "amb@world_human_leaning@male@wall@back@smoking@idle_a",        "idle_b"),
            new Anim("Fumar apoyado 3",             "amb@world_human_leaning@male@wall@back@smoking@idle_a",        "idle_c"),
            new Anim("Fumar apoyado 4",             "amb@world_human_leaning@male@wall@back@smoking@idle_a",        "idle_d"),
            new Anim("Fumar apoyado 5",             "amb@world_human_leaning@female@smoke@idle_a",                  "idle_a"),
            new Anim("Fumar tranquilo",             "amb@world_human_aa_smoke@male@idle_a",                         "idle_a"),
            new Anim("Fumar tranquila",             "amb@world_human_leaning@female@smoke@idle_a",                  "idle_a"),
            new Anim("Grabar mobil",                "amb@world_human_mobile_film_shocking@male@idle_a",             "idle_c"),
            new Anim("Levantar Mano",               "mp_am_hold_up",                                                "handsup_base"),
            new Anim("Limpiar manos agachado",      "amb@world_human_bum_wash@male@high@idle_a",                    "idle_a"),
            new Anim("Me duele la espalda",         "amb@prop_human_bum_bin@idle_b",                                "idle_d"),
            new Anim("Mirar el suelo",              "amb@medic@standing@kneel@idle_a",                              "idle_a"),
            new Anim("Nervioso",                    "amb@world_human_bum_standing@twitchy@base",                    "base"),
            new Anim("Posar",                       "amb@world_human_muscle_flex@arms_in_front@idle_a",             "idle_a"),
            new Anim("SentarseM",                   "amb@world_human_picnic@male@idle_a",                           "idle_a"),
            new Anim("SentarseF",                   "amb@world_human_picnic@female@idle_a",                         "idle_a"),
            new Anim("Sentarse pies cruzados",      "amb@prop_human_seat_chair@female@arms_folded@base",            "base"),
            new Anim("Sentarse piernas cruzadas",   "amb@prop_human_seat_chair@female@legs_crossed@base",           "base"),
            new Anim("Sentarse con pie fuera",      "amb@prop_human_seat_chair@male@right_foot_out@base",           "base"),
            new Anim("Sentarse tímido",             "amb@prop_human_seat_chair@female@proper@base",                 "base"),
            new Anim("Sentarse cansado 1",          "amb@prop_human_seat_chair@male@left_elbow_on_knee@base",       "base"),
            new Anim("Sentarse cansado 2",          "amb@prop_human_seat_chair@male@elbows_on_knees@base",          "base"),
            new Anim("Sentarse apoyado",            "amb@prop_human_seat_chair@male@recline_b@base_b",              "base_b"),
            new Anim("Sentarse con musica",         "amb@prop_human_seat_strip_watch@bit_thuggy@base",              "base"),
            new Anim("Sentarse a disfrutar 1",      "amb@prop_human_seat_strip_watch@bit_thuggy@idle_a",            "idle_c"),
            new Anim("Sentarse a disfrutar 2",      "amb@prop_human_seat_strip_watch@bouncy_guy@idle_a",            "idle_a"),
            new Anim("Sentarse a disfrutar 3",      "amb@prop_human_seat_strip_watch@bouncy_guy@idle_a",            "idle_b"),
            new Anim("Sentarse a disfrutar 4",      "amb@prop_human_seat_strip_watch@bouncy_guy@idle_a",            "idle_c"),
            new Anim("Sentarse piernas abiertas",   "amb@prop_human_seat_chair@male@generic@base",                  "base"),
            new Anim("Timida",                      "amb@world_human_hang_out_street@female_hold_arm@base",         "base"),
            new Anim("Tranquilo tranquilo",         "amb@code_human_police_crowd_control@idle_a",                   "idle_c"),
            new Anim("Vigilar el area",             "amb@world_human_guard_patrol@male@idle_a",                     "idle_a")
        };

        [RemoteEvent("RequestAnims")]
        public void RequestAnims(Client player, object[] arguments)
        {
            player.TriggerEvent("ReceiveAnims", NAPI.Util.ToJson(PlayerAnims.Select(m => m.Name)));
        }

        [RemoteEvent("PlayAnim")]
        public void PlayAnim(Client player, object[] arguments)
        {
            if (arguments.Length < 1) return;

            int id = Convert.ToInt32(arguments[0]);
            if (id < 0 || id >= PlayerAnims.Count) return;

            if (id == 0)
            {
                player.StopAnimation();
                player.TriggerEvent("SetAnimPlaying", false);
            }
            else
            {
                player.PlayAnimation(PlayerAnims[id].AnimDict, PlayerAnims[id].AnimName, (int)(Flags.AnimationFlags.Loop));
                player.TriggerEvent("SetAnimPlaying", true);
            }
        }

        [RemoteEvent("StopPlayerAnim")]
        public void StopPlayerAnim(Client player, object[] arguments)
        {
            player.StopAnimation();
            player.TriggerEvent("SetAnimPlaying", false);
        }
    }
}
