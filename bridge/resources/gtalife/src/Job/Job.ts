﻿/// <reference path='../../types-gt-mp/index.d.ts' />

var is_on_job_marker = false;

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    if (eventName == "OnEnterJobMark") {
        is_on_job_marker = true;
    }
    else if (eventName == "OnLeaveJobMark") {
        is_on_job_marker = false;
    }
});

API.onKeyDown.connect(function (e, key) {
    if (API.isChatOpen()) return;

    switch (key.KeyCode) {
        case Keys.E:
            if (!is_on_job_marker) return;
            API.triggerServerEvent("JobInteract");
            break;
    }
});

API.onUpdate.connect(function () {
    if (is_on_job_marker) API.displaySubtitle("Pulsa ~y~E ~w~para obtener el trabajo.", 100);
});
