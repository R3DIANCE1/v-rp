﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;

namespace gtalife.src.Job.Taxi
{
    class TaxiVehicle : Script
    {

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            Vector3 position = new Vector3(-35.10684, 6423.967, 31.43228);

            NAPI.TextLabel.CreateTextLabel($"~y~Taxi~s~\nSolicitar vehículo\nPulsa ~y~E", position + new Vector3(0.0, 0.0, 0.5), 15f, 0.5f, 1, new Color(255, 255, 255, 255));
            NAPI.Marker.CreateMarker(1, position + new Vector3(0.0, 0.0, -1.0), new Vector3(0, 0, 0), new Vector3(0, 0, 0), 1, new Color(255, 255, 22, 22));

            var ColShape = NAPI.ColShape.CreateCylinderColShape(position, 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    if (player.HasData("TAXI_VEHICLE")) player.TriggerEvent("RequestingTaxi", 2);
                    else player.TriggerEvent("RequestingTaxi", 1);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.TriggerEvent("RequestingTaxi", 0);
                }
            };
        }

        [RemoteEvent("RequestTaxiVehicleMenu")]
        public void RequestTaxiVehicleMenu(Client sender, object[] arguments)
        {
            if (sender.GetJob() != (int)JobType.Taxi) sender.SendChatMessage("~r~ERROR: ~w~No eres un taxista.");
            else sender.TriggerEvent("ShowRequestTaxiMenu");
        }

        [RemoteEvent("RequestTaxi")]
        public void RequestTaxi(Client sender, object[] arguments)
        {
            if (sender.HasData("TAXI_VEHICLE")) NAPI.Entity.DeleteEntity(sender.GetData("TAXI_VEHICLE"));

            var vehicle = NAPI.Vehicle.CreateVehicle(VehicleHash.Taxi, new Vector3(-31.61203, 6420.537, 31.08215), new Vector3(0.5114706, 0.01082217, -136.3495), 42, 42);
            vehicle.EngineStatus = false;
            sender.SetData("TAXI_VEHICLE", vehicle);
        }

        [RemoteEvent("DeleteTaxi")]
        public void DeleteTaxi(Client sender, object[] arguments)
        {
            if (sender.HasData("TAXI_VEHICLE")) NAPI.Entity.DeleteEntity(sender.GetData("TAXI_VEHICLE"));
            sender.ResetData("TAXI_VEHICLE");
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.HasData("TAXI_VEHICLE")) NAPI.Entity.DeleteEntity(player.GetData("TAXI_VEHICLE"));
        }
    }
}
