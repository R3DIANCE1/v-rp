﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var blips = [];

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    switch (eventName) {
        case "ReceiveTaxiPlayers":
            var players = args[0];

            for (let i = 0; i < blips.length; i++) NAPI.Entity.DeleteEntity(blips[i]);
            blips = [];

            for (let i = 0; i < players.Count; i++) {
                var blip = NAPI.Blip.CreateBlip(API.getEntityPosition(players[i]));
                API.setBlipName(blip, "Pasajero");
                API.setBlipSprite(blip, 280);
                API.attachEntity(blip, players[i], "", new Vector3(), new Vector3());
                blips.push(blip);
            }
            break;
    }
});
