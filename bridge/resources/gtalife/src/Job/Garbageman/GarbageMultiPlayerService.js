"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var menu = null;
var blip = null;
var marker = null;
var label = null;
var bin = null;
var truck = null;
var jobProgress = 0;
var isCarrying = false;
var checkpointPosition = null;
var option = null;
var deliverPositions = [
    [
        { x: 53.28602, y: 6642.996, z: 31.5109 },
        { x: 26.1239, y: 6657.194, z: 31.53349 },
        { x: -16.11007, y: 6648.577, z: 31.13752 },
        { x: 0.4745062, y: 6618.522, z: 31.47032 },
        { x: -38.56599, y: 6627, z: 30.16893 },
        { x: -37.73915, y: 6605.806, z: 29.9519 },
        { x: -51.63889, y: 6586.045, z: 31.03278 },
        { x: -108.6273, y: 6536.781, z: 29.80953 },
        { x: -205.5201, y: 6450.417, z: 31.18685 },
        { x: -165.8547, y: 6436.796, z: 32.02013 },
        { x: -159.4652, y: 6430.207, z: 31.9159 },
        { x: -151.6298, y: 6423.775, z: 31.91588 },
        { x: -149.3074, y: 6417.862, z: 31.97451 },
        { x: -157.8998, y: 6408.347, z: 32.02832 },
        { x: -188.021, y: 6413.757, z: 31.89175 },
        { x: -215.25, y: 6401.21, z: 31.68499 },
        { x: -229.2692, y: 6419.783, z: 31.26386 },
        { x: -226.7671, y: 6380.927, z: 31.54446 },
        { x: -250.7308, y: 6368.601, z: 31.48125 },
        { x: -272.8536, y: 6396.745, z: 30.92462 },
        { x: -279.4205, y: 6352.959, z: 32.48915 },
        { x: -307.3243, y: 6336.005, z: 31.28739 },
        { x: -361.2787, y: 6321.734, z: 29.75638 },
        { x: -315.5016, y: 6315.489, z: 32.2038 }
    ],
    [
        { x: 53.28602, y: 6642.996, z: 31.5109 },
        { x: 26.1239, y: 6657.194, z: 31.53349 },
        { x: -16.11007, y: 6648.577, z: 31.13752 },
        { x: 0.4745062, y: 6618.522, z: 31.47032 },
        { x: -38.56599, y: 6627, z: 30.16893 },
        { x: -37.73915, y: 6605.806, z: 29.9519 },
        { x: -51.63889, y: 6586.045, z: 31.03278 },
        { x: -108.6273, y: 6536.781, z: 29.80953 },
        { x: -205.5201, y: 6450.417, z: 31.18685 },
        { x: -165.8547, y: 6436.796, z: 32.02013 },
        { x: -159.4652, y: 6430.207, z: 31.9159 },
        { x: -151.6298, y: 6423.775, z: 31.91588 },
        { x: -149.3074, y: 6417.862, z: 31.97451 },
        { x: -157.8998, y: 6408.347, z: 32.02832 },
        { x: -188.021, y: 6413.757, z: 31.89175 },
        { x: -215.25, y: 6401.21, z: 31.68499 },
        { x: -229.2692, y: 6419.783, z: 31.26386 },
        { x: -226.7671, y: 6380.927, z: 31.54446 },
        { x: -250.7308, y: 6368.601, z: 31.48125 },
        { x: -272.8536, y: 6396.745, z: 30.92462 },
        { x: -279.4205, y: 6352.959, z: 32.48915 },
        { x: -307.3243, y: 6336.005, z: 31.28739 },
        { x: -361.2787, y: 6321.734, z: 29.75638 },
        { x: -315.5016, y: 6315.489, z: 32.2038 }
    ]
];
API.onServerEventTrigger.connect((eventName, args) => {
    if (eventName == "ShowGarbageMenu") {
        if (menu == null) {
            menu = API.createMenu("Basurero Servicio", "Selecciona un jugador para ayudarte", 0, 0, 6);
        }
        var players = args[0];
        if (players.Count > 0) {
            menu.Clear();
            for (var i = 0; i < players.Count; i++) {
                var item = API.createMenuItem(players[i], "");
                item.Activated.connect((sender, selectedItem) => {
                    API.triggerServerEvent("GarbageSelectPlayer", players[i]);
                    menu.Visible = false;
                });
                menu.AddItem(item);
            }
            menu.Visible = true;
        }
        else
            API.SendChatMessage("~r~ERROR: ~w~No hay basureros disponibles cerca de ti.");
    }
    else if (eventName == "HideGarbageMenu") {
        if (menu == null)
            return;
        menu.Visible = false;
    }
    else if (eventName == "StartGarbageJob") {
        deleteGarbageServiceEntities();
        var x = args[0];
        var y = args[1];
        var z = args[2];
        blip = NAPI.Blip.CreateBlip(new Vector3(x, y, z));
        API.setBlipSprite(blip, 67);
        API.setBlipName(blip, "Mi camión");
        truck = null;
        jobProgress = 0;
    }
    else if (eventName == "ResetGarbageJob") {
        deleteGarbageServiceEntities();
        jobProgress = 0;
        truck = null;
        bin = null;
        isCarrying = false;
        checkpointPosition = null;
        option = null;
    }
    else if (eventName == "OnPlayerEnterGarbageJobVehicle") {
        if (truck == null) {
            deleteGarbageServiceEntities();
            truck = API.getPlayerVehicle(API.getLocalPlayer());
            checkpointPosition = new Vector3(deliverPositions[option][jobProgress].x, deliverPositions[option][jobProgress].y, deliverPositions[option][jobProgress].z);
            blip = NAPI.Blip.CreateBlip(checkpointPosition);
            API.setBlipSprite(blip, 1);
            API.setBlipColor(blip, 1);
            API.showBlipRoute(blip, true);
            //marker = NAPI.Marker.CreateMarker(1, new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z - 1.0), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
            label = NAPI.TextLabel.CreateTextLabel("~y~Recoger la bolsa~n~~w~Pulsa ~y~E", new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z + 0.5), 15, 0.5);
            bin = NAPI.Object.CreateObject(-1998455445, new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z - 0.75), new Vector3());
            jobProgress++;
        }
    }
});
API.onKeyDown.connect(function (sender, key) {
    if (API.isChatOpen())
        return;
    else if (jobProgress == 0)
        return;
    else if (API.isPlayerInAnyVehicle(API.getLocalPlayer()))
        return;
    switch (key.KeyCode) {
        case Keys.E:
            if (jobProgress > 0 && jobProgress < deliverPositions[option].length) {
                if (API.getEntityPosition(API.getLocalPlayer()).DistanceTo(checkpointPosition) < 1) {
                    deleteGarbageServiceEntities();
                    if (!isCarrying) {
                        API.triggerServerEvent("GarbageCarryBagAnimPlay");
                        checkpointPosition = API.getEntityRearPosition(truck);
                    }
                    else {
                        NAPI.Entity.DeleteEntity(bin);
                        API.triggerServerEvent("GarbageCarryBagAnimStop");
                        checkpointPosition = new Vector3(deliverPositions[option][jobProgress].x, deliverPositions[option][jobProgress].y, deliverPositions[option][jobProgress].z);
                        jobProgress++;
                    }
                    API.playSoundFrontEnd("SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                    if (jobProgress == deliverPositions[option].length) {
                        checkpointPosition = new Vector3(148.8305, 6362.312, 31.52921);
                        marker = NAPI.Marker.CreateMarker(1, new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
                        label = NAPI.TextLabel.CreateTextLabel("~y~Finalizar trabajo~n~~w~Pulsa ~y~E", new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z + 1.0), 15, 0.5);
                        blip = NAPI.Blip.CreateBlip(new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z));
                        API.setBlipSprite(blip, 1);
                        API.setBlipColor(blip, 1);
                        API.showBlipRoute(blip, true);
                        return;
                    }
                    if (!isCarrying) {
                        marker = NAPI.Marker.CreateMarker(1, new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z - 0.75), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
                        label = NAPI.TextLabel.CreateTextLabel("~y~Cargar camión~n~~w~Pulsa ~y~E", new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z), 15, 0.5);
                    }
                    else {
                        bin = NAPI.Object.CreateObject(-1998455445, new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z - 0.75), new Vector3());
                        label = NAPI.TextLabel.CreateTextLabel("~y~Recoger la bolsa~n~~w~Pulsa ~y~E", new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z), 15, 0.5);
                    }
                    isCarrying = !isCarrying;
                    blip = NAPI.Blip.CreateBlip(new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z));
                    API.setBlipSprite(blip, 1);
                    API.setBlipColor(blip, 1);
                }
            }
            else if (jobProgress == deliverPositions[option].length) {
                if (API.getEntityPosition(API.getLocalPlayer()).DistanceTo(checkpointPosition) < 4) {
                    deleteGarbageServiceEntities();
                    option = null;
                    truck = null;
                    jobProgress = 0;
                    isCarrying = false;
                    checkpointPosition = null;
                    API.playSoundFrontEnd("SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                    API.triggerServerEvent("OnFinishGarbageJob");
                }
            }
            break;
    }
});
function deleteGarbageServiceEntities() {
    if (blip != null) {
        NAPI.Entity.DeleteEntity(blip);
        blip = null;
    }
    if (label != null) {
        NAPI.Entity.DeleteEntity(label);
        label = null;
    }
    if (marker != null) {
        NAPI.Entity.DeleteEntity(marker);
        marker = null;
    }
    if (bin != null) {
        NAPI.Entity.DeleteEntity(bin);
    }
}
