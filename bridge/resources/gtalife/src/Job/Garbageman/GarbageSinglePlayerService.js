"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var menu = null;
var blip = null;
var marker = null;
var position = null;
API.onServerEventTrigger.connect((eventName, args) => {
    switch (eventName) {
        case "job@basurero:singleplayer_show":
            if (menu == null) {
                menu = API.createMenu("Basurero Servicio", " ", 0, 0, 6);
                menu.OnItemSelect.connect(function (menu, item, index) {
                    API.triggerServerEvent("job@basurero:singleplayer_interact");
                    menu.Visible = false;
                });
            }
            menu.Clear();
            menu.AddItem(API.createMenuItem(args[0] == false ? "Iniciar el servicio" : "Terminar trabajo", ""));
            menu.Visible = true;
            break;
        case "job@basurero:singleplayer_hide":
            if (menu != null)
                menu.Visible = false;
            break;
        case "job@basurero:singleplayer_setcheckpoint":
            position = args[0];
            jbb_setCheckpoint(position);
            break;
        case "job@basurero:singleplayer_reset":
            jbb_resetCheckpoint();
            break;
    }
});
API.onKeyDown.connect((sender, e) => {
    if (e.KeyCode === Keys.E) {
        var player = API.getLocalPlayer();
        if (position == null || API.isPlayerInAnyVehicle(player))
            return;
        if (API.getEntityPosition(player).DistanceTo(position) < 1) {
            jbb_resetCheckpoint();
            API.triggerServerEvent("job@basurero:singleplayer_sweep");
        }
    }
});
API.onUpdate.connect(() => {
    if (position == null)
        return;
    var player = API.getLocalPlayer();
    if (API.getEntityPosition(player).DistanceTo(position) < 1) {
        API.displaySubtitle("Pulsa ~y~E ~w~para barrer", 100);
    }
});
function jbb_setCheckpoint(position) {
    marker = NAPI.Marker.CreateMarker(1, new Vector3(position.X, position.Y, position.Z - 1.0), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
    blip = NAPI.Blip.CreateBlip(position);
    API.setBlipSprite(blip, 1);
    API.setBlipColor(blip, 1);
}
function jbb_resetCheckpoint() {
    if (NAPI.Entity.DoesEntityExist(marker))
        NAPI.Entity.DeleteEntity(marker);
    if (NAPI.Entity.DoesEntityExist(blip))
        NAPI.Entity.DeleteEntity(blip);
    blip = null;
    marker = null;
    position = null;
}
