"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
let box_id = 0;
let job_started = false;
var box = null;
var blip = null;
var label = null;
let box_positions = [
    { x: -17.31082, y: 6474.353, z: 34.64059 },
    { x: -9.069155, y: 6494.948, z: 36.6614 },
    { x: 9.414523, y: 6494.594, z: 31.66847 },
    { x: -159.8972, y: 6311.881, z: 35.35175 },
    { x: 444.6987, y: 6456.06, z: 28.79993 },
    { x: 1551.187, y: 6330.166, z: 24.08256 },
    { x: 1982.881, y: 5175.771, z: 47.63908 },
    { x: 2031.409, y: 4733.41, z: 41.61059 },
    { x: 1592.012, y: 3587.67, z: 42.11654 },
    { x: 1578.903, y: 3614.953, z: 42.11666 },
    { x: -604.1417, y: 5367.197, z: 70.64909 }
];
API.onServerEventTrigger.connect((eventName, args) => {
    if (eventName == "StartArmsDealerJob") {
        if (job_started) {
            API.SendChatMessage("~r~ERROR: ~w~Ya estás en un servicio.");
            return;
        }
        job_started = true;
        box_id = Math.floor((Math.random() * box_positions.length));
        label = NAPI.TextLabel.CreateTextLabel("~y~Materiales~n~~w~Pulsa ~y~E", new Vector3(box_positions[box_id].x, box_positions[box_id].y, box_positions[box_id].z + 0.5), 15, 0.5);
        box = NAPI.Object.CreateObject(1302435108, new Vector3(box_positions[box_id].x, box_positions[box_id].y, box_positions[box_id].z - 0.75), new Vector3());
        blip = NAPI.Blip.CreateBlip(new Vector3(box_positions[box_id].x, box_positions[box_id].y, box_positions[box_id].z));
        API.setBlipSprite(blip, 478);
        API.setBlipColor(blip, 50);
        API.showBlipRoute(blip, true);
        API.SendChatMessage("~y~INFO: ~w~El proveedor marcó en su gps la ubicación de los materiales.");
    }
    else if (eventName == "ResetArmsDealerJob") {
        job_started = false;
        if (NAPI.Entity.DoesEntityExist(label))
            NAPI.Entity.DeleteEntity(label);
        if (NAPI.Entity.DoesEntityExist(blip))
            NAPI.Entity.DeleteEntity(blip);
        if (NAPI.Entity.DoesEntityExist(box))
            NAPI.Entity.DeleteEntity(box);
    }
});
API.onKeyDown.connect((sender, e) => {
    if (!job_started)
        return;
    var player = API.getLocalPlayer();
    if (e.KeyCode === Keys.E) {
        if (new Vector3(box_positions[box_id].x, box_positions[box_id].y, box_positions[box_id].z).DistanceTo(API.getEntityPosition(player)) < 2) {
            NAPI.Entity.DeleteEntity(label);
            NAPI.Entity.DeleteEntity(box);
            NAPI.Entity.DeleteEntity(blip);
            job_started = false;
            API.triggerServerEvent("OnTakeMaterialBox");
        }
    }
});
