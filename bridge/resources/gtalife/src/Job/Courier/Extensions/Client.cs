﻿using GTANetworkAPI;

namespace gtalife.src.Job.Courier
{
    public static class ClientExtensions
    {
        public static void sendErrorMessage(this Client player, string message)
        {
            NAPI.Native.SendNativeToPlayer(player, Hash._SET_NOTIFICATION_BACKGROUND_COLOR, 6);
            NAPI.Player.PlaySoundFrontEnd(player, "OTHER_TEXT", "HUD_AWARDS");
            player.SendNotification(message, false);
        }

        public static bool isCarrying(this Client player)
        {
            return player.HasData("Courier_CarryingType");
        }

        // use isCarrying before this!
        public static ProductType getCarryingType(this Client player)
        {
            return (ProductType)player.GetData("Courier_CarryingType");
        }

        public static void startCarrying(this Client player, ProductType type, NetHandle? objHandle = null)
        {
            if (player.HasData("Courier_CarryingType") || !ProductDefinitions.Products.ContainsKey(type)) return;

            Object attachObj = (objHandle == null) ? NAPI.Object.CreateObject(NAPI.Util.GetHashKey(ProductDefinitions.Products[type].PropName), player.Position, new Vector3()) : NAPI.Entity.GetEntityFromHandle<Object>(objHandle.Value);
            if (attachObj == null) return;

            attachObj.AttachTo(player.Handle, "PH_R_Hand", Offsets.PlayerAttachmentOffsets[type].Item1, Offsets.PlayerAttachmentOffsets[type].Item2);
            player.PlayAnimation("anim@heists@box_carry@", "idle", (int)(AnimationFlags.Loop | AnimationFlags.UpperBodyOnly | AnimationFlags.AllowRotation));
            player.SetData("Courier_CarryingType", (int)type);
            player.SetData("Courier_BoxHandle", attachObj.Handle);
            player.TriggerEvent("Courier_SetCarryingState", true, attachObj.Model);
            NAPI.Native.SendNativeToPlayer(player, Hash.SET_CURRENT_PED_WEAPON, player.Handle, WeaponHash.Unarmed, true);
        }

        public static void stopCarrying(this Client player, bool deleteObj = true)
        {
            if (!player.HasData("Courier_CarryingType")) return;

            if (deleteObj)
            {
                Object attachObj = NAPI.Entity.GetEntityFromHandle<Object>(player.GetData("Courier_BoxHandle"));
                attachObj?.Delete();
            }

            player.StopAnimation();
            player.ResetData("Courier_CarryingType");
            player.ResetData("Courier_BoxHandle");
            player.TriggerEvent("Courier_SetCarryingState", false, -1);
        }
    }
}
