﻿using System.Linq;
using GTANetworkAPI;

namespace gtalife.src.Job.Courier
{
    public static class VehicleExtensions
    {
        private static void vehicleProductsCheck(this Vehicle vehicle)
        {
            int loadedItems = Main.LoadedProducts[vehicle.Handle].Count(p => p != null); // count non-null items
            if (loadedItems < 1) Main.LoadedProducts.Remove(vehicle.Handle); // remove vehicle from dictionary if it has no products
        }

        public static LoadedProduct getLoadedProduct(this Vehicle vehicle, int index)
        {
            if (!Main.LoadedProducts.ContainsKey(vehicle.Handle)) return null;
            if (index < 0 || index >= Main.LoadedProducts[vehicle.Handle].Length) return null;
            return Main.LoadedProducts[vehicle.Handle][index];
        }

        public static LoadedProduct[] getLoadedProducts(this Vehicle vehicle)
        {
            if (!Offsets.VehicleAttachmentOffsets.ContainsKey((VehicleHash)vehicle.Model)) return null;
            if (!Main.LoadedProducts.ContainsKey(vehicle.Handle)) Main.LoadedProducts.Add(vehicle.Handle, new LoadedProduct[Offsets.VehicleAttachmentOffsets[(VehicleHash)vehicle.Model].Count]);
            return Main.LoadedProducts[vehicle.Handle];
        }

        public static bool loadProduct(this Vehicle vehicle, int index, ProductType type, NetHandle? objHandle = null)
        {
            VehicleHash vehicleModel = (VehicleHash)vehicle.Model;

            if (!Offsets.VehicleAttachmentOffsets.ContainsKey(vehicleModel)) return false;
            if (!Main.LoadedProducts.ContainsKey(vehicle.Handle)) Main.LoadedProducts.Add(vehicle.Handle, new LoadedProduct[Offsets.VehicleAttachmentOffsets[vehicleModel].Count]);

            if (index < 0 || index >= Main.LoadedProducts[vehicle.Handle].Length) return false;
            if (Main.LoadedProducts[vehicle.Handle][index] != null) return false;

            NetHandle handle = (objHandle == null) ? NAPI.Object.CreateObject(NAPI.Util.GetHashKey(ProductDefinitions.Products[type].PropName), vehicle.Position, new Vector3()) : objHandle.Value;
            Main.LoadedProducts[vehicle.Handle][index] = new LoadedProduct(type, handle);

            Main.LoadedProducts[vehicle.Handle][index].Object.ResetSharedData("Courier_DetectorID");
            Main.LoadedProducts[vehicle.Handle][index].Object.AttachTo(
                vehicle.Handle,
                null,
                Offsets.VehicleAttachmentOffsets[vehicleModel][index].Item1 + ProductDefinitions.Products[type].VehOffset,
                Offsets.VehicleAttachmentOffsets[vehicleModel][index].Item1 + ProductDefinitions.Products[type].VehRotation
            );

            return true;
        }

        public static bool unloadProduct(this Vehicle vehicle, Client toPlayer, int index)
        {
            if (!Main.LoadedProducts.ContainsKey(vehicle.Handle)) return false;
            if (index < 0 || index >= Main.LoadedProducts[vehicle.Handle].Length) return false;
            if (toPlayer.isCarrying()) return false;
            if (Main.LoadedProducts[vehicle.Handle][index] == null) return false;

            Main.LoadedProducts[vehicle.Handle][index].Object.Detach();
            toPlayer.startCarrying(Main.LoadedProducts[vehicle.Handle][index].Type, Main.LoadedProducts[vehicle.Handle][index].Object.Handle);
            Main.LoadedProducts[vehicle.Handle][index] = null;

            vehicleProductsCheck(vehicle);
            return true;
        }

        public static void removeLoadedProduct(this Vehicle vehicle, int index)
        {
            if (!Main.LoadedProducts.ContainsKey(vehicle.Handle)) return;
            if (index < 0 || index >= Main.LoadedProducts[vehicle.Handle].Length) return;

            Main.LoadedProducts[vehicle.Handle][index].Object?.Delete();
            Main.LoadedProducts[vehicle.Handle][index] = null;
            vehicleProductsCheck(vehicle);
        }
    }
}
