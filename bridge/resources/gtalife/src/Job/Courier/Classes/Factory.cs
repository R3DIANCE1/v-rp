﻿using System;
using System.IO;
using System.Timers;
using GTANetworkAPI;
using Newtonsoft.Json;

namespace gtalife.src.Job.Courier
{
    public class Factory
    {
        public Guid ID { get; set; }
        public Vector3 Position { get; set; }
        public ProductType Type { get; set; }

        [JsonIgnore]
        private string LabelText => $"~b~Fábrica de {ProductDefinitions.Products[Type].Name}~n~~n~~w~Precio unitario: ~g~${ProductDefinitions.Products[Type].Price:n0}~n~~w~Stock: ~w~{Stock:n0}";

        [JsonIgnore]
        public string FilePath => Main.ResourceFolder + Path.DirectorySeparatorChar + "data/FactoryData" + Path.DirectorySeparatorChar + ID + ".json";

        [JsonIgnore]
        private int _stock;

        public int Stock
        {
            get { return _stock; }

            set
            {
                _stock = value;
                if (Label != null) Label.Text = LabelText;

                // Start making product if no stock left (will stop when factory reaches X product)
                if (_stock < 1 && RefillTimer == null)
                {
                    RefillTimer = new Timer();
                    RefillTimer.Elapsed += new ElapsedEventHandler(OnRefillTimerUpdate);
                    RefillTimer.Interval = Main.FactoryProductInterval * 1000;
                    RefillTimer.Enabled = true;
                }
            }
        }

        private void OnRefillTimerUpdate(object sender, ElapsedEventArgs e)
        {
            Stock++;
            if (Stock >= Main.FactoryMaxAutoStock) KillTimer();
        }

        [JsonIgnore]
        private DateTime _lastSave;

        [JsonIgnore]
        public Blip Blip { get; set; }

        [JsonIgnore]
        public Marker Marker { get; set; }

        [JsonIgnore]
        public TextLabel Label { get; set; }

        [JsonIgnore]
        public ColShape ColShape { get; set; }

        [JsonIgnore]
        private Timer RefillTimer { get; set; }

        public Factory(Guid id, Vector3 position, ProductType type, int stock = 0)
        {
            ID = id;
            Position = position;
            Type = type;
            Stock = stock;
        }

        public void CreateEntities()
        {
            // blip
            Blip = NAPI.Blip.CreateBlip(Position);
            Blip.Name = $"Fábrica de {ProductDefinitions.Products[Type].Name}";
            Blip.Sprite = 557;
            Blip.Color = 18;
            Blip.ShortRange = true;
            Blip.Scale = 1f;

            // marker
            Marker = NAPI.Marker.CreateMarker(1, Position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 3, new Color(150, 174, 219, 242));

            // label
            Label = NAPI.TextLabel.CreateTextLabel(LabelText, Position + new Vector3(0.0, 0.0, 0.5), 20f, 0.5f, 1, new Color(255, 255, 255, 255));

            // colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(Position, 1.5f, 2f);
            ColShape.OnEntityEnterColShape += (shape, entityHandle) =>
            {
                Client player = null;

                if ((player = NAPI.Player.GetPlayerFromHandle(entityHandle)) != null)
                {
                    player.SetData("Courier_MarkerID", ID);
                    player.TriggerEvent("Courier_SetMarkerType", (int)MarkerType.Factory);
                }
            };

            ColShape.OnEntityExitColShape += (shape, entityHandle) =>
            {
                Client player = null;

                if ((player = NAPI.Player.GetPlayerFromHandle(entityHandle)) != null)
                {
                    player.ResetData("Courier_MarkerID");
                    player.TriggerEvent("Courier_SetMarkerType", (int)MarkerType.None);
                }
            };
        }

        private void KillTimer()
        {
            if (RefillTimer != null) RefillTimer.Dispose();
            RefillTimer = null;
        }

        public void DeleteEntities()
        {
            Blip?.Delete();
            Marker?.Delete();
            Label?.Delete();
            if (ColShape != null) NAPI.ColShape.DeleteColShape(ColShape);
            KillTimer();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(_lastSave).TotalSeconds < Main.SaveInterval) return;

            File.WriteAllText(FilePath, JsonConvert.SerializeObject(this, Formatting.Indented));
            _lastSave = DateTime.Now;
        }
    }
}
