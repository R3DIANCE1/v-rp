﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using GTANetworkAPI;
using Newtonsoft.Json;
using gtalife.src.Player.Utils;

namespace gtalife.src.Job.Courier
{
    public class Main : Script
    {
        // You can change these from meta.xml
        public static int SaveInterval = 120;
        public static int WorldPropLife = 10;
        public static int FactoryProductInterval = 180;
        public static int FactoryMaxAutoStock = 10;
        public static int BuyerSaleInterval = 180;
        public static int BuyerMinStockPercentage = 50;

        // Don't touch these
        public static string ResourceFolder;
        public static List<Factory> Factories = new List<Factory>();
        public static List<Buyer> Buyers = new List<Buyer>();
        public static List<WorldProduct> DroppedProduct = new List<WorldProduct>();
        public static Dictionary<NetHandle, LoadedProduct[]> LoadedProducts = new Dictionary<NetHandle, LoadedProduct[]>();
        public Timer WorldCleaner = null;

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            ResourceFolder = NAPI.Resource.GetResourceFolder(this);

            // Load settings
            if (NAPI.Resource.HasSetting(this, "saveInterval")) SaveInterval = NAPI.Resource.GetSetting<int>(this, "saveInterval");
            if (NAPI.Resource.HasSetting(this, "worldPropLife")) WorldPropLife = NAPI.Resource.GetSetting<int>(this, "worldPropLife");
            if (NAPI.Resource.HasSetting(this, "factoryProductInterval")) FactoryProductInterval = NAPI.Resource.GetSetting<int>(this, "factoryProductInterval");
            if (NAPI.Resource.HasSetting(this, "factoryMaxAutoStock")) FactoryMaxAutoStock = NAPI.Resource.GetSetting<int>(this, "factoryMaxAutoStock");
            if (NAPI.Resource.HasSetting(this, "buyerSaleInterval")) BuyerSaleInterval = NAPI.Resource.GetSetting<int>(this, "buyerSaleInterval");
            if (NAPI.Resource.HasSetting(this, "buyerMinStockPercentage")) BuyerMinStockPercentage = NAPI.Resource.GetSetting<int>(this, "buyerMinStockPercentage");

            // Print settings
            NAPI.Util.ConsoleOutput("-> Save Interval: {0}", TimeSpan.FromSeconds(SaveInterval).ToString(@"hh\:mm\:ss"));
            NAPI.Util.ConsoleOutput("-> Dropped Item Lifetime: {0}", TimeSpan.FromMinutes(WorldPropLife).ToString(@"hh\:mm\:ss"));
            NAPI.Util.ConsoleOutput("-> Factory Product Interval: {0}", TimeSpan.FromSeconds(FactoryProductInterval).ToString(@"hh\:mm\:ss"));
            NAPI.Util.ConsoleOutput("-> Factory Max. Auto Stock: {0}", FactoryMaxAutoStock);
            NAPI.Util.ConsoleOutput("-> Buyer Sale Interval: {0}", TimeSpan.FromSeconds(BuyerSaleInterval).ToString(@"hh\:mm\:ss"));
            NAPI.Util.ConsoleOutput("-> Buyer Min. Stock Percentage: {0}%", BuyerMinStockPercentage);

            // Load factories
            string factoryDir = ResourceFolder + Path.DirectorySeparatorChar + "data/FactoryData";
            if (!Directory.Exists(factoryDir)) Directory.CreateDirectory(factoryDir);

            foreach (string file in Directory.EnumerateFiles(factoryDir, "*.json"))
            {
                Factory newFactory = JsonConvert.DeserializeObject<Factory>(File.ReadAllText(file));
                Factories.Add(newFactory);

                newFactory.CreateEntities();
            }

            NAPI.Util.ConsoleOutput("Loaded {0} factories.", Factories.Count);

            // Load buyers
            string buyerDir = ResourceFolder + Path.DirectorySeparatorChar + "data/BuyerData";
            if (!Directory.Exists(buyerDir)) Directory.CreateDirectory(buyerDir);

            foreach (string file in Directory.EnumerateFiles(buyerDir, "*.json"))
            {
                Buyer newBuyer = JsonConvert.DeserializeObject<Buyer>(File.ReadAllText(file));
                Buyers.Add(newBuyer);

                newBuyer.CreateEntities();
            }

            NAPI.Util.ConsoleOutput("Loaded {0} buyers.", Buyers.Count);

            // Start world cleaner
            WorldCleaner = new Timer();
            WorldCleaner.Elapsed += new ElapsedEventHandler(OnWorldCleanerTimerUpdate);
            WorldCleaner.Interval = 60000;
            WorldCleaner.Enabled = true;
        }

        private void OnWorldCleanerTimerUpdate(object sender, ElapsedEventArgs e)
        {
            foreach (WorldProduct product in DroppedProduct)
            {
                if (DateTime.Now.Subtract(product.SpawnTime).TotalMinutes >= WorldPropLife) product.DeleteEntities();
            }

            DroppedProduct.RemoveAll(p => DateTime.Now.Subtract(p.SpawnTime).TotalMinutes >= WorldPropLife);
        }

        [ServerEvent(Event.PlayerDeath)]
        public void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            if (player.isCarrying()) player.stopCarrying(true);
        }

        [RemoteEvent("Courier_RequestVehicleProducts")]
        public void CourierRequestVehicleProducts(Client player, object[] arguments)
        {
            if (arguments.Length < 1) return;

            NetHandle vehHandle = (NetHandle)arguments[0];
            Vehicle vehicle = NAPI.Entity.GetEntityFromHandle<Vehicle>(vehHandle);
            if (vehicle == null)
            {
                player.sendErrorMessage("Vehículo inválido.");
                return;
            }

            player.TriggerEvent("Courier_ReceiveVehicleProducts", NAPI.Util.ToJson(vehicle.getLoadedProducts()));
        }

        [RemoteEvent("Courier_LoadToVehicle")]
        public void CourierLoadToVehicle(Client player, object[] arguments)
        {
            if (arguments.Length < 2) return;

            if (!player.isCarrying())
            {
                player.sendErrorMessage("No estás llevando un producto.");
                return;
            }

            NetHandle vehHandle = (NetHandle)arguments[0];
            Vehicle vehicle = NAPI.Entity.GetEntityFromHandle<Vehicle>(vehHandle);
            if (vehicle == null)
            {
                player.sendErrorMessage("Vehículo inválido.");
                return;
            }

            if (player.Position.DistanceTo(vehicle.Position) > 5f)
            {
                player.sendErrorMessage("El vehículo está muy lejos.");
                return;
            }

            int idx = Convert.ToInt32(arguments[1]);
            if (vehicle.loadProduct(idx, player.getCarryingType(), (NetHandle?)player.GetData("Courier_BoxHandle")))
            {
                player.stopCarrying(false);
            }
            else
            {
                player.sendErrorMessage("No se pudo cargar su producto al vehículo.");
            }
        }

        [RemoteEvent("Courier_TakeFromVehicle")]
        public void CourierTakeFromVehicle(Client player, object[] arguments)
        {
            if (arguments.Length < 2) return;

            if (player.isCarrying())
            {
                player.sendErrorMessage("Ya llevas un producto.");
                return;
            }

            NetHandle vehHandle = (NetHandle)arguments[0];
            Vehicle vehicle = NAPI.Entity.GetEntityFromHandle<Vehicle>(vehHandle);
            if (vehicle == null)
            {
                player.sendErrorMessage("Vehículo inválido.");
                return;
            }

            if (player.Position.DistanceTo(vehicle.Position) > 5f)
            {
                player.sendErrorMessage("El vehículo está muy lejos.");
                return;
            }

            int idx = Convert.ToInt32(arguments[1]);
            if (!vehicle.unloadProduct(player, idx))
            {
                player.sendErrorMessage("No se pudo cargar su producto al vehículo.");
                return;
            }
        }

        [RemoteEvent("Courier_Drop")]
        public void CourierDrop(Client player, object[] arguments)
        {
            if (arguments.Length < 1) return;

            if (!player.isCarrying())
            {
                player.sendErrorMessage("No estás llevando un producto.");
                return;
            }

            Vector3 position = (Vector3)arguments[0];
            if (player.Position.DistanceTo(position) >= 5)
            {
                player.sendErrorMessage("No puedes dejar caer tu producto.");
                return;
            }

            DroppedProduct.Add(new WorldProduct(player.getCarryingType(), (NetHandle?)player.GetData("Courier_BoxHandle"), position, player.Rotation));
            player.stopCarrying(false);
        }

        [RemoteEvent("Courier_Take")]
        public void CourierTake(Client player, object[] arguments)
        {
            if (arguments.Length < 1) return;

            if (player.isCarrying())
            {
                player.sendErrorMessage("Ya tienes un producto.");
                return;
            }

            Guid productID = new Guid(arguments[0].ToString());
            WorldProduct product = DroppedProduct.FirstOrDefault(p => p.ID == productID);
            if (product == null)
            {
                player.sendErrorMessage("Producto inválido.");
                return;
            }

            product.DeleteEntities(false);
            product.Object.ResetSharedData("Courier_DetectorID");
            player.startCarrying(product.Type, product.Object.Handle);
            DroppedProduct.Remove(product);
        }

        [RemoteEvent("Courier_FactoryBuy")]
        public void CourierFactoryBuy(Client player, object[] arguments)
        {
            if (player.GetJob() != (int)JobType.Courier)
            {
                player.sendErrorMessage("No eres un repartidor.");
                return;
            }

            if (player.isCarrying())
            {
                player.sendErrorMessage("Ya tienes un producto.");
                return;
            }

            if (!player.HasData("Courier_MarkerID"))
            {
                player.sendErrorMessage("No estás en el marcador de una fábrica.");
                return;
            }

            Factory factory = Methods.GetFactory(player.GetData("Courier_MarkerID"));
            if (factory == null)
            {
                player.sendErrorMessage("Fábrica inválida.");
                return;
            }

            if (player.getMoney() < ProductDefinitions.Products[factory.Type].Price)
            {
                player.sendErrorMessage("No puedes pagar este producto.");
                return;
            }

            if (factory.Stock < 1)
            {
                player.sendErrorMessage("Esta fábrica no tiene producto.");
                return;
            }

            player.giveMoney(-ProductDefinitions.Products[factory.Type].Price);
            player.startCarrying(factory.Type);

            factory.Stock--;
            factory.Save();
        }

        [RemoteEvent("Courier_FactoryRefund")]
        public void CourierFactoryRefund(Client player, object[] arguments)
        {
            if (!player.isCarrying())
            {
                player.sendErrorMessage("No estás llevando un producto.");
                return;
            }

            if (!player.HasData("Courier_MarkerID"))
            {
                player.sendErrorMessage("No estás en el marcador de una fábrica.");
                return;
            }

            Factory factory = Methods.GetFactory(player.GetData("Courier_MarkerID"));
            if (factory == null)
            {
                player.sendErrorMessage("Fábrica inválida.");
                return;
            }

            if (factory.Type != player.getCarryingType())
            {
                player.sendErrorMessage("Esta fábrica no reembolsa el producto que tiene.");
                return;
            }

            player.giveMoney(ProductDefinitions.Products[factory.Type].Price);
            player.stopCarrying();

            factory.Stock++;
            factory.Save();
        }

        [RemoteEvent("Courier_BuyerSell")]
        public void CourierBuyerSell(Client player, object[] arguments)
        {
            if (player.GetJob() != (int)JobType.Courier)
            {
                player.sendErrorMessage("No eres un repartidor.");
                return;
            }

            if (!player.isCarrying())
            {
                player.sendErrorMessage("No estás llevando un producto!");
                return;
            }

            if (!player.HasData("Courier_MarkerID"))
            {
                player.sendErrorMessage("No estás en el marcador de un negócio.");
                return;
            }

            Buyer buyer = Methods.GetBuyer(player.GetData("Courier_MarkerID"));
            if (buyer == null)
            {
                player.sendErrorMessage("Negócio inválido.");
                return;
            }

            if (buyer.Type != player.getCarryingType())
            {
                player.sendErrorMessage("Este negócio no compra el producto que estás vendiendo.");
                return;
            }

            if ((buyer.Stock + 1) > buyer.MaxStock)
            {
                player.sendErrorMessage("Este comprador no puede aceptar más productos.");
                return;
            }

            player.giveMoney(Methods.GetFinalPrice(player.getCarryingType()));
            player.stopCarrying();

            buyer.Stock++;
            buyer.Save();
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.isCarrying()) player.stopCarrying(true);
        }

        [ServerEvent(Event.VehicleDeath)]
        public void OnVehicleDeath(Vehicle vehicle)
        {
            Vehicle veh = NAPI.Entity.GetEntityFromHandle<Vehicle>(vehicle);
            if (veh == null) return;

            if (LoadedProducts.ContainsKey(vehicle))
            {
                for (int i = 0; i < LoadedProducts[vehicle].Length; i++) LoadedProducts[vehicle][i]?.Object?.Delete();
                LoadedProducts.Remove(vehicle);
            }
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Client player in NAPI.Pools.GetAllPlayers().Where(p => p.isCarrying())) player.stopCarrying();

            foreach (Factory factory in Factories)
            {
                factory.DeleteEntities();
                factory.Save(true);
            }

            foreach (Buyer buyer in Buyers)
            {
                buyer.DeleteEntities();
                buyer.Save(true);
            }

            foreach (WorldProduct product in DroppedProduct) product.DeleteEntities();

            foreach (LoadedProduct[] loadedProduct in LoadedProducts.Values)
            {
                for (int i = 0; i < loadedProduct.Length; i++) loadedProduct[i]?.Object?.Delete();
            }

            Factories.Clear();
            Buyers.Clear();
            DroppedProduct.Clear();
            LoadedProducts.Clear();
            if (WorldCleaner != null) WorldCleaner.Dispose();
        }
        #endregion
    }
}
