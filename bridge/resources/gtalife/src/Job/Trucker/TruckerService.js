"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var menu = null;
var blip = null;
var marker = null;
var label = null;
var truck = null;
var jobProgress = 0;
var isCarrying = false;
var checkpointPosition = null;
var option = null;
var deliverPositions = [
    [
        { x: -497.1505, y: -336.071, z: 33.50175 },
    ],
    [
        { x: -152.3143, y: -307.1108, z: 37.66355 },
    ],
    [
        { x: 2703.283, y: 3455.583, z: 54.62112 },
    ],
    [
        { x: 1093.128, y: -2970.938, z: 4.901186 },
    ]
];
API.onServerEventTrigger.connect((eventName, args) => {
    if (eventName == "ShowTruckerMenu") {
        if (menu == null) {
            menu = API.createMenu("Camionero", "Selecciona una opción", 0, 0, 6);
            var medicineItem = API.createMenuItem("Medicina", "Recompensa: $400 + 1 botiquin");
            var clothesItem = API.createMenuItem("Ropa", "Recompensa: $460");
            var youtoolItem = API.createMenuItem("YouTool", "Recompensa: $290");
            var materialItem = API.createMenuItem("Contrabando", "Recompensa: $850 + 1 material");
            menu.AddItem(medicineItem);
            menu.AddItem(clothesItem);
            menu.AddItem(youtoolItem);
            menu.AddItem(materialItem);
            menu.OnItemSelect.connect(function (sender, item, index) {
                option = index;
                API.triggerServerEvent("TruckerSelectOption", option);
                menu.Visible = false;
            });
        }
        menu.Visible = true;
    }
    else if (eventName == "StartTruckerJob") {
        deleteTruckerServiceEntities();
        var x = args[0];
        var y = args[1];
        var z = args[2];
        blip = NAPI.Blip.CreateBlip(new Vector3(x, y, z));
        API.setBlipSprite(blip, 67);
        API.setBlipName(blip, "Mi camión");
        jobProgress = 0;
    }
    else if (eventName == "ResetTruckerJob") {
        deleteTruckerServiceEntities();
        jobProgress = 0;
        truck = null;
        isCarrying = false;
        checkpointPosition = null;
        option = null;
    }
    else if (eventName == "OnPlayerEnterOnTruckJobVehicle") {
        if (jobProgress == 0) {
            deleteTruckerServiceEntities();
            blip = NAPI.Blip.CreateBlip(new Vector3(991.9622, -3020.979, 5.900765));
            API.setBlipSprite(blip, 1);
            API.setBlipColor(blip, 1);
            API.showBlipRoute(blip, true);
            marker = NAPI.Marker.CreateMarker(1, new Vector3(991.9622, -3020.979, 5.900765 - 1.0), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
            label = NAPI.TextLabel.CreateTextLabel("~y~Cargar camión~n~~w~Pulsa ~y~E", new Vector3(991.9622, -3020.979, 5.900765 + 0.5), 15, 0.5);
            checkpointPosition = new Vector3(991.9622, -3020.979, 5.900765);
            jobProgress++;
        }
        truck = API.getPlayerVehicle(API.getLocalPlayer());
    }
});
API.onKeyDown.connect(function (sender, key) {
    if (API.isChatOpen())
        return;
    if (jobProgress == 0)
        return;
    switch (key.KeyCode) {
        case Keys.E:
            if (jobProgress > 0 && jobProgress < 5) {
                if (API.getEntityPosition(API.getLocalPlayer()).DistanceTo(checkpointPosition) < 1) {
                    deleteTruckerServiceEntities();
                    jobProgress++;
                    if (!isCarrying) {
                        API.triggerServerEvent("TruckerCarryBoxAnimPlay");
                        checkpointPosition = API.getEntityRearPosition(truck);
                    }
                    else {
                        API.triggerServerEvent("TruckerCarryBoxAnimStop");
                        checkpointPosition = new Vector3(991.9622, -3020.979, 5.900765);
                    }
                    if (jobProgress == 5) {
                        var i = Math.floor((Math.random() * deliverPositions[option].length));
                        checkpointPosition = new Vector3(deliverPositions[option][i].x, deliverPositions[option][i].y, deliverPositions[option][i].z);
                        marker = NAPI.Marker.CreateMarker(1, new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
                        label = NAPI.TextLabel.CreateTextLabel("~y~Descargar camión~n~~w~Pulsa ~y~E", new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z + 1.0), 15, 0.5);
                        blip = NAPI.Blip.CreateBlip(new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z));
                        API.setBlipSprite(blip, 1);
                        API.setBlipColor(blip, 1);
                        API.showBlipRoute(blip, true);
                        API.SendChatMessage("~y~INFO: ~w~Ve a entregar la carga.");
                        API.playSoundFrontEnd("YES", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                        return;
                    }
                    isCarrying = !isCarrying;
                    marker = NAPI.Marker.CreateMarker(1, new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z - 1.5), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
                    label = NAPI.TextLabel.CreateTextLabel("~y~Cargar camión~n~~w~Pulsa ~y~E", new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z), 15, 0.5);
                    blip = NAPI.Blip.CreateBlip(new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z));
                    API.setBlipSprite(blip, 1);
                    API.setBlipColor(blip, 1);
                    API.playSoundFrontEnd("SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                }
            }
            else if (jobProgress == 5) {
                if (API.getEntityPosition(API.getLocalPlayer()).DistanceTo(checkpointPosition) < 4) {
                    if (!API.getPlayerVehicle(API.getLocalPlayer()).Equals(truck)) {
                        API.SendChatMessage("~r~ERROR: ~w~Usted no está en su camión.");
                        API.playSoundFrontEnd("ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                        return;
                    }
                    deleteTruckerServiceEntities();
                    jobProgress++;
                    checkpointPosition = new Vector3(-421.7409, 6126.658, 30.35735);
                    marker = NAPI.Marker.CreateMarker(1, new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z - 0.5), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
                    label = NAPI.TextLabel.CreateTextLabel("~y~Devolver el camión~n~~w~Pulsa ~y~E", new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z + 1.0), 15, 0.5);
                    blip = NAPI.Blip.CreateBlip(new Vector3(checkpointPosition.X, checkpointPosition.Y, checkpointPosition.Z));
                    API.setBlipSprite(blip, 1);
                    API.setBlipColor(blip, 1);
                    API.showBlipRoute(blip, true);
                    API.playSoundFrontEnd("SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                    API.SendChatMessage("~y~INFO: ~w~Entregastes la carga, regresa a la transportadora.");
                }
            }
            else if (jobProgress == 6) {
                if (API.getEntityPosition(API.getLocalPlayer()).DistanceTo(checkpointPosition) < 4) {
                    deleteTruckerServiceEntities();
                    option = null;
                    truck = null;
                    jobProgress = 0;
                    isCarrying = false;
                    checkpointPosition = null;
                    API.playSoundFrontEnd("SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET");
                    API.triggerServerEvent("OnFinishTruckerJob");
                }
            }
            break;
    }
});
function deleteTruckerServiceEntities() {
    if (blip != null) {
        NAPI.Entity.DeleteEntity(blip);
        blip = null;
    }
    if (label != null) {
        NAPI.Entity.DeleteEntity(label);
        label = null;
    }
    if (marker != null) {
        NAPI.Entity.DeleteEntity(marker);
        marker = null;
    }
}
