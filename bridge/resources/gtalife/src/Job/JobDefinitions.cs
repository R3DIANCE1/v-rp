﻿using GTANetworkAPI;
using System.Collections.Generic;

namespace gtalife.src.Job
{
    public enum JobType
    {
        Trucker,
        Garbageman,
        ArmsDealer,
        Courier,
        Taxi,
        Guard
    }

    public class JobDefinitions
    {
        public static List<Job> Jobs = new List<Job>
        {
            { new Job("Camionero", JobType.Trucker, 477, 46, new Vector3(-406.6562, 6149.319, 31.6783), true) },
            { new Job("Basurero", JobType.Garbageman, 318, 46, new Vector3(148.5198, 6362.242, 31.52922), true) },
            { new Job("Traficante de armas", JobType.ArmsDealer, 110, 46, new Vector3(-552.704, 5348.505, 74.74306), false) },
            { new Job("Repartidor", JobType.Courier, 478, 46, new Vector3(-5.936188, 6274.486, 31.38263), true) },
            { new Job("Taxista", JobType.Taxi, 56, 46, new Vector3(-38.58875, 6419.875, 31.49045), true) },
            { new Job("Guardia", JobType.Guard, 487, 46, new Vector3(-95.69944, 6474.229, 31.42516), true) },
        };
    }
}
