﻿using GTANetworkAPI;
using gtalife.src.Managers;
using gtalife.src.Player.Utils;
using System;
using System.Linq;

namespace gtalife.src.Job.Guard
{
    class Commands : Script
    {
        [Command("proteger", "~y~USO: ~w~/proteger [jugador] [price]")]
        public void CMD_OfferProtection(Client sender, string idOrName, int price)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
            else if (sender.GetJob() != (int)JobType.Guard) sender.SendChatMessage("~r~ERROR: ~w~No eres un guardia.");
            else if (sender == target) sender.SendChatMessage("~r~ERROR: ~w~No puedes protegerte a ti mismo.");
            else if (sender.Position.DistanceTo(target.Position) > 2f) sender.SendChatMessage("~r~ERROR: ~w~No estas cerca del jugador.");
            else if (price < 0) sender.SendChatMessage("~r~ERROR: ~w~Precio inválido.");
            else
            {
                target.SendChatMessage($"~y~INFO: ~w~{sender.Name} está ofreciendo protegerte por ~g~$~w~{price}. (( /aceptarproteccion ))");
                sender.SendChatMessage($"~y~INFO: ~w~Usted está ofreciendo proteger {target.Name} por ~g~$~w~{price}.");

                target.SetData("COVER_PLAYER", sender);
                target.SetData("COVER_PRICE", price);
            }
        }

        [Command("aceptarproteccion")]
        public void CMD_AcceptProtection(Client sender)
        {
            if (!sender.HasData("COVER_PRICE")) sender.SendChatMessage("~r~ERROR: ~w~Nadie te ofreció protección.");
            else
            {
                Client target = sender.GetData("COVER_PLAYER");
                int price = sender.GetData("COVER_PRICE");

                if (sender.getMoney() < price) sender.SendChatMessage("~r~ERROR: ~w~No tienes suficiente dinero.");
                else if (!target.IsLogged()) NAPI.Notification.SendNotificationToPlayer(sender, "~r~ERROR: ~w~El jugador no está conectado.");
                else if (sender.Position.DistanceTo(target.Position) > 2f) sender.SendChatMessage("~r~ERROR: ~w~No estas cerca del jugador.");
                else
                {
                    target.SendChatMessage($"~y~INFO: ~w~{sender.Name} aceptó tu protección.");
                    sender.SendChatMessage($"~y~INFO: ~w~Usted aceptou la proteccion.");

                    sender.giveMoney(-price);
                    target.giveMoney(price);

                    target.ResetData("COVER_PLAYER");
                    target.ResetData("COVER_PRICE");
                }
            }
        }

        [Command("trabajoguardia")]
        public void CMD_StartService(Client sender)
        {
            if (sender.GetJob() != (int)JobType.Guard) sender.SendChatMessage("~r~ERROR: ~w~No eres un guardia.");
            else if (Service.PlayerJobProgress.ContainsKey(sender)) sender.SendChatMessage("~r~ERROR: ~w~Ya comenzaste el servicio.");
            else
            {
                var atms = Gameplay.Bank.Main.Banks.Where(x => x.Type != 0).ToList();
                if (atms.Count < 1)
                {
                    sender.SendNotification("Todos los cajeros automáticos están completos.");
                    return;
                }

                if (Main.PlayerJobVehicle.ContainsKey(sender))
                {
                    if (NAPI.Entity.DoesEntityExist(Main.PlayerJobVehicle[sender].NetHandle)) NAPI.Entity.DeleteEntity(Main.PlayerJobVehicle[sender].NetHandle);
                    Main.PlayerJobVehicle.Remove(sender);
                }

                var vehicle = NAPI.Vehicle.CreateVehicle(VehicleHash.Stockade, new Vector3(-88.64186, 6472.465, 30.86138), new Vector3(-0.04589463, -0.8046648, 47.34253), 134, 139);
                vehicle.EngineStatus = false;

                Main.PlayerJobVehicle.Add(sender, new JobVehicle { NetHandle = vehicle.Handle, DeleteAt = DateTime.Now.AddMinutes(3) });
                Service.PlayerJobProgress.Add(sender, 0);

                Random random = new Random();
                sender.SendNotification("Tome el camión y vaya a recoger el dinero");
                sender.TriggerEvent("SecurityGuardJobStart", vehicle, atms[random.Next(atms.Count)].Position);
            }
        }
    }
}
