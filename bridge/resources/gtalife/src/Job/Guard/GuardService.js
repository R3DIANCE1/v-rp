"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var truck = null;
var truckBlip = null;
var blip = null;
var marker = null;
var label = null;
var checkpoint = null;
var is_carrying = false;
API.onServerEventTrigger.connect((eventName, args) => {
    if (eventName == "SecurityGuardJobStart") {
        if (truckBlip != null)
            NAPI.Entity.DeleteEntity(truckBlip);
        truck = args[0];
        truckBlip = NAPI.Blip.CreateBlip(API.getEntityPosition(truck));
        API.setBlipSprite(truckBlip, 67);
        API.setBlipName(truckBlip, "Vehículo de trabajo");
        API.attachEntity(truckBlip, truck, "", new Vector3(), new Vector3());
        // checkpoints
        checkpoint = args[1];
        createSecurityServiceEntities();
    }
    else if (eventName == "SecurityGuardGetNextCheckpoint") {
        checkpoint = args[0];
        if (args.Length > 1)
            is_carrying = true;
        createSecurityServiceEntities();
    }
    else if (eventName == "SecurityGuardDeleteEntities") {
        deleteSecurityServiceEntities();
    }
});
API.onKeyUp.connect((sender, key) => {
    switch (key.KeyCode) {
        case Keys.Y:
            if (checkpoint == null)
                return;
            if (API.getEntityPosition(API.getLocalPlayer()).DistanceTo(checkpoint) < 1) {
                deleteSecurityServiceEntities();
                if (!is_carrying) {
                    is_carrying = true;
                    API.triggerServerEvent("SecurityGuardCarryAnimPlay");
                    checkpoint = API.getEntityRearPosition(truck);
                    createSecurityServiceEntities();
                }
                else {
                    is_carrying = false;
                    API.triggerServerEvent("SecurityGuardCarryAnimStop");
                    API.triggerServerEvent("SecurityGuardGetNextCheckpoint");
                }
            }
            break;
    }
});
API.onUpdate.connect(() => {
    if (truck == null)
        return;
    if (!NAPI.Entity.DoesEntityExist(truck))
        truck = null;
    if (API.getEntityPosition(truck).DistanceTo(API.getEntityPosition(API.getLocalPlayer())) < 5 && truckBlip != null) {
        NAPI.Entity.DeleteEntity(truckBlip);
        truckBlip = null;
    }
    else if (API.getEntityPosition(truck).DistanceTo(API.getEntityPosition(API.getLocalPlayer())) > 50 && truckBlip == null) {
        truckBlip = NAPI.Blip.CreateBlip(API.getEntityPosition(truck));
        API.setBlipSprite(truckBlip, 67);
        API.setBlipName(truckBlip, "Vehículo de trabajo");
        API.attachEntity(truckBlip, truck, "", new Vector3(), new Vector3());
    }
});
function deleteSecurityServiceEntities() {
    if (blip != null) {
        NAPI.Entity.DeleteEntity(blip);
        blip = null;
    }
    if (label != null) {
        NAPI.Entity.DeleteEntity(label);
        label = null;
    }
    if (marker != null) {
        NAPI.Entity.DeleteEntity(marker);
        marker = null;
    }
    checkpoint = null;
}
function createSecurityServiceEntities() {
    blip = NAPI.Blip.CreateBlip(checkpoint);
    API.setBlipName(blip, "Trabajo");
    API.setBlipSprite(blip, 1);
    API.setBlipColor(blip, 1);
    API.showBlipRoute(blip, true);
    marker = NAPI.Marker.CreateMarker(1, new Vector3(checkpoint.X, checkpoint.Y, checkpoint.Z - 1.0), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
    label = NAPI.TextLabel.CreateTextLabel((is_carrying) ? "~b~Colocar dinero~n~~w~Pulsa ~b~Y" : "~b~Coger dinero~n~~w~Pulsa ~b~Y", new Vector3(checkpoint.X, checkpoint.Y, checkpoint.Z + 0.5), 15, 0.5);
}
