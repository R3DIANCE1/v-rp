﻿using System.Threading.Tasks;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;

namespace gtalife.src.Discord
{
    class Commands
    {
        [Command("ping")]
        [Description("Ping command")]
        public async Task Ping(CommandContext ctx)
        {
            await ctx.TriggerTypingAsync();

            var emoji = DiscordEmoji.FromName(ctx.Client, ":ping_pong:");

            var embed = new DiscordEmbedBuilder
            {
                Title = "Pong",
                Description = $"{emoji} pong!",
                Color = new DiscordColor(0xFF0000)
            };

            await ctx.RespondAsync($"", embed: embed);
        }
    }
}
