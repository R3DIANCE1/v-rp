﻿using GTANetworkAPI;
using gtalife.src.Database.Models;
using Microsoft.EntityFrameworkCore;

namespace gtalife.src.Database
{
    public class Main : Script
    {
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            using (var db = new DefaultDbContext())
            {
                db.Database.Migrate();
            }
        }
    }
}
