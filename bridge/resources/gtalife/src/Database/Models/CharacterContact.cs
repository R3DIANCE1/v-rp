﻿namespace gtalife.src.Database.Models
{
    public class CharacterContact
    {
        public int Id { get; set; }

        public virtual Character Character { get; set; }

        public string Name { get; set; }

        public int Phone { get; set; }
    }
}
