﻿using System;

namespace gtalife.src.Database.Models
{
    public class Weather
    {
        public int Id { get; set; }

        public int Current { get; set; }

        public int Next { get; set; }

        public TimeSpan Duration { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
