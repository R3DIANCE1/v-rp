﻿using GTANetworkAPI;
using gtalife.src.Admin;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.Bank
{
    class Commands : Script
    {
        [Command("bancocmds")]
        public void CommandsCommand(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tiene permiso.");
                return;
            }

            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Banco ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /createbank - /deletebank - /setbanktype");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Banco ~~~~~~~~~~~");
        }

        [Command("createbank")]
        public void CMD_CreateBank(Client player, int type)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tiene permiso.");
                return;
            }

            else if (type < 0 || type > 1)
            {
                player.SendChatMessage("~r~ERROR: ~w~Utilice valores entre 0 y 1 para el tipo.");
                return;
            }

            Bank new_bank = new Bank(Main.GetGuid(), player.Position, type);
            new_bank.Save();

            player.SetData("BankMarker_ID", new_bank.ID);
            player.TriggerEvent("ShowBankText", 1);

            Main.Banks.Add(new_bank);
            NAPI.Chat.SendChatMessageToPlayer(player, (type == 0) ? "~g~ÉXITO: ~s~Usted ha agregado un banco en su posición." : "~g~ÉXITO: ~s~Usted ha agregado un cajero automático en su posición.");
        }

        [Command("deletebank")]
        public void CMD_RemoveBank(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tiene permiso.");
                return;
            }

            if (!player.HasData("BankMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Permanezca en la posición del banco que desea eliminar.");
                return;
            }

            Bank bank = Main.Banks.FirstOrDefault(h => h.ID == player.GetData("BankMarker_ID"));
            if (bank == null) return;

            bank.Destroy();
            Main.Banks.Remove(bank);

            player.ResetData("BankMarker_ID");
            player.TriggerEvent("ShowBankText", 0);

            string bank_file = Main.BANK_SAVE_DIR + Path.DirectorySeparatorChar + bank.ID + ".json";
            if (File.Exists(bank_file)) File.Delete(bank_file);
            NAPI.Chat.SendChatMessageToPlayer(player, (bank.Type == 0) ? "~g~ÉXITO: ~s~Usted ha borrado este banco." : "~g~ÉXITO: ~s~Usted ha borrado este cajero automático.");
        }

        [Command("setbanktype")]
        public void CMD_SetBankType(Client player, int new_type)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tiene permiso.");
                return;
            }

            if (!player.HasData("BankMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Permanezca en la posición del banco que desea editar.");
                return;
            }

            if (new_type < 0 || new_type > 1)
            {
                player.SendChatMessage("~r~ERROR: ~w~Utilice valores entre 0 y 1 para el tipo.");
                return;
            }

            Bank bank = Main.Banks.FirstOrDefault(h => h.ID == player.GetData("BankMarker_ID"));
            if (bank == null) return;

            bank.SetType(new_type);
            bank.Save();

            NAPI.Chat.SendChatMessageToPlayer(player, (new_type == 0) ? "~g~ÉXITO: ~s~Ha cambiado el tipo a banco." : "~g~ÉXITO: ~s~Ha cambiado el tipo a cajero electrónico.");
        }
    }
}
