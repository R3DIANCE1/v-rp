﻿using GTANetworkAPI;
using gtalife.src.Managers;
using gtalife.src.Player.Utils;
using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace gtalife.src.Gameplay.Bank
{
    class Main : Script
    {
        public static string BANK_SAVE_DIR = "data/Bank";
        public static int SAVE_INTERVAL = 120;

        public static List<Bank> Banks = new List<Bank>();

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Banks.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }
        #endregion

        #region Events

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // load settings
            if (NAPI.Resource.HasSetting(this, "bankDirName")) BANK_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "bankDirName");

            BANK_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + BANK_SAVE_DIR;
            if (!Directory.Exists(BANK_SAVE_DIR)) Directory.CreateDirectory(BANK_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            // load parking lots
            foreach (string file in Directory.EnumerateFiles(BANK_SAVE_DIR, "*.json"))
            {
                Bank bank = JsonConvert.DeserializeObject<Bank>(File.ReadAllText(file));
                Banks.Add(bank);
            }

            NAPI.Util.ConsoleOutput("Loaded {0} banks/atms.", Banks.Count);
        }

        [RemoteEvent("BankInteract")]
        public void BankInteract(Client player, object[] arguments)
        {
            if (!player.HasData("BankMarker_ID")) return;

            player.TriggerEvent("show_bank_menu");
        }

        [RemoteEvent("bank_deposit")]
        public void BankDeposit(Client player, object[] arguments)
        {
            if (!int.TryParse(arguments[0].ToString(), out var money)) player.SendChatMessage($"~r~ERROR: ~s~Valor invalido.");
            else if (money > player.getMoney()) player.SendChatMessage($"~r~ERROR: ~s~No tienes suficiente dinero.");
            else
            {
                Player.Data.Character[player].Bank += money;
                player.giveMoney(-money);

                player.SendPictureNotificationToPlayer($"Depositastes ${money}.", "CHAR_BANK_MAZE", 0, 9, "Banco", "Deposito");
                player.sendChatAction("depositó cierta cantidad de dinero.");
            }
        }

        [RemoteEvent("bank_withdraw")]
        public void BankWithdraw(Client player, object[] arguments)
        {
            if (!int.TryParse(arguments[0].ToString(), out var money)) player.SendChatMessage($"~r~ERROR: ~s~Valor invalido.");
            else if (money > Player.Data.Character[player].Bank) player.SendChatMessage($"~r~ERROR: ~s~No tienes suficiente dinero.");
            else
            {
                Player.Data.Character[player].Bank -= money;
                player.giveMoney(money);

                player.SendPictureNotificationToPlayer($"Sacastes ${money}.", "CHAR_BANK_MAZE", 0, 9, "Banco", "Saque");
                player.sendChatAction("sacó cierta cantidad de dinero.");
            }
        }

        [RemoteEvent("bank_balance")]
        public void BankBalance(Client player, object[] arguments)
        {
            player.SendPictureNotificationToPlayer($"Su saldo es ${Player.Data.Character[player].Bank}.", "CHAR_BANK_MAZE", 0, 9, "Banco", "Saldo");
            player.sendChatAction("verifica el saldo bancario.");
        }

        [RemoteEvent("bank_transfer")]
        public void BankTransfer(Client sender, object[] arguments)
        {
            string playerName = (string)arguments[1];

            if (!int.TryParse(arguments[0].ToString(), out var money)) sender.SendChatMessage($"~r~ERROR: ~s~Valor invalido.");
            else if (money > Player.Data.Character[sender].Bank) sender.SendChatMessage($"~r~ERROR: ~s~No tienes suficiente dinero.");
            else
            {
                Client target = PlayeridManager.FindPlayer(playerName);

                if (target == null) sender.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
                else if (!target.IsLogged()) sender.SendNotification("~r~ERROR: ~w~El jugador no está conectado.");
                else if (sender == target) sender.SendChatMessage("~r~ERROR: ~w~No puedes transferir a usted mismo.");
                else
                {
                    Player.Data.Character[sender].Bank -= money;
                    Player.Data.Character[target].Bank += money;

                    sender.SendPictureNotificationToPlayer($"Has transferido ${money} para {target.Name}.", "CHAR_BANK_MAZE", 0, 9, "Banco", "Transferencia");
                    target.SendPictureNotificationToPlayer($"Has recibido una transferencia a nombre de {sender.Name} con un valor de ${money}.", "CHAR_BANK_MAZE", 0, 9, "Banco", "Transferencia");

                    sender.sendChatAction("transferió cierta cantidad de dinero.");
                }
            }
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Bank bank in Banks)
            {
                bank.Save(true);
                bank.Destroy();
            }

            Banks.Clear();
        }
        #endregion
    }
}
