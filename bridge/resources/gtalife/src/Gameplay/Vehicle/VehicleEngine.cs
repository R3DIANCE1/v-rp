﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;
using System.Timers;

namespace gtalife.src.Gameplay
{
    class VehicleEngine : Script
    {
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            Timer statusUpdate = new Timer();
            statusUpdate.Elapsed += new ElapsedEventHandler(OnResourceUpdate);
            statusUpdate.Interval = 120000;
            statusUpdate.Enabled = true;

        }

        private void OnResourceUpdate(object sender, ElapsedEventArgs e)
        {
            /* deprecated
             * foreach (var vehicle in NAPI.Pools.GetAllVehicles())
            {
                var fuel = NAPI.Vehicle.GetVehicleFuelLevel(vehicle);
                if (NAPI.Vehicle.GetVehicleEngineStatus(vehicle) && fuel > 0 && !NAPI.Vehicle.IsVehicleABicycle((VehicleHash)NAPI.Entity.GetEntityModel(vehicle)))
                {
                    NAPI.Vehicle.SetVehicleFuelLevel(vehicle, (fuel >= 1 && fuel <= 3) ? 0 : fuel - 1);
                }
            }*/
        }

        [RemoteEvent("on_player_toggle_engine")]
        public void OnPlayerToggleEngine(Client sender, object[] arguments)
        {
            if (!sender.IsInVehicle || sender.VehicleSeat != (int)VehicleSeat.Driver || sender.HasData("IsFillingVehicle")) return;
            if (!Dealership.Main.IsADealershipVehicle(sender.Vehicle) && !Business.Main.IsABusinessVehicle(sender.Vehicle))
            {
                sender.Vehicle.EngineStatus = !sender.Vehicle.EngineStatus;
                sender.sendChatAction((sender.Vehicle.EngineStatus) ? "arrancó el motor del vehículo." : "apagó el motor del vehículo.");
            }
        }

        [ServerEvent(Event.PlayerEnterVehicle)]
        public void OnPlayerEnterVehicle(Client player, Vehicle vehicle, sbyte seatID)
        {
            if (!NAPI.Vehicle.GetVehicleEngineStatus(vehicle) && !Business.Main.IsABusinessVehicle(vehicle) && !Dealership.Main.IsADealershipVehicle(vehicle) && seatID == -1)
            {
                NAPI.Chat.SendChatMessageToPlayer(player, "~y~INFO: ~s~El motor está apagado, para alternar presione ~y~Y~s~.");
            }
        }
    }
}
