﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.IO;

namespace gtalife.src.Gameplay.Icon
{
    #region Icon Class
    public class Icon
    {
        public Guid ID { get; }
        public string Name { get; private set; }
        public uint Sprite { get; private set; }
        public int Color { get; private set; }
        public bool ShortRange { get; private set; }
        public Vector3 Position { get; private set; }

        // entities
        [JsonIgnore]
        private Blip Blip;

        [JsonIgnore]
        private DateTime LastSave;

        public Icon(Guid id, string name, uint sprite, int color, Vector3 position, bool shortrange)
        {
            ID = id;
            Name = name;
            Sprite = sprite;
            Color = color;
            Position = position;
            ShortRange = shortrange;

            // create blip
            Blip = NAPI.Blip.CreateBlip(position);
            Blip.Scale = 1f;
            UpdateBlip();
        }

        private void UpdateBlip()
        {
            Blip.Name = Name;
            Blip.Sprite = Sprite;
            Blip.Color = Color;
            Blip.Position = Position;
            Blip.ShortRange = ShortRange;
        }

        public void SetName(string new_name)
        {
            Name = new_name;

            UpdateBlip();
            Save();
        }

        public void SetSprite(uint new_sprite)
        {
            Sprite = new_sprite;

            UpdateBlip();
            Save();
        }

        public void SetPosition(Vector3 position)
        {
            Position = position;

            UpdateBlip();
            Save();
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.BLIP_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy(bool exit = false)
        {
            Blip.Delete();
        }
    }
    #endregion
}
