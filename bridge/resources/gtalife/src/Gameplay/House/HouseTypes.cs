﻿using System.Collections.Generic;
using GTANetworkAPI;

namespace gtalife.src.Gameplay.House
{
    #region HouseType Class
    public class HouseType
    {
        public string Name { get; }
        public Vector3 Position { get; }

        private Marker Marker;
        private ColShape ColShape;
        private TextLabel Label;

        public HouseType(string name, Vector3 position)
        {
            Name = name;
            Position = position;
        }

        public void Create()
        {
            Marker = NAPI.Marker.CreateMarker(1, Position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 1, new Color(150, 64, 196, 255));

            ColShape = NAPI.ColShape.CreateCylinderColShape(Position, 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.TriggerEvent("ShowHouseText", 2);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.TriggerEvent("ShowHouseText", 0);
                }
            };

            Label = NAPI.TextLabel.CreateTextLabel("Dejar la casa", Position, 10f, 0.65f, 1, new Color(255, 255, 255, 255));
        }

        public void Destroy()
        {
            Marker.Delete();
            NAPI.ColShape.DeleteColShape(ColShape);
            Label.Delete();
        }
    }
    #endregion

    public class HouseTypes : Script
    {
        public static List<HouseType> HouseTypeList = new List<HouseType>
        {
            // name, position
            new HouseType("Low End", new Vector3(265.0858, -1000.888, -99.00855)),
            new HouseType("Medium End", new Vector3(346.453, -1002.456, -99.19622)),
            new HouseType("Modern Apartment", new Vector3(-786.8663, 315.7642, 217.6385)),
            /*new HouseType("Mody Apartment", new Vector3(-787.0749, 315.8198, 217.6386)),
            new HouseType("Vibrant Apartment", new Vector3(-786.6245, 315.6175, 217.6385)),
            new HouseType("Sharp Apartment", new Vector3(-787.0902, 315.7039, 217.6384)),
            new HouseType("Monochrome Apartment", new Vector3(-786.9887, 315.7393, 217.6386)),
            new HouseType("Seductive Apartment", new Vector3(-787.1423, 315.6943, 217.6384)),
            new HouseType("Regal Apartment", new Vector3(-787.029, 315.7113, 217.6385)),
            new HouseType("Aqua Apartment", new Vector3(-786.9469, 315.5655, 217.6383)),*/
            new HouseType("Lux Apartment", new Vector3(-31.05241, -595.2823, 80.03093))
        };

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            foreach (HouseType house_type in HouseTypeList) house_type.Create();
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (HouseType house_type in HouseTypeList) house_type.Destroy();
        }
        #endregion
    }
}
