﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GTANetworkAPI;
using gtalife.src.Player.Utils;
using Newtonsoft.Json;

namespace gtalife.src.Gameplay.House
{
    #region House Class
    public class House
    {
        public Guid ID { get; }
        public string Owner { get; private set; }
        public int Type { get; private set; }
        public Vector3 Position { get; }
        public int Price { get; private set; }
        public bool Locked { get; private set; }
        [JsonIgnore] public uint Dimension { get; set; }

        // customization
        public string Name { get; private set; }
        public List<HouseFurniture> Furnitures { get; }

        // storage
        public long Money { get; private set; }
        public List<HouseWeapon> Weapons { get; }

        // entities
        [JsonIgnore]
        private Blip Blip;

        [JsonIgnore]
        private Marker Marker;

        [JsonIgnore]
        private ColShape ColShape;

        [JsonIgnore]
        private TextLabel Label;

        // misc
        [JsonIgnore]
        private List<NetHandle> PlayersInside = new List<NetHandle>();

        [JsonIgnore]
        private Client OwnerHandle;

        [JsonIgnore]
        private DateTime LastSave;

        public House(Guid id, string owner, int type, Vector3 position, int price, bool locked, string name = "", List<HouseFurniture> furnitures = null, long money = 0, List<HouseWeapon> weapons = null)
        {
            ID = id;
            Owner = owner;
            Type = type;
            Position = position;
            Price = price;
            Locked = locked;

            Name = name;
            Furnitures = furnitures ?? new List<HouseFurniture>();

            Money = money;
            Weapons = weapons ?? new List<HouseWeapon>();

            // create blip
            Blip = NAPI.Blip.CreateBlip(position);

            if (string.IsNullOrEmpty(owner))
            {
                Blip.Sprite = 350;
                Blip.Name = "Casa en venta";
            }
            else
            {
                Blip.Sprite = 40;
                Blip.Name = "Casa del jugador";
            }

            Blip.Color = 0;
            Blip.Scale = 0.5f;
            Blip.ShortRange = true;
            Blip.SetSharedData("PlayersInside", 0);

            // create marker
            Marker = NAPI.Marker.CreateMarker(1, position - new Vector3(0.0, 0.0, 1.0), new Vector3(), new Vector3(), 1, new Color(150, 64, 196, 255));

            // create colshape
            ColShape = NAPI.ColShape.CreateCylinderColShape(position, 0.85f, 0.85f);
            ColShape.OnEntityEnterColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.SetData("HouseMarker_ID", ID);
                    player.TriggerEvent("ShowHouseText", 1);
                }
            };

            ColShape.OnEntityExitColShape += (s, ent) =>
            {
                Client player;

                if ((player = NAPI.Player.GetPlayerFromHandle(ent)) != null)
                {
                    player.ResetData("HouseMarker_ID");
                    player.TriggerEvent("ShowHouseText", 0);
                }
            };

            // create text label
            Label = NAPI.TextLabel.CreateTextLabel("House", position, 15f, 0.65f, 1, new Color(255, 255, 255, 255));
            UpdateLabel();

            OwnerHandle = null;
        }

        private void UpdateLabel()
        {
            if (string.IsNullOrEmpty(Owner))
            {
                Label.Text = string.Format("~g~Casa en venta~n~~n~~w~Interior: ~g~{0}~n~~w~Precio: ~g~${1:n0}", HouseTypes.HouseTypeList[Type].Name, Price);
            }
            else
            {
                Label.Text = string.Format("~g~Casa~n~~n~~w~{0}~n~~w~Propietario: ~g~{1}~n~{2}", Name, Owner, ((Locked) ? "~r~Bloqueada" : "~g~Desbloqueada"));
            }
        }

        private void UpdateBlip()
        {
            int count = PlayersInside.Count;
            Blip.SetSharedData("PlayersInside", count);

            if (count < 1)
            {
                if (Blip.Sprite != 40)
                {
                    Blip.Sprite = 40;

                    if (OwnerHandle != null) UpdateBlipForOwner();
                }

                NAPI.Native.SendNativeToAllPlayers(Hash.HIDE_NUMBER_ON_BLIP, Blip.Handle);
            }
            else
            {
                if (Blip.Sprite != 417)
                {
                    Blip.Sprite = 417;

                    if (OwnerHandle != null) UpdateBlipForOwner();
                }

                NAPI.Native.SendNativeToAllPlayers(Hash.SHOW_NUMBER_ON_BLIP, Blip.Handle, count);
            }
        }

        public void SetOwner(Client player)
        {
            if (OwnerHandle != null) OwnerHandle.TriggerEvent("ResetHouseBlip", Blip.Handle);
            Owner = (player == null) ? string.Empty : player.Name;

            Blip.Sprite = (player == null) ? (uint)350 : 40;
            Blip.Color = 0;
            Blip.ShortRange = true;

            SetOwnerHandle(player);
            UpdateLabel();
            Save();
        }

        public void SetOwnerHandle(Client player)
        {
            OwnerHandle = player;
            if (player != null) player.TriggerEvent("UpdateHouseBlip", Blip.Handle);
        }

        public void UpdateBlipForOwner()
        {
            if (OwnerHandle == null) return;
            OwnerHandle.TriggerEvent("UpdateHouseBlip", Blip.Handle);
        }

        public void SetName(string new_name)
        {
            Name = new_name;

            UpdateLabel();
            Save();
        }

        public void SetLock(bool locked)
        {
            Locked = locked;

            UpdateLabel();
            Save();
        }

        public void SetType(int new_type)
        {
            Type = new_type;

            UpdateLabel();
            Save();
        }

        public void SetPrice(int new_price)
        {
            Price = new_price;

            UpdateLabel();
            Save();
        }

        public void ChangeMoney(int amount)
        {
            Money += amount;

            Save();
        }

        public void SendPlayer(Client player)
        {
            player.Position = HouseTypes.HouseTypeList[Type].Position;
            player.Dimension = Dimension;
            player.SetData("InsideHouse_ID", ID);

            if (!PlayersInside.Contains(player.Handle)) PlayersInside.Add(player.Handle);
            UpdateBlip();
        }

        public void RemovePlayer(Client player, bool set_pos = true)
        {
            if (set_pos)
            {
                player.Position = Position;
                player.Dimension = 0;
            }

            player.ResetData("InsideHouse_ID");

            if (PlayersInside.Contains(player.Handle)) PlayersInside.Remove(player.Handle);
            UpdateBlip();
        }

        public void RemoveAllPlayers(bool exit = false)
        {
            for (int i = PlayersInside.Count - 1; i >= 0; i--)
            {
                Client player = NAPI.Entity.GetEntityFromHandle<Client>(PlayersInside[i]);

                if (player != null)
                {
                    player.Position = Position;
                    player.Dimension = 0;

                    player.ResetData("InsideHouse_ID");
                }

                PlayersInside.RemoveAt(i);
            }

            if (!exit) UpdateBlip();
        }

        public void Save(bool force = true)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.HOUSE_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }

        public void Destroy(bool exit = false)
        {
            foreach (HouseFurniture furniture in Furnitures) furniture.Destroy();
            RemoveAllPlayers(exit);

            Blip.Delete();
            Marker.Delete();
            NAPI.ColShape.DeleteColShape(ColShape);
            Label.Delete();
        }
    }
    #endregion

    public class Main : Script
    {
        // settings
        public static string HOUSE_SAVE_DIR = "data/HouseData";
        public static int PLAYER_HOUSE_LIMIT = 3;
        public static int HOUSE_MONEY_LIMIT = 5000000;
        public static int HOUSE_WEAPON_LIMIT = 10;
        public static int HOUSE_FURNITURE_LIMIT = 25;
        public static bool RESET_DIMENSION_ON_DEATH = true;
        public static int SAVE_INTERVAL = 120;

        public static List<House> Houses = new List<House>();
        public static uint DimensionID = 1;

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Houses.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }

        public void RemovePlayerFromHouseList(Client player)
        {
            if (player.HasData("InsideHouse_ID"))
            {
                House house = Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
                if (house == null) return;

                house.RemovePlayer(player, false);
            }
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            DimensionID = 1;

            // load settings
            if (NAPI.Resource.HasSetting(this, "houseDirName")) HOUSE_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "houseDirName");

            HOUSE_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + HOUSE_SAVE_DIR;
            if (!Directory.Exists(HOUSE_SAVE_DIR)) Directory.CreateDirectory(HOUSE_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "playerHouseLimit")) PLAYER_HOUSE_LIMIT = NAPI.Resource.GetSetting<int>(this, "playerHouseLimit");
            if (NAPI.Resource.HasSetting(this, "houseMoneyLimit")) HOUSE_MONEY_LIMIT = NAPI.Resource.GetSetting<int>(this, "houseMoneyLimit");
            if (NAPI.Resource.HasSetting(this, "houseWeaponLimit")) HOUSE_WEAPON_LIMIT = NAPI.Resource.GetSetting<int>(this, "houseWeaponLimit");
            if (NAPI.Resource.HasSetting(this, "houseFurnitureLimit")) HOUSE_FURNITURE_LIMIT = NAPI.Resource.GetSetting<int>(this, "houseFurnitureLimit");
            if (NAPI.Resource.HasSetting(this, "resetDimensionOnDeath")) RESET_DIMENSION_ON_DEATH = NAPI.Resource.GetSetting<bool>(this, "resetDimensionOnDeath");
            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            NAPI.Util.ConsoleOutput("-> Player House Limit: {0}", ((PLAYER_HOUSE_LIMIT == 0) ? "Disabled" : PLAYER_HOUSE_LIMIT.ToString()));
            NAPI.Util.ConsoleOutput("-> House Safe Limit: ${0:n0}", HOUSE_MONEY_LIMIT);
            NAPI.Util.ConsoleOutput("-> House Weapon Limit: {0}", ((HOUSE_WEAPON_LIMIT == 0) ? "Disabled" : HOUSE_WEAPON_LIMIT.ToString()));
            NAPI.Util.ConsoleOutput("-> House Furniture Limit: {0}", ((HOUSE_FURNITURE_LIMIT == 0) ? "Disabled" : HOUSE_FURNITURE_LIMIT.ToString()));
            NAPI.Util.ConsoleOutput("-> Dimension Reset On Death: {0}", ((RESET_DIMENSION_ON_DEATH) ? "Enabled" : "Disabled"));

            // load houses
            foreach (string file in Directory.EnumerateFiles(HOUSE_SAVE_DIR, "*.json"))
            {
                House house = JsonConvert.DeserializeObject<House>(File.ReadAllText(file));
                house.Dimension = DimensionID;
                foreach (HouseFurniture furniture in house.Furnitures) furniture.Create((byte)DimensionID);

                Houses.Add(house);
                DimensionID++;
            }

            NAPI.Util.ConsoleOutput("Loaded {0} houses.", Houses.Count);
        }

        [ServerEvent(Event.PlayerConnected)]
        public void OnPlayerConnected(Client player)
        {
            System.Threading.Tasks.Task.Run(() =>
            {
                NAPI.Task.Run(() =>
                {
                    foreach (House house in Houses.Where(h => h.Owner == player.Name)) house.SetOwnerHandle(player);
                }, delayTime: 2000);
            });
        }

        [RemoteEvent("HouseInteract")]
        public void HouseInteract(Client player, object[] arguments)
        {
            if (!player.HasData("HouseMarker_ID")) return;

            House house = Houses.FirstOrDefault(h => h.ID == player.GetData("HouseMarker_ID"));
            if (house == null) return;

            if (string.IsNullOrEmpty(house.Owner))
            {
                // not owned house
                player.TriggerEvent("House_PurchaseMenu", NAPI.Util.ToJson(new { Interior = HouseTypes.HouseTypeList[house.Type].Name, house.Price }));
            }
            else
            {
                // owned house
                if (house.Locked)
                {
                    if (house.Owner == player.Name)
                    {
                        house.SendPlayer(player);
                    }
                    else
                    {
                        player.SendNotification("~r~Solo el propietario puede acceder a esta casa.");
                    }
                }
                else
                {
                    house.SendPlayer(player);
                }
            }
        }

        [RemoteEvent("HousePurchase")]
        public void HousePurchase(Client player, object[] arguments)
        {
            if (!player.HasData("HouseMarker_ID")) return;

            House house = Houses.FirstOrDefault(h => h.ID == player.GetData("HouseMarker_ID"));
            if (house == null) return;

            if (!string.IsNullOrEmpty(house.Owner))
            {
                player.SendNotification("~r~Esta casa tiene dueño.");
                return;
            }

            if (house.Price > player.getMoney())
            {
                player.SendNotification("~r~No puedes pagar por esta casa.");
                return;
            }

            if (PLAYER_HOUSE_LIMIT > 0 && Houses.Count(h => h.Owner == player.Name) >= PLAYER_HOUSE_LIMIT)
            {
                player.SendNotification("~r~No puedes tener más casas.");
                return;
            }

            player.SendNotification("~g~Felicitaciones, compraste esta casa!");

            house.SetLock(true);
            house.SetOwner(player);
            house.SendPlayer(player);

            player.giveMoney(-house.Price);
        }

        [RemoteEvent("HouseMenu")]
        public void HouseMenu(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID")) return;

            House house = Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el propietario puede acceder al menú de la casa.");
                return;
            }

            player.TriggerEvent("HouseMenu", NAPI.Util.ToJson(house));
        }

        [RemoteEvent("HouseSetName")]
        public void HouseSetName(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID") || arguments.Length < 1) return;

            House house = Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            string new_name = arguments[0].ToString();
            if (new_name.Length > 32)
            {
                player.SendNotification("~r~El nombre no puede tener más de 32 caracteres.");
                return;
            }

            house.SetName(new_name);
            player.SendNotification(string.Format("~g~El nombre de la casa cambió a: ~w~\"{0}\"", new_name));
        }

        [RemoteEvent("HouseSetLock")]
        public void HouseSetLock(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID") || arguments.Length < 1) return;

            House house = Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            bool new_state = Convert.ToBoolean(arguments[0]);
            house.SetLock(new_state);

            player.SendNotification(((new_state) ? "~g~La casa ahora está bloqueada." : "~g~La casa ahora está desbloqueada."));
        }

        [RemoteEvent("HouseSafe")]
        public void HouseSafe(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID") || arguments.Length < 2) return;

            House house = Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            int type = Convert.ToInt32(arguments[0]);

            if (!int.TryParse(arguments[1].ToString(), out int amount))
            {
                player.SendNotification("~r~Monto invalido.");
                return;
            }

            if (amount < 1) return;
            if (type == 0)
            {
                if (player.getMoney() < amount)
                {
                    player.SendNotification("~r~No tienes tanto dinero.");
                    return;
                }

                if (house.Money + amount > HOUSE_MONEY_LIMIT)
                {
                    player.SendNotification("~r~Límite de dinero de la casa alcanzado.");
                    return;
                }

                player.giveMoney(-amount);

                house.ChangeMoney(amount);
                player.SendNotification(string.Format("~g~Colocaste ${0:n0} en el cofre.", amount));
                player.TriggerEvent("HouseUpdateSafe", NAPI.Util.ToJson(new { house.Money }));
            }
            else
            {
                if (house.Money < amount)
                {
                    player.SendNotification("~r~La casa caja fuerte no tiene tanto dinero.");
                    return;
                }

                player.giveMoney(amount);

                house.ChangeMoney(-amount);
                player.SendNotification(string.Format("~g~Tomó ${0:n0} de la caja fuerte.", amount));
                player.TriggerEvent("HouseUpdateSafe", NAPI.Util.ToJson(new { house.Money }));
            }
        }

        [RemoteEvent("HouseSell")]
        public void HouseSell(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID")) return;

            House house = Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            if (house.Owner != player.Name)
            {
                player.SendNotification("~r~Solo el dueño puede hacer esto.");
                return;
            }

            if (house.Money > 0)
            {
                player.SendNotification("~r~Vacíe la casa segura antes de vender la casa.");
                return;
            }

            if (house.Weapons.Count > 0)
            {
                player.SendNotification("~r~Vacía el arma casillero de la casa antes de vender la casa.");
                return;
            }

            if (house.Furnitures.Count > 0)
            {
                player.SendNotification("~r~Vende los muebles antes de vender la casa.");
                return;
            }

            int price = (int)Math.Round(house.Price * 0.8);
            player.giveMoney(price);

            house.RemoveAllPlayers();
            house.SetOwner(null);

            player.SendNotification(string.Format("~g~Vendió tu casa por ${0:n0}.", price));
        }

        [RemoteEvent("HouseLeave")]
        public void HouseLeave(Client player, object[] arguments)
        {
            if (!player.HasData("InsideHouse_ID")) return;

            House house = Houses.FirstOrDefault(h => h.ID == player.GetData("InsideHouse_ID"));
            if (house == null) return;

            house.RemovePlayer(player);
        }

        [ServerEvent(Event.PlayerDeath)]
        public void OnPlayerDeath(Client player, Client killer, uint reason)
        {
            if (RESET_DIMENSION_ON_DEATH) player.Dimension = 0;
            RemovePlayerFromHouseList(player);
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.IsLogged()) Player.Data.Character[player].InsideHouseId = player.GetData("InsideHouse_ID");

            RemovePlayerFromHouseList(player);
            foreach (House house in Houses.Where(h => h.Owner == player.Name)) house.SetOwnerHandle(null);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (House house in Houses)
            {
                house.Save(true);
                house.Destroy(true);
            }

            Houses.Clear();
        }
        #endregion
    }
}
