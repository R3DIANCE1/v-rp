﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var main_menu = null;

var face_list = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45];

var selected_face_1 = 0;
var selected_face_2 = 0;
var selected_face_mix = 0;

API.onServerEventTrigger.connect(function (event_name, args) {
    switch (event_name) {
        case "BusinessSurgeon":
            var player = API.getLocalPlayer();
            if (API.getEntityModel(player) == 1885233650 || API.getEntityModel(player) == -1667301416) {
                if (main_menu == null) {
                    main_menu = API.createMenu("Cirujano", "Seleccione una opcion", 0, 0, 6);
                    main_menu.OnItemSelect.connect(function (menu, item, index) {
                        if (index == 4) {
                            API.triggerServerEvent("BusinessPurchaseSurgery", selected_face_1, selected_face_2, selected_face_mix);
                            main_menu.Visible = false;
                            API.setActiveCamera(null);
                        }
                    });

                    main_menu.OnMenuClose.connect(function (sender) {
                        API.triggerServerEvent("BusinessCloseSurgeonMenu");
                        API.setActiveCamera(null);
                    });

                    // Faces
                    var faceList = new List(String);
                    for (var i = 0; i < face_list.length; i++) faceList.Add(i.toString());

                    var faceListItem = API.createListItem("Rosto #1", "", faceList, 0);
                    main_menu.AddItem(faceListItem);
                    faceListItem.OnListChanged.connect(function (sender, new_index) {
                        selected_face_1 = face_list[new_index];

                        var skinFirstId = API.getEntitySyncedData(player, "GTAO_SKIN_FIRST_ID");
                        var skinSecondId = API.getEntitySyncedData(player, "GTAO_SKIN_SECOND_ID");
                        var skinMix = API.getEntitySyncedData(player, "GTAO_SKIN_MIX");

                        API.setPlayerHeadBlendData(player, selected_face_1, selected_face_2, 0, skinFirstId, skinSecondId, 0, selected_face_mix, skinMix, 0, false);
                    });

                    var faceList = new List(String);
                    for (var i = 0; i < face_list.length; i++) faceList.Add(i.toString());

                    var faceListItem = API.createListItem("Rosto #2", "", faceList, 0);
                    main_menu.AddItem(faceListItem);
                    faceListItem.OnListChanged.connect(function (sender, new_index) {
                        selected_face_2 = face_list[new_index];

                        var skinFirstId = API.getEntitySyncedData(player, "GTAO_SKIN_FIRST_ID");
                        var skinSecondId = API.getEntitySyncedData(player, "GTAO_SKIN_SECOND_ID");
                        var skinMix = API.getEntitySyncedData(player, "GTAO_SKIN_MIX");

                        API.setPlayerHeadBlendData(player, selected_face_1, selected_face_2, 0, skinFirstId, skinSecondId, 0, selected_face_mix, skinMix, 0, false);
                    });

                    var faceMix = new List(String);
                    for (var i = 1; i <= 9; i++) faceMix.Add((i / 10).toString());

                    var faceMixItem = API.createListItem("Rosto mix", "", faceMix, 0);
                    main_menu.AddItem(faceMixItem);
                    faceMixItem.OnListChanged.connect(function (sender, new_index) {
                        selected_face_mix = new_index / 10;

                        var skinFirstId = API.getEntitySyncedData(player, "GTAO_SKIN_FIRST_ID");
                        var skinSecondId = API.getEntitySyncedData(player, "GTAO_SKIN_SECOND_ID");
                        var skinMix = API.getEntitySyncedData(player, "GTAO_SKIN_MIX");

                        API.setPlayerHeadBlendData(player, selected_face_1, selected_face_2, 0, skinFirstId, skinSecondId, 0, selected_face_mix, skinMix, 0, false);
                    });

                    // Price
                    var item = API.createMenuItem("Precio", "Precio de la cirugía");
                    item.SetRightLabel("$10000");
                    main_menu.AddItem(item);

                    // Purchase
                    var item_c = API.createColoredItem("Pagar", "Seleccione esta opción para pagar.", "#4caf50", "#ffffff");
                    main_menu.AddItem(item_c);
                }
                selected_face_1 = API.getEntitySyncedData(player, "GTAO_SHAPE_FIRST_ID");
                selected_face_2 = API.getEntitySyncedData(player, "GTAO_SHAPE_SECOND_ID");
                selected_face_mix = API.getEntitySyncedData(player, "GTAO_SHAPE_MIX");

                main_menu.Visible = true;

                // Position
                NAPI.Entity.SetEntityPosition(API.getLocalPlayer(), new Vector3(402.9198, -996.5348, -99.40024));
                NAPI.Entity.SetEntityRotation(API.getLocalPlayer(), new Vector3(0.0, 0.0, 176.8912));
                API.callNative("CLEAR_PED_TASKS_IMMEDIATELY", API.getLocalPlayer());

                // Camera start
                var newCam = API.createCamera(new Vector3(403.6378, -998.5422, -99.00404), new Vector3());
                API.pointCameraAtPosition(newCam, new Vector3(402.9198, -996.5348, -99.00024));
                API.setActiveCamera(newCam);

                // Camera head
                var camPos = new Vector3(402.9378, -997.0, -98.35);
                var camRot = new Vector3(0.0, 0.0, 176.891);
                var camera = API.createCamera(camPos, camRot);
                API.pointCameraAtPosition(camera, new Vector3(402.9198, -996.5348, -98.35));
                API.interpolateCameras(API.getActiveCamera(), camera, 2000, false, false);
            }
            else {
                API.SendChatMessage("~r~ERROR: ~w~No puedes cambiar tu rosto.");
            }
            break;
    }
});

API.onUpdate.connect(() => {
    if (main_menu != null && main_menu.Visible) API.disableAllControlsThisFrame();
});
