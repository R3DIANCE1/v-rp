﻿using GTANetworkAPI;
using System.Collections.Generic;

namespace gtalife.src.Gameplay.Business
{
    #region BusinessType Class
    public class BusinessType
    {
        public string Name { get; }

        public BusinessType(string name)
        {
            Name = name;
        }
    }
    #endregion

    public class BusinessTypes : Script
    {
        public static List<BusinessType> BusinessTypeList = new List<BusinessType>
        {
            // name
            new BusinessType("24-7"),
            new BusinessType("Gasolinera"),
            new BusinessType("Ropas"),
            new BusinessType("Alquiler"),
            new BusinessType("Bar"),
            new BusinessType("Anuncio"),
            new BusinessType("Peluquero"),
            new BusinessType("Cirujano")
        };
    }
}
