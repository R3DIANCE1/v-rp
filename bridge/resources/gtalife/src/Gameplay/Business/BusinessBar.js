"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var business_menu = null;
var products = [
    { name: "Agua", price: "5", description: "Un líquido incoloro, transparente, inodoro e insípido que forma los mares, lagos, ríos y la lluvia y es la base de los fluidos de los organismos vivos." },
    { name: "Cerveza", price: "25", description: "Bebida producida a partir de la fermentación de cereales, principalmente la cebada maltada. Se cree que fue una de las primeras bebidas alcohólicas que fueron creadas por el ser humano." },
    { name: "Whisky", price: "50", description: "El whisky es una bebida alcohólica destilada de granos, a menudo incluyendo malta, que ha sido envejecida en barricas." },
    { name: "Vodka", price: "100", description: "Una popular bebida destilada, incolora, casi sin sabor y con un grado alcohólico." },
];
API.onServerEventTrigger.connect(function (event_name, args) {
    switch (event_name) {
        case "BusinessBar":
            var data = JSON.parse(args[0]);
            if (business_menu == null) {
                business_menu = API.createMenu("Bebidas", "Seleccione una bebida", 0, 0, 6);
                // API.setMenuBannerSprite(business_menu, "shopui_title_liqourstore3", "shopui_title_liqourstore3");
                business_menu.OnItemSelect.connect(function (menu, item, index) {
                    API.triggerServerEvent("BusinessBuyDrink", JSON.stringify(products[index]));
                });
            }
            // business main menu
            business_menu.Clear();
            for (var i = 0, len = products.length; i < len; i++) {
                var temp_item = API.createMenuItem(`${products[i].name}`, `${products[i].description}`);
                temp_item.SetRightLabel(`$${products[i].price}`);
                business_menu.AddItem(temp_item);
            }
            business_menu.Visible = true;
            break;
    }
});
