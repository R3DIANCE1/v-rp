﻿using GTANetworkAPI;
using gtalife.src.Managers;
using Newtonsoft.Json;

namespace gtalife.src.Gameplay.Business
{
    #region BusinessVehicle Class
    public class BusinessVehicle
    {
        public uint Model { get; }
        public int PrimaryColor { get; }
        public int SecondaryColor { get; }

        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }

        public int Price { get; }

        [JsonIgnore]
        public Vehicle Vehicle { get; private set; }

        [JsonIgnore]
        public TextLabel Label { get; private set; }

        public BusinessVehicle(uint model, int color1, int color2, Vector3 position, Vector3 rotation, int price)
        {
            Model = model;
            PrimaryColor = color1;
            SecondaryColor = color2;

            Position = position;
            Rotation = rotation;

            Price = price;
        }

        public void Create(uint dimension = 0)
        {
            Vehicle = VehicleManager.CreateVehicle((VehicleHash)Model, Position - new Vector3(0.0, 0.0, 0.3), Rotation, PrimaryColor, SecondaryColor, dimension);
            Vehicle.Invincible = true;
            Vehicle.EngineStatus = false;
            Vehicle.FreezePosition = true;

            Label = NAPI.TextLabel.CreateTextLabel($"~y~{Vehicle.DisplayName}~s~\nPrecio: ~g~${Price}", new Vector3(0, 0, 0), 15f, 0.5f, 1, new Color(255, 255, 255, 255), dimension: dimension);
            NAPI.Entity.AttachEntityToEntity(Label, Vehicle, "0", new Vector3(0, 0, 0), new Vector3(0, 0, 0));
        }

        public void Destroy()
        {
            if (Vehicle != null) Vehicle.Delete();
            if (Label != null) Label.Delete();
        }
    }
    #endregion
}
