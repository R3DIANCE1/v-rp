﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.Gang
{
    public class Main : Script
    {
        // settings
        public static string GANG_SAVE_DIR = "data/GangData";
        public static string GANG_ZONE_SAVE_DIR = "data/GangZoneData";
        public static int PLAYER_GANG_LIMIT = 1;
        public static int SAVE_INTERVAL = 120;

        public static List<Gang> Gangs = new List<Gang>();
        public static List<GangZone> GangZones = new List<GangZone>();

        [ServerEvent(Event.Update)]
        public void OnUpdate()
        {
            foreach (var zone in GangZones)
            {
                if (!zone.IsUnderAttack) continue;

                if (zone.PlayersInside.Count(x => x.GetGangId() == zone.Attacker) < 1)
                {
                    Gang gang = Gangs.FirstOrDefault(z => z.ID == zone.Attacker);
                    if (gang == null) continue;

                    SendGangMessageToAll($"~y~INFO: ~w~La gang {gang.Name} ha fallado el ataque a la zona.");
                    zone.SetUnderAttack(false);
                    break;
                }

                // finish attack
                if (zone.AttackStartedAt.AddMinutes(3) < DateTime.Now)
                {
                    Gang gang = Gangs.FirstOrDefault(z => z.ID == zone.Attacker);
                    if (gang == null) continue;

                    SendGangMessageToAll($"~y~INFO: ~w~La gang {gang.Name} ha dominado la zona.");
                    zone.SetOwner(gang.ID);
                    zone.SetUnderAttack(false);
                    break;
                }
            }
        }

        [RemoteEvent("GangZoneInteract")]
        public void GangZoneInteract(Client sender, object[] arguments)
        {
            if (!sender.HasData("GangZoneMarker_ID")) return;

            GangZone gangzone = GangZones.FirstOrDefault(z => z.ID == sender.GetData("GangZoneMarker_ID"));
            if (gangzone == null) return;

            if (!sender.IsGangMember()) sender.SendChatMessage("~r~ERROR: ~w~No es miembro de gang.");
            else if (gangzone.GangID == sender.GetGangId()) sender.SendChatMessage("~r~ERROR: ~w~Tu gang ya tiene el control de esta zona.");
            else if (gangzone.IsUnderAttack) sender.SendChatMessage("~r~ERROR: ~w~Esta zona ya está bajo ataque.");
            else
            {
                Gang gang = Gangs.FirstOrDefault(g => g.ID == sender.GetGangId());
                if (gang == null)
                {
                    sender.SendChatMessage("~r~ERROR: ~w~No es miembro de gang.");
                    return;
                }

                SendGangMessageToAll($"~y~INFO: ~w~Una gangzone está siendo atacada por {gang.Name}.");
                SendGangMessage(gang, "~y~INFO: ~w~Al menos un miembro de tu gang debe estar vivo en la zona durante 3 minutos para dominarlo.");
                gangzone.SetUnderAttack(true, gang.ID);
            }
        }

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Gangs.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }

        public static void SendGangMessage(Gang gang, string message)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                if (!player.IsLogged()) continue;
                else if (Player.Data.Character[player].Gang != gang.ID) continue;

                player.SendChatMessage(message);
            }
        }

        public static void SendGangMessageToAll(string message)
        {
            foreach (var player in NAPI.Pools.GetAllPlayers())
            {
                if (!player.IsLogged()) continue;
                else if (Player.Data.Character[player].Gang == null) continue;

                player.SendChatMessage(message);
            }
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // load settings
            if (NAPI.Resource.HasSetting(this, "gangDirName")) GANG_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "gangDirName");

            GANG_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + GANG_SAVE_DIR;
            if (!Directory.Exists(GANG_SAVE_DIR)) Directory.CreateDirectory(GANG_SAVE_DIR);

            GANG_ZONE_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + GANG_ZONE_SAVE_DIR;
            if (!Directory.Exists(GANG_ZONE_SAVE_DIR)) Directory.CreateDirectory(GANG_ZONE_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "playerGangLimit")) PLAYER_GANG_LIMIT = NAPI.Resource.GetSetting<int>(this, "playerGangLimit");
            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            NAPI.Util.ConsoleOutput("-> Player Gang Limit: {0}", ((PLAYER_GANG_LIMIT == 0) ? "Disabled" : PLAYER_GANG_LIMIT.ToString()));

            // load gangs
            foreach (string file in Directory.EnumerateFiles(GANG_SAVE_DIR, "*.json"))
            {
                Gang gang = JsonConvert.DeserializeObject<Gang>(File.ReadAllText(file));
                Gangs.Add(gang);
            }

            // load gang zones
            foreach (string file in Directory.EnumerateFiles(GANG_ZONE_SAVE_DIR, "*.json"))
            {
                GangZone zone = JsonConvert.DeserializeObject<GangZone>(File.ReadAllText(file));
                GangZones.Add(zone);
            }

            NAPI.Util.ConsoleOutput("Loaded {0} gangs.", Gangs.Count);
            NAPI.Util.ConsoleOutput("Loaded {0} gangzones.", GangZones.Count);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Gang gang in Gangs)
            {
                gang.Save(true);
            }

            Gangs.Clear();
        }
        #endregion
    }
}
