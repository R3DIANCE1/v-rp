﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace gtalife.src.Gameplay.Gang
{
    public enum GangType
    {
        Gang,
        Mafia
    }

    public class Gang
    {
        public Guid ID { get; }
        public int CharacterId { get; private set; }
        public GangType Type { get; private set; }
        public List<string> Ranks { get; set; }

        // customization
        public string Name { get; private set; }

        [JsonIgnore]
        private DateTime LastSave;

        public Gang(Guid id, int characterid, string name = "", GangType type = GangType.Gang, List<string> ranks = null)
        {
            ID = id;
            CharacterId = characterid;
            Type = type;
            Ranks = ranks ?? new List<string>() { "Jefe", "Miembro", "Miembro", "Miembro", "Miembro", "Miembro" };

            Name = name;
        }

        public void Save(bool force = false)
        {
            if (!force && DateTime.Now.Subtract(LastSave).TotalSeconds < Main.SAVE_INTERVAL) return;

            File.WriteAllText(Main.GANG_SAVE_DIR + Path.DirectorySeparatorChar + ID + ".json", JsonConvert.SerializeObject(this, Formatting.Indented));
            LastSave = DateTime.Now;
        }
    }
}
