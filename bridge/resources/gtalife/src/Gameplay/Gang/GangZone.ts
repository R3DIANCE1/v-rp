﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var gz_text = 0;

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    switch (eventName) {
        case "ShowGangZoneText":
            gz_text = args[0];
            break;
    }
});

API.onKeyDown.connect((sender: any, e: System.Windows.Forms.KeyEventArgs) => {
    if (API.isChatOpen()) return;

    if (e.KeyCode === Keys.E) {
        if (gz_text > 0) {
            API.triggerServerEvent("GangZoneInteract");
        }
    }
});

API.onUpdate.connect(() => {
    if (gz_text > 0) API.displaySubtitle("Presione ~y~E ~w~para interactuar.", 100);
});