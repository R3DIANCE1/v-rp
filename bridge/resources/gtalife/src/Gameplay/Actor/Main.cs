﻿using GTANetworkAPI;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace gtalife.src.Gameplay.Actor
{
    public class Main : Script
    {
        // settings
        public static string ACTOR_SAVE_DIR = "data/Actors";
        public static int SAVE_INTERVAL = 120;

        public static List<Actor> Actors = new List<Actor>();

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Actors.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // load settings
            if (NAPI.Resource.HasSetting(this, "actorDirName")) ACTOR_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "actorDirName");

            ACTOR_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + ACTOR_SAVE_DIR;
            if (!Directory.Exists(ACTOR_SAVE_DIR)) Directory.CreateDirectory(ACTOR_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            // load parking lots
            foreach (string file in Directory.EnumerateFiles(ACTOR_SAVE_DIR, "*.json"))
            {
                Actor actor = JsonConvert.DeserializeObject<Actor>(File.ReadAllText(file));
                Actors.Add(actor);
            }

            NAPI.Util.ConsoleOutput("Loaded {0} actors.", Actors.Count);
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (Actor actor in Actors)
            {
                actor.Save(true);
                actor.Destroy();
            }

            Actors.Clear();
        }
        #endregion
    }
}
