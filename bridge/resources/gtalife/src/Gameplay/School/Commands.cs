﻿using GTANetworkAPI;
using gtalife.src.Admin;
using gtalife.src.Managers;
using gtalife.src.Player.Utils;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.School
{
    class Commands : Script
    {
        [Command("escuelacmds")]
        public void CommandsCommand(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Escuela ~~~~~~~~~~~");
            NAPI.Chat.SendChatMessageToPlayer(player, "* /crearescuela - /borrarescuela");
            NAPI.Chat.SendChatMessageToPlayer(player, "!{#a5f413}~~~~~~~~~~~ Comandos Escuela  ~~~~~~~~~~~");
        }

        [Command("crearescuela")]
        public void CMD_CreateSchool(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            School new_school = new School(Main.GetGuid(), player.Position);
            new_school.Save();

            Main.Schools.Add(new_school);
            NAPI.Chat.SendChatMessageToPlayer(player, $"~g~EXITO: ~s~Creaste una escuela.");
        }

        [Command("borrarescuela")]
        public void CMD_RemoveSchool(Client player)
        {
            if (!player.IsAdmin())
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes permison.");
                return;
            }

            else if (!player.HasData("SchoolMarker_ID"))
            {
                player.SendChatMessage("~r~ERROR: ~w~Párese en el marcador de entrada de la escuela que desea borrar.");
                return;
            }

            School school = Main.Schools.FirstOrDefault(h => h.ID == player.GetData("SchoolMarker_ID"));
            if (school == null) return;

            school.Destroy();
            Main.Schools.Remove(school);

            string school_file = Main.SCHOOL_SAVE_DIR + Path.DirectorySeparatorChar + school.ID + ".json";
            if (File.Exists(school_file)) File.Delete(school_file);
            NAPI.Chat.SendChatMessageToPlayer(player, $"~g~EXITO: ~s~Borraste la escuela.");
        }

        [Command("licencias", "~y~USO: ~w~/licencias [jugador]", GreedyArg = true)]
        public void CMD_ShowLicenses(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (target.Position.DistanceTo(player.Position) > 2f) player.SendChatMessage("~r~ERROR: ~w~No estás cerca del jugador.");
            else
            {
                string hasLicense = player.getDrivingLicense() ? "Sí" : "No";
                player.sendChatAction($"muestra las licencias para {target.Name}.");
                NAPI.Chat.SendChatMessageToPlayer(target, $"!{{#a5f413}}~~~~~~~~~~~~~~~~ Licencias de {player.Name} ~~~~~~~~~~~~~~~~");
                NAPI.Chat.SendChatMessageToPlayer(target, $"Licencia de conducción: {hasLicense}");
            }
        }
    }
}
