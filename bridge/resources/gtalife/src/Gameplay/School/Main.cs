﻿using GTANetworkAPI;
using gtalife.src.Player.Utils;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace gtalife.src.Gameplay.School
{
    public enum SchoolPrice
    {
        Driving = 400
    }

    public class Main : Script
    {
        // settings
        public static string SCHOOL_SAVE_DIR = "data/SchoolData";
        public static int SAVE_INTERVAL = 120;

        public static List<School> Schools = new List<School>();

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            if (player.HasData("SCHOOL_VEHICLE"))
            {
                Vehicle veh = player.GetData("SCHOOL_VEHICLE");
                veh.Delete();
            }
        }

        [ServerEvent(Event.PlayerExitVehicle)]
        public void OnPlayerExitVehicle(Client player, Vehicle vehicle)
        {
            if (player.HasData("SCHOOL_VEHICLE"))
            {
                Vehicle veh = player.GetData("SCHOOL_VEHICLE");
                veh.Delete();

                if (!player.getDrivingLicense())
                {
                    player.SendChatMessage("~y~INFO: ~w~Fallaste la prueba.");
                    player.TriggerEvent("School_DeleteEntites");
                    player.ResetData("SCHOOL_VEHICLE");
                }
            }
        }

        #region Methods
        public static Guid GetGuid()
        {
            Guid new_guid;

            do
            {
                new_guid = Guid.NewGuid();
            } while (Schools.Count(h => h.ID == new_guid) > 0);

            return new_guid;
        }
        #endregion

        #region Events
        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // load settings
            if (NAPI.Resource.HasSetting(this, "schoolDirName")) SCHOOL_SAVE_DIR = NAPI.Resource.GetSetting<string>(this, "schoolDirName");

            SCHOOL_SAVE_DIR = NAPI.Resource.GetResourceFolder(this) + Path.DirectorySeparatorChar + SCHOOL_SAVE_DIR;
            if (!Directory.Exists(SCHOOL_SAVE_DIR)) Directory.CreateDirectory(SCHOOL_SAVE_DIR);

            if (NAPI.Resource.HasSetting(this, "saveInterval")) SAVE_INTERVAL = NAPI.Resource.GetSetting<int>(this, "saveInterval");

            // load schools
            foreach (string file in Directory.EnumerateFiles(SCHOOL_SAVE_DIR, "*.json"))
            {
                School school = JsonConvert.DeserializeObject<School>(File.ReadAllText(file));
                Schools.Add(school);
            }

            NAPI.Util.ConsoleOutput("Loaded {0} schools.", Schools.Count);
        }

        [RemoteEvent("SchoolInteract")]
        public void SchoolInteract(Client player, object[] arguments)
        {
            if (!player.HasData("SchoolMarker_ID")) return;

            School school = Schools.FirstOrDefault(h => h.ID == player.GetData("SchoolMarker_ID"));
            if (school == null) return;

            player.TriggerEvent("School_PurchaseMenu", NAPI.Util.ToJson(new { Price = SchoolPrice.Driving }));
        }

        [RemoteEvent("School_StartTest")]
        public void SchoolStartTest(Client player, object[] arguments)
        {
            if (player.getDrivingLicense())
            {
                player.SendChatMessage("~r~ERROR: ~w~Ya tienes esta licencia.");
                return;
            }
            else if (player.getMoney() < (int)SchoolPrice.Driving)
            {
                player.SendChatMessage("~r~ERROR: ~w~No tienes suficiente dinero.");
                return;
            }

            player.giveMoney(-(int)SchoolPrice.Driving);
            player.TriggerEvent("School_ShowDrivingTheoric");
        }

        [RemoteEvent("School_FinishDrivingTheoric")]
        public void SchoolFinishDrivingTheoric(Client player, object[] arguments)
        {
            int correct_answers = (int)arguments[0];
            if (correct_answers < 7)
            {
                player.SendChatMessage($"~y~INFO: ~w~Lo siento, pero solo el {correct_answers * 10}% de las respuestas fueron correctas.");
                return;
            }
            player.SendChatMessage("~y~INFO: ~w~Si abandona el vehículo, suspenderá la prueba.");

            var vehicle = NAPI.Vehicle.CreateVehicle(-1177863319, new Vector3(-258.157, 6251.224, 30.9673), new Vector3(0.4668645, -0.007162704, 47.22823), 27, 27);
            vehicle.EngineStatus = false;
            NAPI.Player.SetPlayerIntoVehicle(player, vehicle, -1);

            player.SetData("SCHOOL_VEHICLE", vehicle);
            player.TriggerEvent("School_ShowDrivingPractical");
        }

        [RemoteEvent("School_FinishDrivingPractical")]
        public void SchoolFinishDrivingPractical(Client player, object[] arguments)
        {
            player.setDrivingLicense(true);
            NAPI.Player.WarpPlayerOutOfVehicle(player);
            player.SendChatMessage("~y~INFO: ~w~Terminaste la prueba y obtuviste tu licencia.");
        }

        [ServerEvent(Event.ResourceStop)]
        public void OnResourceStop()
        {
            foreach (School school in Schools)
            {
                school.Save(true);
                school.Destroy(true);
            }

            Schools.Clear();
        }
        #endregion
    }
}
