"use strict";
/// <reference path='../../../../types-gt-mp/Definitions/index.d.ts' />
var correct_answers = 0;
var answers_menu = null;
var count = 0;
var blip = null;
var marker = null;
var questions = [
    "1.- ¿Cuando el semáforo está en amarillo, qué debo hacer?",
    "2.- ¿A qué edad puedo sacarme la licencia de conducción?",
    "3.- ¿Cómo influye el consumo de cocaina en la conducción?",
    "4.- ¿Qué debe hacer si un agente le hace señales luminosas?",
    "5. ¿Qué vehículos puedo manejar con la licencia de conducción?",
    "6. ¿Qué debe hacer si su vehículo se avería en plena carretera?",
    "7. ¿Puede conducir bajo los efectos de el alcohol?",
    "8. ¿A qué velocidad puedo circular en la ciudad?",
    "9. ¿Cómo debe de estar el coche al utilizar una gasolinera?",
    "10. ¿Están permitidas las carreras en SA?"
];
var answers = [
    [
        { text: "a. Acelerar, ya que es igual al rojo.", correct: false },
        { text: "b. Puedo pasar con precaución.", correct: false },
        { text: "c. Debo pararme y actuar como si estuviese en rojo.", correct: true },
    ],
    [
        { text: "a. Desde que cumpla 5 años.", correct: false },
        { text: "b. Desde que cumpla 21 años.", correct: true },
        { text: "c. No existe una edad especifica.", correct: false },
    ],
    [
        { text: "a. Aumenta la agresividad y la fatiga.", correct: true },
        { text: "b. Te relaja y aumenta la capacidad de concentración.", correct: false },
        { text: "c. Aumenta el campo visual.", correct: false },
    ],
    [
        { text: "a. Iniciar una persecucción.", correct: false },
        { text: "b. Pararme delante de el a la derecha y seguir indicaciones.", correct: true },
        { text: "c. Nada, porque no soy ciudadano de San Andreas.", correct: false },
    ],
    [
        { text: "a. Todos los vehículos.", correct: false },
        { text: "b. Turismos y cohetes espaciales.", correct: false },
        { text: "c. Turismos y motocicletas.", correct: true },
    ],
    [
        { text: "a. Lo dejo tirado y robo otro, nadie se dará cuenta.", correct: false },
        { text: "b. Lo estaciono en un lugar donde no estorbe, y llamo a la grua", correct: true },
        { text: "c. Llamo a mi primo para que me lo remolque.", correct: false },
    ],
    [
        { text: "a. Sí, este ayuda a mi concentración mientras conduzco.", correct: false },
        { text: "b. Sí, voy rápido para que no me pille la policía.", correct: false },
        { text: "c. Está totalmente prohibido conducir borracho.", correct: true },
    ],
    [
        { text: "a. A la máxima que de mi vehículo, conduzco bien y no voy a chocar.", correct: false },
        { text: "b. 50 km/h", correct: true },
        { text: "c. 300 kh/h", correct: false },
    ],
    [
        { text: "a. Con el motor apagado, para evitar incendios.", correct: true },
        { text: "b. Con la música a todo volumen para que se enteren de que estoy aquí.", correct: false },
        { text: "c. En marcha para irme corriendo sin pagar.", correct: false },
    ],
    [
        { text: "a. Sí, participaré en una que hace mi hermano todos los sabados.", correct: false },
        { text: "b. Están totalmente prohibidas las carreras sin expresa autorización. ", correct: true },
        { text: "c. Solo en sitios escondidos.", correct: false },
    ]
];
var checkpoints = [
    { x: -294.0597, y: 6244.595, z: 30.40641 },
    { x: -314.0506, y: 6244.914, z: 30.48676 },
    { x: -357.5120, y: 6301.065, z: 28.87362 },
    { x: -181.8237, y: 6462.721, z: 29.64107 },
    { x: -158.5746, y: 6461.502, z: 30.08678 },
    { x: -127.4175, y: 6430.999, z: 30.42851 },
    { x: -258.3565, y: 6251.153, z: 30.48921 }
];
API.onServerEventTrigger.connect((eventName, args) => {
    switch (eventName) {
        case "School_ShowDrivingTheoric":
            if (answers_menu == null) {
                answers_menu = API.createMenu("Autoescuela", "Seleccione una respuesta", 0, 0, 6);
                var temp_item;
                temp_item = API.createMenuItem("A", " ");
                answers_menu.AddItem(temp_item);
                temp_item = API.createMenuItem("B", " ");
                answers_menu.AddItem(temp_item);
                temp_item = API.createMenuItem("C", " ");
                answers_menu.AddItem(temp_item);
                answers_menu.OnItemSelect.connect(function (menu, item, index) {
                    if (answers[count][index].correct)
                        correct_answers++;
                    count++;
                    if (count == questions.length) {
                        answers_menu.Visible = false;
                        API.triggerServerEvent("School_FinishDrivingTheoric", correct_answers);
                    }
                    else {
                        API.SendChatMessage(" ");
                        API.SendChatMessage(questions[count]);
                        for (let i = 0; i < answers[count].length; i++)
                            API.SendChatMessage(answers[count][i].text);
                    }
                });
            }
            count = 0;
            correct_answers = 0;
            answers_menu.Visible = true;
            API.SendChatMessage(" ");
            API.SendChatMessage("~y~~~~~~~~~~~~~~~~~~~~~ Driving Test ~~~~~~~~~~~~~~~~~~~~");
            API.SendChatMessage(questions[count]);
            for (let i = 0; i < answers[count].length; i++)
                API.SendChatMessage(answers[count][i].text);
            break;
        case "School_ShowDrivingPractical":
            if (blip != null)
                NAPI.Entity.DeleteEntity(blip);
            if (marker != null)
                NAPI.Entity.DeleteEntity(marker);
            count = 0;
            blip = NAPI.Blip.CreateBlip(new Vector3(checkpoints[count].x, checkpoints[count].y, checkpoints[count].z));
            API.setBlipSprite(blip, 1);
            API.setBlipColor(blip, 1);
            API.setBlipRouteVisible(blip, true);
            marker = NAPI.Marker.CreateMarker(1, new Vector3(checkpoints[count].x, checkpoints[count].y, checkpoints[count].z), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
            break;
        case "School_DeleteEntites":
            if (blip != null)
                NAPI.Entity.DeleteEntity(blip);
            if (marker != null)
                NAPI.Entity.DeleteEntity(marker);
            break;
    }
});
API.onUpdate.connect(() => {
    if (marker != null && blip != null && count < checkpoints.length) {
        var player = API.getLocalPlayer();
        if (API.getEntityPosition(player).DistanceTo(new Vector3(checkpoints[count].x, checkpoints[count].y, checkpoints[count].z)) < 2) {
            count++;
            NAPI.Entity.DeleteEntity(blip);
            NAPI.Entity.DeleteEntity(marker);
            if (count == checkpoints.length) {
                API.triggerServerEvent("School_FinishDrivingPractical");
                return;
            }
            blip = NAPI.Blip.CreateBlip(new Vector3(checkpoints[count].x, checkpoints[count].y, checkpoints[count].z));
            API.setBlipSprite(blip, 1);
            API.setBlipColor(blip, 1);
            API.setBlipRouteVisible(blip, true);
            marker = NAPI.Marker.CreateMarker(1, new Vector3(checkpoints[count].x, checkpoints[count].y, checkpoints[count].z), new Vector3(), new Vector3(), new Vector3(1, 1, 1), 255, 0, 0, 255);
        }
    }
});
