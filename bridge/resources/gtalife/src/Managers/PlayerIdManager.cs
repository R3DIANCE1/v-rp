﻿using GTANetworkAPI;
using System.Collections.Generic;
using System.Linq;

namespace gtalife.src.Managers
{
    class PlayeridManager : Script
    {
        public static List<Client> Players = new List<Client>();

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            for (var i = 0; i < NAPI.Server.GetMaxPlayers(); i++)
            {
                Players.Add(null);
            }
        }

        [ServerEvent(Event.PlayerConnected)]
        public void OnPlayerConnected(Client player)
        {
            int index = GetFreeId();
            if (index != -1)
            {
                Players[index] = player;
            }
        }

        [ServerEvent(Event.PlayerDisconnected)]
        public void OnPlayerDisconnected(Client player, DisconnectionType type, string reason)
        {
            int index = Players.IndexOf(player);
            if (index != -1)
            {
                Players[index] = null;
            }
        }

        public static Client FindPlayer(string idOrName)
        {
            if (int.TryParse(idOrName, out int id))
            {
                if (Players.ElementAtOrDefault(id) != null)
                    return Players[id];                
            }

            if (Players.FindAll(x => x != null && x.Name.ToLower().Contains(idOrName)).Count == 1)
            {
                return Players.FirstOrDefault(x => x.Name.ToLower().Contains(idOrName));
            }
            return null;
        }

        public static int GetPlayerId(Client player)
        {
            return Players.FindIndex(x => x != null && x.Name == player.Name);
        }

        private int GetFreeId()
        {
            foreach (var player in Players)
            {
                if (player == null)
                {
                    return Players.IndexOf(player);
                }
            }
            return -1;
        }
    }
}
