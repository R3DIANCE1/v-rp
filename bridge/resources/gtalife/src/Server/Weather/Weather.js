"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
API.onServerEventTrigger.connect((eventName, args) => {
    if (eventName == "SetClientWeather") {
        var currWeather = args[0];
        var nextWeather = args[1];
        var transition = args[2];
        API.setWeather(currWeather);
        API.setNextWeather(nextWeather);
        API.setWeatherTransitionType(transition);
    }
});
