﻿using GTANetworkAPI;
using gtalife.src.Player.Phone;
using gtalife.src.Player.Utils;

namespace gtalife.src.Server.Chat
{
    class Chat : Script
    {
        [ServerEvent(Event.ChatMessage)]
        public void OnChatMessage(Client player, string message)
        {
            if (player.IsLogged())
            {
                bool isInCall = Phone.PlayersInCall.ContainsKey(player);
                message = (!isInCall) ? $"{player.Name} dice: {message}" : $"(Teléfono) {player.Name} dice: {message}";

                if (isInCall)
                {
                    player.SendChatMessage($"!{{#FFFF00}}{message}");
                    var target = Phone.PlayersInCall[player];
                    if (Phone.PlayersInCall.ContainsKey(target))
                        target.SendChatMessage($"!{{#FFFF00}}{message}");
                }

                var players = player.getPlayersInRadiusOfPlayer(15f);
                foreach (var p in players)
                {
                    if (p.Dimension != player.Dimension) continue;
                        
                    if (isInCall)
                    {
                        if (p == player) continue;

                        if (Phone.PlayersInCall.ContainsKey(Phone.PlayersInCall[player]) && p == Phone.PlayersInCall[player]) continue;
                    }

                    p.SendChatMessage(message);
                }
            }
        }
    }
}
