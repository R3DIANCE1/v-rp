﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var mainBrowser = null;

API.onResourceStart.connect(function () {
    var res = API.getScreenResolution();

    var size_x = res.Width / 1.5;
    var size_y = res.Height / 1.25;

    var pos_x = res.Width / 2 - size_x / 2;
    var pos_y = res.Height / 2 - size_y / 2;

    mainBrowser = API.createCefBrowser(size_x, size_y);
    API.setCefBrowserPosition(mainBrowser, pos_x, pos_y);
    API.setCefBrowserHeadless(mainBrowser, true);
    API.waitUntilCefBrowserInit(mainBrowser);
    API.loadPageCefBrowser(mainBrowser, "res/views/playerlist.html");
});

API.onResourceStop.connect(function () {
    if (mainBrowser !== null) {
        API.destroyCefBrowser(mainBrowser);
    }
});

API.onKeyDown.connect(function (sender, arg) {
    if (arg.KeyCode === Keys.Z) {
        if (!API.isChatOpen() && mainBrowser !== null && !API.isCursorShown() && API.getCefBrowserHeadless(mainBrowser)) {
            API.triggerServerEvent("get_players_list");
        }
    }
});

API.onKeyUp.connect(function (sender, arg) {
    if (arg.KeyCode === Keys.Z) {
        if (!API.getCefBrowserHeadless(mainBrowser)) {
            API.showCursor(false);
            API.setCanOpenChat(true);
            mainBrowser.call("unfocus");
            API.setCefBrowserHeadless(mainBrowser, true);
        }
    }
});

API.onServerEventTrigger.connect(function (name, args) {
    if (name === "show_players_list") {
        API.setCanOpenChat(false);
        API.setCefBrowserHeadless(mainBrowser, false);
        mainBrowser.call("update_players_list", args[0]);
    }
});
