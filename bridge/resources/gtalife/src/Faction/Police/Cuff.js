"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var isCuffed = false;
var controlsToDisable = [12, 13, 14, 15, 16, 17, 24, 25, 37, 44, 45, 47, 58, 69, 70, 92, 114, 140, 141, 142, 143, 257, 263, 264, 331];
API.onServerEventTrigger.connect((eventName, args) => {
    switch (eventName) {
        case "SetPlayerCuffed":
            isCuffed = args[0];
            break;
    }
});
API.onUpdate.connect(() => {
    if (isCuffed)
        for (var i = 0; i < controlsToDisable.length; i++)
            API.disableControlThisFrame(controlsToDisable[i]);
});
