﻿/// <reference path='../../../types-gt-mp/index.d.ts' />

var blips = [];

API.onServerEventTrigger.connect((eventName: string, args: System.Array<any>) => {
    switch (eventName) {
        case "ReceiveInjuredPlayers":
            var players = JSON.parse(args[0]);

            for (let i = 0; i < blips.length; i++) NAPI.Entity.DeleteEntity(blips[i]);
            blips = [];

            for (let i = 0; i < players.length; i++) {
                var blip = NAPI.Blip.CreateBlip(new Vector3(players[i].position.X, players[i].position.Y, players[i].position.Z));
                API.setBlipName(blip, "Jugador lesionado");
                API.setBlipSprite(blip, 310);
                blips.push(blip);
            }
            break;
    }
});
