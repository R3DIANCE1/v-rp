﻿using GTANetworkAPI;
using gtalife.src.Admin;
using gtalife.src.Flags;
using gtalife.src.Managers;
using gtalife.src.Player.Outfit;
using gtalife.src.Player.Utils;
using System.Linq;
using System.Timers;

namespace gtalife.src.Faction.Medic
{
    class Commands : Script
    {
        #region player
        [Command("socorrer")]
        public void CMD_HealInjuredPlayer(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || faction.Type != (int)FactionType.Medic) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (player == target) player.SendChatMessage("~r~ERROR: ~w~Usted no puede socorrerse a sí mismo.");
            else if (player.Position.DistanceTo(target.Position) > 2f) player.SendChatMessage("~r~ERROR: ~w~Usted no está cerca del jugador.");
            else if (!Death.PlayersInjured.ContainsKey(target)) player.SendChatMessage("~r~ERROR: ~w~Este jugador no necesita socorro.");
            else
            {
                player.sendChatAction($"comienza a prestar ayuda en {target.Name}.");
                player.PlayAnimation("amb@medic@standing@tendtodead@enter", "enter", (int)(AnimationFlags.StopOnLastFrame));

                NAPI.Task.Run(() =>
                {
                    NAPI.Player.PlayPlayerAnimation(player, (int)(AnimationFlags.Loop), "amb@medic@standing@tendtodead@idle_a", "idle_a");
                }, delayTime: 1000);

                NAPI.Task.Run(() =>
                {
                    player.PlayAnimation("amb@medic@standing@tendtodead@exit", "exit", 0);
                    target.PlayAnimation("get_up@standard", "front", 0);

                    player.giveMoney(150);
                    target.giveMoney(-150);

                    target.Health = 25;
                    target.RemoveAllWeapons();

                    target.Invincible = false;
                    target.FreezePosition = false;
                    Death.PlayersInjured.Remove(target);

                    target.TriggerEvent("DisableAllControls", false);
                    target.TriggerEvent("StopDeathEffect");
                }, delayTime: 10000);
            }
        }

        [Command("sanar")]
        public void CMD_Heal(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());

            if (target == null) player.SendChatMessage("~r~ERROR: ~w~Demasiados o ningún jugador encontrado.");
            else if (!target.IsLogged()) player.SendChatMessage("~r~ERROR: ~w~El jugador no está conectado.");
            else if (faction == null || faction.Type != (int)FactionType.Medic) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (player == target) player.SendChatMessage("~r~ERROR: ~w~Usted no puede curar usted mismo.");
            else if (!player.IsInVehicle || player.Vehicle.Model != (int)VehicleHash.Ambulance) player.SendChatMessage("~r~ERROR: ~w~Usted necesita estar en una ambulancia.");
            else if (target.Vehicle != player.Vehicle) player.SendChatMessage("~r~ERROR: ~w~El jugador necesita estar en su ambulancia.");
            else if (target.Health >= 100) player.SendChatMessage("~r~ERROR: ~w~El jugador no necesita curaciones.");
            else
            {
                target.Health = 100;
                player.sendChatAction($"hace curativos en {target.Name}.");
            }
        }

        [Command("uniforme")]
        public void CMD_Uniform(Client player)
        {
            Faction faction = Main.Factions.FirstOrDefault(f => f.ID == player.GetFactionID());
            if (faction == null || faction.Type != (int)FactionType.Medic) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else
            {
                if (!player.HasData("IsUsingParamedicUniform"))
                {
                    player.SetOutfit(1197);
                    player.SetData("IsUsingParamedicUniform", true);
                    player.sendChatAction("viste el uniforme.");
                }
                else
                {
                    player.loadClothes();
                    player.ResetData("IsUsingParamedicUniform");
                    player.sendChatAction("desaprovecha el uniforme.");
                }
            }
        }
        #endregion

        #region admin
        [Command("resucitar")]
        public void CMD_Revive(Client player, string idOrName)
        {
            var target = PlayeridManager.FindPlayer(idOrName);
            if (!player.IsAdmin()) player.SendChatMessage("~r~ERROR: ~w~No tienes permiso.");
            else if (!Death.PlayersInjured.ContainsKey(target)) player.SendChatMessage("~r~ERROR: ~w~El jugador no estás herido.");
            else
            {
                target.PlayAnimation("get_up@standard", "front", 0);

                target.Health = 25;

                target.Invincible = false;
                target.FreezePosition = false;
                Death.PlayersInjured.Remove(target);

                target.TriggerEvent("DisableAllControls", false);
                target.TriggerEvent("StopDeathEffect");

                player.SendChatMessage($"~y~INFO: ~w~Has revivido a {target.Name}.");
                if (target != player) target.SendChatMessage($"~y~ADMIN: ~w~{player.Name} has revivido usted.");
            }
        }
        #endregion
    }
}
