"use strict";
/// <reference path='../../../types-gt-mp/Definitions/index.d.ts' />
var disableControls = false;
var controlsToDisable = [12, 13, 14, 15, 16, 17, 22, 23, 24, 25, 37, 44, 45, 47, 58, 69, 70, 92, 114, 140, 141, 142, 143, 257, 263, 264, 266, 267, 268, 269, 331];
API.onServerEventTrigger.connect((eventName, args) => {
    switch (eventName) {
        case "DisableAllControls":
            disableControls = args[0];
            API.disableVehicleEnteringKeys(args[0]);
            break;
        case "StopDeathEffect":
            API.stopScreenEffect("DeathFailMPIn");
            if (args.Length == 0)
                API.playScreenEffect("DeathFailOut", 5000, false);
            break;
    }
});
API.onPlayerDeath.connect((killer, weapon) => {
    API.playScreenEffect("DeathFailMPIn", 1000, true);
});
API.onUpdate.connect(() => {
    if (disableControls)
        for (var i = 0; i < controlsToDisable.length; i++)
            API.disableControlThisFrame(controlsToDisable[i]);
});
