﻿using GTANetworkAPI;

namespace gtalife
{
    public class Main : Script
    {
        public static int MAJOR_VERSION = 0;
        public static int MINOR_VERSION = 3;
        public static int PATCH_VERSION = 1;

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart()
        {
            // Pre-load IPL
            NAPI.World.RequestIpl("apa_v_mp_h_01_a");

            // Tequi-la-la
            NAPI.World.RequestIpl("v_rockclub");

            // Life invader
            NAPI.World.RemoveIpl("facelobbyfake");
            NAPI.World.RequestIpl("facelobby");

            // Jewel store
            NAPI.World.RequestIpl("post_hiest_unload");
            NAPI.World.RemoveIpl("jewel2fake");
            NAPI.World.RemoveIpl("bh1_16_refurb");

            NAPI.World.RequestIpl("bkr_biker_interior_placement_interior_6_biker_dlc_int_ware05_milo");

            NAPI.Server.SetGamemodeName($"vroleplay v{MAJOR_VERSION}.{MINOR_VERSION}.{PATCH_VERSION}");
            NAPI.Server.SetCommandErrorMessage("~r~ERROR: ~w~El comando no existe, mira /cmds.");
            NAPI.Server.SetGlobalServerChat(false);
        }
    }
}
