const NativeUI = require("nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const Point = NativeUI.Point;
const Offset = require("gtalife/util/offsets.js")

var menu = null;

mp.events.add('open_vehicle_menu', () => {
    if (!menu) {
        menu = new Menu("Vehículo", "Selecciona una opción", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));
    
        let capo = new UIMenuItem("Capó", "")
        let maletero = new UIMenuItem("Maletero", "");
        let doors = new UIMenuItem("Puertas", "");
        let engine = new UIMenuItem("Motor", "");
        let lock = new UIMenuItem("Cambiar cerraduras");
        
        menu.AddItem(capo);
        menu.AddItem(maletero);
        menu.AddItem(doors);
        menu.AddItem(engine);
        menu.AddItem(lock);
        
        menu.ItemSelect.on((item, index) => {
            if (item == capo) mp.events.callRemote("open_vehicle_hood");
            else if (item == maletero) mp.events.callRemote("open_vehicle_trunk");
            else if (item == doors) mp.events.callRemote("open_vehicle_doors");
            else if (item == engine) mp.events.callRemote("on_player_toggle_engine");
            else if (item == lock) mp.events.callRemote("on_player_toggle_vehicle_lock");
            menu.Visible = false;
        });
    }

    menu.Visible = true;
});
