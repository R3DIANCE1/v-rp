let gz_text = 0;

mp.events.add('ShowGangZoneText', (args) => {
    gz_text = args
});

// E key
mp.keys.bind(0x45, false, () => {
    if (mp.gui.cursor.visible) return;
    
    if (gz_text > 0) mp.events.callRemote("GangZoneInteract");
});
