mp.events.add('render', () => {
    let result = pointingAt(3.5);
    if (typeof result !== 'undefined') {
        let entity = mp.objects.atHandle(result.entity);
        if (!entity) return;

        if (entity.getVariable("ItemDropID")) {
            mp.game.graphics.drawText('Presione ~y~E ~w~para tomar.', [0.5, 0.005], {
                font: 7,
                color: [255, 255, 255, 185],
                scale: [0.5, 0.5],
                outline: true
            });
        }
    }
});

/*
 * CnR | Splak
 */
function pointingAt(distance) {
    const camera = mp.cameras.new("gameplay");
    let position = camera.getCoord();
    let direction = camera.getDirection();
    let farAway = new mp.Vector3((direction.x * distance) + (position.x), (direction.y * distance) + (position.y), (direction.z * distance) + (position.z));

    let result = mp.raycasting.testPointToPoint(position, farAway, mp.players.local, [16]);

    return result;
}
