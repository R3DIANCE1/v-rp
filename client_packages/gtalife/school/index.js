const NativeUI = require("nativeui");
const Menu = NativeUI.Menu;
const UIMenuItem = NativeUI.UIMenuItem;
const Point = NativeUI.Point;
const Offset = require("gtalife/util/offsets.js")

var school_text_mode = 0;
var school_purchase_menu = null;

mp.events.add('ShowSchoolText', (args) => {
    school_text_mode = args;
});

mp.events.add('School_PurchaseMenu', (args) => {
    var data = JSON.parse(args);

    if (school_purchase_menu == null) {
        school_purchase_menu = new Menu("Autoescuela", "Seleccione una opcion.", new Point(mp.game.resolution.width - Offset.getWidthOffset(mp.game.resolution.width), 50));

        school_purchase_menu.ItemSelect.on((item, index) => {
            if (index == 0) mp.events.callRemote("School_StartTest");
            school_purchase_menu.Visible = false;
        });
    }

    school_purchase_menu.Clear();

    var temp_item = new UIMenuItem("Vehículos", "Seleccionar para comenzar la prueba.");
    temp_item.SetRightLabel("$" + data.Price);
    school_purchase_menu.AddItem(temp_item);

    school_purchase_menu.Visible = true;
});

mp.keys.bind(0x45, false, function() {
    if (mp.gui.cursor.visible) return;
    
    if (school_text_mode == 1) {
        mp.events.callRemote("SchoolInteract");
    }
});
