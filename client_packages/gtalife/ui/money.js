var moneyText = null;
var moneyNegative = false;

var changeText = null;
var changeNegative = false;
var changeTimer = null;

function m_numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

mp.events.add('UpdateMoneyHUD', (money, diff) => {
    if (money < 0) {
        money = Math.abs(money);

        moneyNegative = true;
        moneyText = "-$" + m_numberWithCommas(money);
    } else {
        moneyNegative = false;
        moneyText = "$" + m_numberWithCommas(money);
    }

    if (diff) {
        if (diff < 0) {
            diff = Math.abs(diff);

            changeNegative = true;
            changeText = "-$" + m_numberWithCommas(diff);
        } else if (diff > 0) {
            changeNegative = false;
            changeText = "+$" + m_numberWithCommas(diff);
        }

        if (changeTimer) clearTimeout(changeTimer);
        changeTimer = setTimeout(() => {
            changeText = null;
        }, 5000);
    }
});

mp.events.add('render', () => {
    if (moneyText != null) {
        if (moneyNegative) {
            mp.game.graphics.drawText(moneyText, [0.95, 0.05], { 
                font: 7, 
                color: [255, 0, 0, 185], 
                scale: [0.5, 0.5], 
                outline: true
            });
        } else {
            mp.game.graphics.drawText(moneyText, [0.95, 0.05], { 
                font: 7, 
                color: [0, 255, 0, 185], 
                scale: [0.5, 0.5], 
                outline: true
            });
        }
    }

    if (changeText != null) {
        if (changeNegative) {
            mp.game.graphics.drawText(changeText, [0.95, 0.08], { 
                font: 7, 
                color: [255, 0, 0, 185], 
                scale: [0.5, 0.5], 
                outline: true
            });
        } else {
            mp.game.graphics.drawText(changeText, [0.95, 0.08], { 
                font: 7, 
                color: [0, 255, 0, 185], 
                scale: [0.5, 0.5], 
                outline: true
            });
        }
    }
});
